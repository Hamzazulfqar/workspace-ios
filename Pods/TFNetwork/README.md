# Network Manager (TFNetwork) - IOS

## Description
TFNetwork is a Swift framework that will help to network api calls with Outh2 authentication

## Requirements

* iOS 11.0+
* CocoaPods

## CocoaPods Installation

 In your `Podfile`:

*  `For HTTP`
 ```ruby
 use_frameworks!

 target 'TARGET_NAME' do
     pod 'TFNetwork' , :git => 'https://gitlab.com/target-digital-transformation/ios-module/network-manager.git', :tag => '1.0.13'
 end
``` 

*  `For SSH Key`
```ruby
use_frameworks!

target 'TARGET_NAME' do
    pod 'TFNetwork' , :git => 'git@gitlab.com:target-digital-transformation/ios-module/network-manager.git', :tag => '1.0.13'

end
``` 

Replace `TARGET_NAME` and then, in the `Podfile` directory, type:

```bash
$ pod install
```

## Usage

First Create the Instance of TFNetwork by using these methods
* TFAuth? is passing to TFNetwork class, this will get new token in case of 401 error code.

```swift
TFNetworkManager(access_token: String, refresh_token: String, id_token: String, token_type: String, expires_in: Double?)

TFNetworkManager(authToken: TFAuth.TFTokenModel, auth: TFAuth?)
```

#### if you are using the singleton than you need to set the auth token for Networkmanager class

```swift
TFNetwork.setToken(authToken: TFAuth.TFTokenModel)

TFNetwork.setToken(access_token: String, refresh_token: String, id_token: String, token_type: String, expires_in: Double?, auth: TFAuth?)
```
#### There are two delegate, which tell you about the outh2 token changed or failure

*  `func didFailedRenewToken`
* `func didAccessTokenChanged(token:)`

## Authentication

* For authentication you should use these methods which will return the outhToken information.

```swift

let auth = TFAuth(oauthswift: oauthswift)

auth.authenticate(with: String, password: String) { (model, error) in
    self.token = model
    model?.printValues()
}
```

## Socket Manager

* Socket Manager Initializer

```swift

init(domainUrl: String, network: TFNetworkManager, name: String? = nil)

let socket = TFWebsocket(domainUrl: String, network: TFNetworkManager)
```

### Socket Connect and  Disconnect methods

* ` socket.connectSocket()`

* Alternatively you can use the connection call back

* ` func socket.connectSocket(_ completionhandler: SocketConnectCallback? = nil)`

* ` func closeSocket()`

### Send Message on socket

* ` func sendStringMessage(message: String)`

* ` func sendJSONMessage(data: Data)`

### Socket Manager delegate

* ` func socketDidReceivedEvent(eventJson:String, event: WebSocketEvent, client: WebSocket)`

* ` func socketDidConnect(connect:Bool, event: WebSocketEvent, client: WebSocket)`

* ` func socketDidDisconnect(reason:String,code: UInt16?, event: WebSocketEvent, client: WebSocket)`

* ` func socketFailedWith(error:Error?, event: WebSocketEvent, client: WebSocket)`

* ` func socketRenewAccessToken(renewToken:Bool,accessToken: String?, client: WebSocket?)`

### Use main thread to access the socket instance


