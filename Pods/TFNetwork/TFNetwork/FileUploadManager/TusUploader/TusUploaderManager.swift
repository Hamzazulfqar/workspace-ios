//
//  TusUploader.swift
//  TFNetworkFileUploader
//
//  Created by Muhammad Arslan Khalid on 05/08/2021.
//

import UIKit
import RxCocoa
import RxSwift
import TUSKit

let tusUploader = TusUploaderManager.default;

class TusUploaderManager: NSObject {
    static let  `default` = TusUploaderManager();
    
    var progressHandler = PublishSubject<TusUploadProgress>()
    var success = PublishSubject<TusUploadSuccess>()
    var failure = PublishSubject<TusUploadFailure>()
    
    var uploadFileDic = [String:TusFleUploadModel]()
    var tusClient: TUSClient!
    
    private override init() {
        super.init();
        
        tusClient = TUSClient.shared
        tusClient.delegate = self;
    }
    
}


extension TusUploaderManager {
    
}


extension TusUploaderManager:TUSDelegate{
    func TUSProgress(bytesUploaded uploaded: Int, bytesRemaining remaining: Int) {
        
    }
    
    func TUSProgress(forUpload upload: TUSUpload, bytesUploaded uploaded: Int, bytesRemaining remaining: Int) {
        if let data = self.uploadFileDic[upload.id]{
                self.progressHandler.onNext(TusUploadProgress(bytesUploaded: uploaded, bytesRemaining: remaining, fileId: upload.id, progress: Double( Float(uploaded) / Float(data.fileSize))));
        }
        
    }
    
    func TUSSuccess(forUpload upload: TUSUpload) {
        self.success.onNext(TusUploadSuccess(fileID: upload.id, uploadFileLocationURL: upload.uploadLocationURL?.absoluteString));
        self.uploadFileDic.removeValue(forKey: upload.id);
    }
    
    func TUSFailure(forUpload upload: TUSUpload?, withResponse response: TUSResponse?, andError error: Error?) {
        if(upload?.id != nil){
            self.failure.onNext(TusUploadFailure(fileID: upload!.id, response: response, error: ErrorModel(errorTitle: "", errorDescp: error?.localizedDescription ?? "Unknown Error", error: error)))
            self.uploadFileDic.removeValue(forKey: upload!.id);
        }
       
    }
    
    
}



public struct TusUploadProgress {
   public let bytesUploaded:Int
    public let bytesRemaining:Int
    public let fileId:String
    public let progress:Double
}

public struct TusUploadSuccess {
    public let fileID:String
    public let uploadFileLocationURL:String?
    //public var fileName: String = ""
    
//    mutating func fileNameUpdate(fileName: String) {
//        self.fileName = fileName
//    }
}


public struct TusUploadFailure {
    let fileID:String
    let response:TUSResponse?
    let error:ErrorModel
}


