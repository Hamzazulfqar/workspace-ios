//
//  TFUploaderTusUploadOperation.swift
//  TFNetworkFileUploader
//
//  Created by Muhammad Arslan Khalid on 05/08/2021.
//

import UIKit
import TUSKit
import RxCocoa
import RxSwift

class TFUploaderTusUploadOperation: Operation {
    
    private var fileID:String!
    private var fileName:String!
    private var fileType:String!
    private var filePath:String!
    private var fileSize:UInt64!
    private var progressHandler:((_ progress:TusUploadProgress) -> Void)?
    private var completionHandler:((_ model:TusUploadSuccess) -> Void)!
    private var failure:((_ error:ErrorModel)-> Void)!
    private var disposebag = DisposeBag()
    private var configurator:TFNetworkFileUploaderConfigurator!
    private var uploadToken: String?
    
    var fileNameWithExtention: String {
        return self.fileName + "." + self.filePath.pathExtension
    }
    
    var uploaded: Bool = false {
        willSet {
            willChangeValue(forKey: "isFinished")
        }
        didSet {
            didChangeValue(forKey: "isFinished")
            if uploaded == true {
                print("Custom Async operation has finished")
            }
        }
    }
    var uploading: Bool = false {
        willSet {
            willChangeValue(forKey: "isExecuting")
        }
        didSet {
            didChangeValue(forKey: "isExecuting")
            if uploading == true {
                print("Custom Async operation is executing")
            } else {
                print("Custom Async method is not executing")
            }
        }
    }
    
    deinit {
        print("TusUploaderOperation - Deinit")
    }
    
    init(filePath:String, fileType:String, fileName:String,uploadToken: String?,config:TFNetworkFileUploaderConfigurator, progressHandler:((_ progress:TusUploadProgress) -> Void)?, completionHandler:@escaping(_ model:TusUploadSuccess) -> Void, failure:@escaping(_ error:ErrorModel)-> Void) {
        
        
        self.fileID = "\(Date().timeIntervalSince1970)_file_id_uploader";
        self.fileName = fileName
        self.fileType = fileType
        self.filePath = filePath
        self.configurator = config
        self.completionHandler = completionHandler
        self.progressHandler = progressHandler
        self.failure = failure;
        self.uploadToken = uploadToken
        
        do {
            let attr = try FileManager.default.attributesOfItem(atPath: filePath)
            self.fileSize = attr[FileAttributeKey.size] as? UInt64
        } catch  {
            print("CollaborationChatViewController:btnAttachment: Error getting file size \(error)")
        }
        
        
    }
    
    override var isFinished: Bool {
        return uploaded
    }
    
    override var isExecuting: Bool {
        return uploading
    }
    
    override var isAsynchronous: Bool {
        return true
    }
    
    private func completeOperation() {
        uploading = false
        uploaded = true
    }
    
    override func main() {
        
        self.addObserverForResult()
        
        if self.uploadToken == nil {
            
            self.getUploadToken { [unowned self] (token, error) in
                
                if let token = token {
                    
                    //self.checkFileName();
                    self.saveUploadModelInfo()
                    self.startToUploadFile(fileID: self.fileID, filePath: self.filePath, fileType: self.fileType, fileName: self.fileName, fileUploadToken: token);
                }
                else if let error = error {
                    self.failure(error);
                    self.completeOperation();
                }
            }
        }
        else {
            
            //self.checkFileName();
            self.saveUploadModelInfo()
            self.startToUploadFile(fileID: self.fileID, filePath: self.filePath, fileType: self.fileType, fileName: self.fileName, fileUploadToken: uploadToken!);
        }
        
    }
    
    private func saveUploadModelInfo(){
        tusUploader.uploadFileDic[self.fileID] = TusFleUploadModel(fileName: self.fileID + "IOS-fileName", fileSize: self.fileSize);
    }
    
    func startToUploadFile(fileID:String, filePath:String, fileType:String, fileName:String, fileUploadToken :String){
        let tusUploaderModel = TUSUpload.init(withId: fileID, andFilePathString: filePath, andFileType: self.fileType)
        tusUploaderModel.metadata = ["filename": self.fileID + "IOS-fileName"];
        tusUploader.tusClient.createOrResume(forUpload: tusUploaderModel, withCustomHeaders: ["Authorization" : "Bearer \(self.configurator.network.authToken!)","X-Meera-Storage-Token": "Bearer \(fileUploadToken)"])
    }
    
    func addObserverForResult(){
        tusUploader.progressHandler.subscribe {[weak self] (value) in
            guard let self = self else {return}
            if(value.fileId == self.fileID){
                self.progressHandler?(value);
            }
            
            
        } onError: { [weak self](error) in
            guard let _ = self else {return}
            
        } onCompleted: { [weak self] in
            guard let _ = self else {return}
            
        } onDisposed: { [weak self] in
            guard let _ = self else {return}
            
        }
        .disposed(by: self.disposebag)
        
        
        tusUploader.success.subscribe {[weak self] (value) in
            
            guard let strongSelf = self else {return}
            
            if(value.fileID == strongSelf.fileID){
                
                //var newVal = value
                //newVal.fileNameUpdate(fileName: strongSelf.fileNameWithExtention)
                strongSelf.completionHandler(value);
                strongSelf.completeOperation()
            }
            
            
        } onError: { [weak self](error) in
            guard let _ = self else {return}
            
        } onCompleted: { [weak self] in
            guard let _ = self else {return}
            
        } onDisposed: { [weak self] in
            guard let _ = self else {return}
            
        }
        .disposed(by: self.disposebag)
        
        
        tusUploader.failure.subscribe {[weak self] (value) in
            guard let self = self else {return}
            if(value.fileID == self.fileID){
                self.failure(value.error);
                self.completeOperation()
            }
            
        } onError: { [weak self](error) in
            guard let _ = self else {return}
            
        } onCompleted: { [weak self] in
            guard let _ = self else {return}
            
        } onDisposed: { [weak self] in
            guard let _ = self else {return}
            
        }
        .disposed(by: self.disposebag)
        
    }
    
}


extension TFUploaderTusUploadOperation {
    func getUploadToken(completionHandler:@escaping(_ token:String?,_ error:ErrorModel?) -> Void){
        let path = "/v2/files/upload-token"
        let tfReq = TFRequest(baseUrl: self.configurator.REST_CHAT_API_URL, path: path, method: .get)
        
        self.configurator.network.asyncCall(apiRequest: tfReq, requiredResponce: .plain) { (tfResp) in
            if tfResp.error == nil && ((tfResp.data as? String) != nil) {
                completionHandler(tfResp.data as? String, nil)
                
            } else {
                completionHandler(nil, tfResp.error!)
            }
            
        }
        
    }
}
