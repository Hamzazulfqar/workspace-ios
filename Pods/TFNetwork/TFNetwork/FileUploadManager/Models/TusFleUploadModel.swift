//
//  TusFleUploadModel.swift
//  TFNetworkFileUploader
//
//  Created by Muhammad Arslan Khalid on 09/08/2021.
//

import UIKit

class TusFleUploadModel: NSObject {
    internal init(fileName: String? = nil, fileSize: UInt64? = nil) {
        self.fileName = fileName
        self.fileSize = fileSize
    }
    
    var fileName:String!
    var fileSize:UInt64!
}
