//
//  TFNetworkFileUploaderManager.swift
//  TFNetworkFileUploader
//
//  Created by Muhammad Arslan Khalid on 05/08/2021.
//

import UIKit
import SwiftyJSON
import TUSKit


public class TFNetworkFileUploaderManager: NSObject {
    
    var configurator = TFNetworkFileUploaderConfigurator();
    private let operationQueue:OperationQueue = OperationQueue()
    private var docDirectiryURL:URL{
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
    
    public func setup(domain:String, network:TFNetworkManager, tusUploadPath:String = "/meerastorage/files/", grpcURL:String) {
        
        configurator.MainServer = domain;
        configurator.updateURL();
        configurator.network = network;
        configurator.GRPC_DOMAIN_URL = grpcURL;
        
        let config = TUSConfig(withUploadURLString: self.configurator.REST_BASIC_URL + tusUploadPath, andSessionConfig: URLSessionConfiguration.default)
        TUSClient.setup(with: config)
    }
}



//MARK:- Public Interfaces

extension TFNetworkFileUploaderManager {
    
    
    public func writeDataToDocumentDirectory(fileName:String, data:Data, completionHandler:@escaping(_ fileURL:URL?,_ error:ErrorModel?) -> Void){
        
        DispatchQueue.global(qos: .default)
            .async {
                let docURL = self.docDirectiryURL.appendingPathComponent(fileName);
                do{
                    try data.write(to: docURL)
                    DispatchQueue.main.async {
                        completionHandler(docURL, nil);
                    }
                    
                }
                catch let error {
                    DispatchQueue.main.async {
                        completionHandler(nil,ErrorModel(errorTitle: "", errorDescp: error.localizedDescription, error: error))
                    }
                    
                }
            }
    }
    
    public func writeDataToDocumentDirectory(fileName:String, filePath:String, completionHandler:@escaping(_ fileURL:URL?,_ error:ErrorModel?) -> Void){
        
        DispatchQueue.global(qos: .default)
            .async {
                let docURL = self.docDirectiryURL.appendingPathComponent(fileName);
                do{
                    
                    guard let url =  URL(string: filePath) else{
                        DispatchQueue.main.async {
                            completionHandler(nil,ErrorModel(errorTitle: "", errorDescp: "File path is invalid", error: nil))
                        }
                        return
                    }
                    
                    let data = try Data(contentsOf:url)
                    try data.write(to: docURL)
                    DispatchQueue.main.async {
                        completionHandler(docURL, nil);
                    }
                    
                }
                catch let error {
                    DispatchQueue.main.async {
                        completionHandler(nil,ErrorModel(errorTitle: "", errorDescp: error.localizedDescription, error: error))
                    }
                    
                }
            }
    }
    
    public func deleteFileFromDocumentDirectory(fileName:String, completionHandler:@escaping(_ isSuccess:Bool,_ error:ErrorModel?) -> Void){
        DispatchQueue.global(qos: .default)
            .async {
                let docURL = self.docDirectiryURL.appendingPathComponent(fileName);
                do{
                    if(FileManager.default.fileExists(atPath: docURL.path)){
                        
                        try FileManager.default.removeItem(atPath: docURL.path)
                    }
                    DispatchQueue.main.async {
                        completionHandler(true, nil);
                    }
                    
                }
                catch let error {
                    DispatchQueue.main.async {
                        completionHandler(false,ErrorModel(errorTitle: "", errorDescp: error.localizedDescription, error: error))
                    }
                    
                }
            }
    }
    
    public func fileUploadWithMultiPartFormData(
        data:Data,
        parameters:[String:String]? = nil,
        path:String = "upload",
        queryParamters:[String:String]? = ["bucket":"upload", "share_with":"sys:anonymous,sys:authenticated", "permission":"view"],
        fileKeyName:String = "image",
        fileName:String,
        mimType:String,
        progressHandler:((_ progress:Double) -> Void)?,
        completionHandler:@escaping(_ attachment:JSON) -> Void,
        failure:@escaping(_ error:ErrorModel?)-> Void){
        self.checkDomainError();
        let operation = MultiPartUploadFileOperation(data: data, parameters: parameters,path: path,queryParameters: queryParamters, fileKeyName: fileKeyName, fileName: fileName, mimType: mimType, config: self.configurator) { (value) in
            progressHandler?(value);
        } completionHandler: { (data) in
            
            self.jsonParseser(data: data, completionHandler: completionHandler, failure: failure)
            
        } failure: { (error) in
            failure(error);
        }
        self.operationQueue.addOperation(operation);
    }
    
    public func fileUploadWithMultiPartFormData<D:Decodable> (
        
        data:Data,
        type:D.Type ,
        parameters:[String:String],
        path:String = "upload",
        queryParamters:[String:String]? = ["bucket":"upload", "share_with":"sys:anonymous,sys:authenticated", "permission":"view"],
        fileName:String, //Name + extension
        mimType:String, //File Type
        fileKeyName:String = "image",
        progressHandler:((_ progress:Double) -> Void)?,
        completionHandler:@escaping(_ attachment:AnyObject) -> Void,
        failure:@escaping(_ error:ErrorModel)-> Void){
        self.checkDomainError();
        let operation = MultiPartUploadFileOperation(data: data, parameters: parameters,path: path,queryParameters: queryParamters, fileKeyName: fileKeyName, fileName: fileName, mimType: mimType, config: self.configurator) { (value) in
            progressHandler?(value);
        } completionHandler: { (data) in
            self.convertDataWithCompletionHandler(data, decode: type, success: completionHandler, failure: failure)
        } failure: { (error) in
            failure(error);
        }
        self.operationQueue.addOperation(operation);
    }
    
    public func fileUPloadWithTusClinet( fileName: String,fileType: String, filePath:String,uploadToken: String?,progressHandler:((_ progress:TusUploadProgress) -> Void)?, completionHandler:@escaping(_ attachment:TusUploadSuccess) -> Void, failure:@escaping(_ error:ErrorModel)-> Void){
        
        self.checkDomainError();
        
        //let fileNewName = self.checkFileName(fileName: fileName, filePath: filePath, worksapceId: worksapceId,grpcFilePath: grpcFilePath)
        
        let operation = TFUploaderTusUploadOperation(filePath: filePath, fileType: fileType, fileName: fileName, uploadToken: uploadToken, config: self.configurator, progressHandler: progressHandler, completionHandler: completionHandler, failure: failure);
        self.operationQueue.addOperation(operation);
    }
    
    /*public func fileUPloadWithTusClinet( fileName: String,fileType: String, filePath:String,grpcFilePath: String?,worksapceId: UInt32,uploadToken: String?,progressHandler:((_ progress:TusUploadProgress) -> Void)?, completionHandler:@escaping(_ attachment:TusUploadSuccess) -> Void, failure:@escaping(_ error:ErrorModel)-> Void){
        
        self.checkDomainError();
        
        let fileNewName = self.checkFileName(fileName: fileName, filePath: filePath, worksapceId: worksapceId,grpcFilePath: grpcFilePath)
        
        let operation = TFUploaderTusUploadOperation(filePath: filePath, fileType: fileType, fileName: fileName, uploadToken: uploadToken, config: self.configurator, progressHandler: progressHandler, completionHandler: completionHandler, failure: failure);
        self.operationQueue.addOperation(operation);
    }*/
    
}

extension TFNetworkFileUploaderManager {
    /**
     * check file name is exist on grpc system
     */
    /*private func checkFileName(fileName: String,filePath: String, worksapceId: UInt32,grpcFilePath: String?) -> String {
           
        var fileAlreadyExist = false
            //let path =  self.filePath!;
            let name = fileName.components(separatedBy: ".\(filePath.pathExtension)").first!
            let fileExtension = filePath.pathExtension;
            
            var newFileName = name
            var fileNameSuffix = ""
            var count = 0
            repeat {
                if fileAlreadyExist {
                    //path = "\(name)\(count).\(fileExtension)"
                    //options.directory.pathWithFileName("\(file.name)\(count).\(file.fileExtention)")
                    count = count + 1
                    fileNameSuffix = "\(count)"
                    
                }
                
                var newPath = name + "." + fileExtension
                if count != 0 {
                    newPath = name + "(" + fileNameSuffix + ")" + "." + fileExtension
                    newFileName = name + "(" + fileNameSuffix + ")"
                }
                
                
                if grpcFilePath == nil {
                    newPath = "/" + newPath
                }
                else {
                    newPath = grpcFilePath! + "/" + newPath
                }
                
                let result = GRPClient.fileExists(at: newPath ,worksapceId: worksapceId, configurator: self.configurator)
                fileAlreadyExist = result.exists
            } while fileAlreadyExist
            
            return newFileName
        }*/
}

//MARK:- Helping Methods
extension TFNetworkFileUploaderManager {
    
    func checkDomainError(){
        assert(self.configurator.MainServer != nil, "domain is not set");
    }
    
    func convertDataWithCompletionHandler<D: Decodable>(_ data: Data,decode:D.Type, success:@escaping(_ result:AnyObject) -> Void,
                                                        failure:@escaping (_ errorMode:ErrorModel) -> Void) {
        do {
            let decoder = JSONDecoder()
            let obj = try decoder.decode(decode, from: data)
            
            success(obj as AnyObject)
        } catch let error {
            
            let userInfo = [NSLocalizedDescriptionKey : "Could not parse the data as JSON: '\(data)'"]
            
            print("userInfo error: \(userInfo)")
            failure(ErrorModel(errorTitle: "", errorDescp: error.localizedDescription, error: error))
        }
        
    }
    
    func jsonParseser(data:Data,completionHandler:@escaping(_ attachment:JSON) -> Void, failure:@escaping(_ error:ErrorModel?)-> Void ){
        
        do {
            
            let json  = try JSON(data: data);
            DispatchQueue.main.async {
                completionHandler(json);
            }
        }
        catch (let error) {
            failure(ErrorModel(errorTitle: "Decoding failed", errorDescp: error.localizedDescription, error: error))
            
        }
    }
    
}
