//
//  TFNetworkFileUploaderConfigurator.swift
//  TFNetworkFileUploader
//
//  Created by Muhammad Arslan Khalid on 05/08/2021.
//

import UIKit

class TFNetworkFileUploaderConfigurator: NSObject {    
    var network:TFNetworkManager!
    
    var MainServer:String!
    private var HTTPS = "https://";
    var REST_BASIC_URL:String!
    var REST_CHAT_API_URL:String!
    var FILE_UPLOAD_URL:String!
    var GRPC_DOMAIN_URL:String!
    
    func updateURL() {
        REST_BASIC_URL = HTTPS + "api.\(self.MainServer!)"
        REST_CHAT_API_URL = REST_BASIC_URL + "/chat" + "/api";
        FILE_UPLOAD_URL = REST_BASIC_URL + "/fm";
    }
}
