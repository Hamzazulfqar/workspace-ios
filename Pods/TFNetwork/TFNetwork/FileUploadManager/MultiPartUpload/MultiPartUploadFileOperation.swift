//
//  MultiPartUploadFileOperation.swift
//  TFNetworkFileUploader
//
//  Created by Muhammad Arslan Khalid on 05/08/2021.
//

import UIKit
import SwiftyJSON
import Alamofire

class MultiPartUploadFileOperation: Operation {
    
    private var fileData:Data!
    private var parameters:[String:String]?
    private var fileName:String!
    private var fileType:String!
    private var path:String!
    private var fileKeyName:String!
    private var queryParameters:[String:String]?
    private var progressHandler:((_ progress:Double) -> Void)?
    private var completionHandler:((_ attachmentID:Data) -> Void)!
    private var failure:((_ error:ErrorModel)-> Void)!
    private var configurator:TFNetworkFileUploaderConfigurator!
    

    var uploaded: Bool = false {
        willSet {
            willChangeValue(forKey: "isFinished")
        }
        didSet {
            didChangeValue(forKey: "isFinished")
            if uploaded == true {
                print("Custom Async operation has finished")
            }
        }
    }
    var uploading: Bool = false {
        willSet {
            willChangeValue(forKey: "isExecuting")
        }
        didSet {
            didChangeValue(forKey: "isExecuting")
            if uploading == true {
                print("Custom Async operation is executing")
            } else {
                print("Custom Async method is not executing")
            }
        }
    }
    
    deinit {
        print("TusUploaderOperation - Deinit")
    }
    
    
    
    init(data:Data ,parameters:[String:String]?,path:String,queryParameters:[String:String]?,fileKeyName:String,fileName:String,mimType:String,config:TFNetworkFileUploaderConfigurator, progressHandler:((_ progress:Double) -> Void)?, completionHandler:@escaping(_ attachment:Data) -> Void, failure:@escaping(_ error:ErrorModel)-> Void) {
     
        self.fileData = data
        self.parameters = parameters
        self.fileName = fileName
        self.fileType = mimType
        self.configurator = config
        self.path = path
        self.fileKeyName = fileKeyName
        self.queryParameters = queryParameters;
        self.progressHandler = progressHandler
        self.completionHandler = completionHandler
        self.failure = failure;
    }
    
    
    override func main() {
        
        let fileUploadURLStr =  self.getUploadFileURL(urlStr: self.configurator.FILE_UPLOAD_URL);
        let request = self.getRequestForFileUpload(urlStr: fileUploadURLStr)
        
        AF.upload(multipartFormData: { multiPartFormData in
            if let parameter = self.parameters{
                for (k,v) in parameter {
                    multiPartFormData.append(v.data(using: String.Encoding.utf8)!, withName: k)
                }
            }
            
            multiPartFormData.append(self.fileData, withName: self.fileKeyName, fileName: self.fileName, mimeType: self.fileType);
        }, with: request)
        .uploadProgress { [weak self] (progress) in
            guard let self = self else {return}
            self.progressHandler?(progress.fractionCompleted);
            print( "progress is : \(progress.fractionCompleted)")
        }
        .responseData(completionHandler: {[weak self] (response) in
            guard let self = self else {return}
            
            switch(response.result){
            case .success(let result):
                self.completionHandler(result);
                break;
            case .failure(let error):
                
                self.failure(ErrorModel(errorTitle: "", errorDescp: error.localizedDescription, error: error))
                self.completeOperation()
                
                break;
            }
        })
    }
    
    
    override var isFinished: Bool {
        return uploaded
    }
    
    override var isExecuting: Bool {
        return uploading
    }
    
    override var isAsynchronous: Bool {
        return true
    }
    
    private func completeOperation() {
        uploading = false
        uploaded = true
    }
}

extension MultiPartUploadFileOperation {
    
    func getUploadFileURL(urlStr:String) -> String {
        let imageBaseUrlUpload = urlStr + "/upload?bucket=upload&share_with=sys:anonymous,sys:authenticated&permission=view"
        return  imageBaseUrlUpload
    }
    
    func getRequestForFileUpload(urlStr:String) -> URLRequest {
        
        if  let accessToken  = self.configurator.network.authToken {
            
            var urlComponents = URLComponents(string: urlStr  + "/" + self.path)
            
            if let queryParametes = self.queryParameters {
                urlComponents?.queryItems = queryParametes.map{URLQueryItem(name: $0.key, value: $0.value)}
            }
            
            if let url =  urlComponents?.url {
                let mainUrl:URL = URL(string: url.absoluteString.encodeUrl!)!
                var request = URLRequest(url: mainUrl)
                request.httpMethod = "POST"
                let boundary  = "---------------------------147378098314876653456641449"
                request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
                return request
            }
            else {
                
                assertionFailure("issue in URL");
                return URLRequest(url: URL(string: "")!);
            }
            
           
        }
        else {
            assertionFailure("access token is missing")
            return URLRequest(url: URL(string: "")!);
        }
        
       
    }
}


enum ReponseType {
    case json
    case decodable(D: Decodable)
}
