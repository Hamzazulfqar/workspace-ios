//
//  TFAuth.swift
//  BloomingBuyer
//
//  Created by Waqas Rasheed on 20/03/2021.
//

import OAuthSwift
import SwiftyJSON

public class TFAuth {
    
    private var oauthswift: OAuth2Swift!
    
    public init(oauthswift: OAuth2Swift) {
        self.oauthswift = oauthswift
    }
    public func setOauthswift(oauthswift: OAuth2Swift) {
        self.oauthswift = oauthswift
    }
    
    public func authenticate(with userName: String, password: String, completion: @escaping (TFTokenModel?,OAuthSwiftError?) -> Void) {
        
        self.preparingOth2WithPassword(with: userName, password: password) { (dic) in
            
            var token_id: String?
            var token_type: String?
            var expireIn: Double?
            var expireTimestamp: Double?
            
            if let acToken = dic["access_token"] as? String, let refToken = dic["refresh_token"] as? String {
                
                
                if let idtoken = dic["id_token"] as? String {
                    token_id = idtoken
                }
                
                if let tokentype = dic["token_type"] as? String {
                    token_type = tokentype
                }
                
                if let expiresin = dic["expires_in"] as? Double {
                    expireIn = expiresin
                    
                    let duration = expireIn
                    
                    expireTimestamp = Date().timeIntervalSince1970.rounded() + duration!
                }
                
                
                
                let tokeModel = TFTokenModel(access_token: acToken, refresh_token: refToken, id_token: token_id, token_type: token_type, expires_in: expireIn,expire_timestamp: expireTimestamp)
                completion(tokeModel,nil)
            }
            
            
        } failure: { (error) in
            
            print(error.localizedDescription)
            completion(nil, error)
        }
    }
    
    public func preparingOth2WithPassword(with userName:String, password: String, jsonHandler:@escaping(_ json: [String:Any]) -> Void, failure:@escaping(_ error: OAuthSwiftError)-> Void)
    {
        guard oauthswift != nil else {
            precondition(oauthswift != nil, "oauthswift is nil, please set it")
            return
        }
        oauthswift.authorize(username: userName, password: password, scope: "openid email groups profile offline_access") { (result) in
            switch(result)
            {
            
            case .success(let (_,response ,_ )):
                if let jsonResponce = response?.data
                {
                    do {
                        let jsonResult = try JSONSerialization.jsonObject(with: jsonResponce, options: JSONSerialization.ReadingOptions.allowFragments)
                        
                        let parseJson = jsonResult as! [String:Any]
                        jsonHandler(parseJson)
                    }
                    catch {
                        let notValid = OAuthSwiftError.configurationError(message: "json not valid")
                        failure(notValid)
                    }
                }
                else{
                    let notValid = OAuthSwiftError.configurationError(message: "No user found")
                    failure(notValid)
                }
                
            case.failure(let error):
                
                let errorDic = error.errorUserInfo;
                
                if(self.outhErrorHandler(dic: errorDic).code == 401)
                {
                    let notValid = OAuthSwiftError.configurationError(message: "Invalid email or password.")
                    failure(notValid)
                }
                else if(self.outhErrorHandler(dic: errorDic).code == 400)
                {
                    failure(error)
                }
                else
                {
                    let meessages = self.outhErrorHandler(dic: errorDic)
                    let message = meessages.errorStr + "\(meessages.code)"
                    let notValid = OAuthSwiftError.configurationError(message: message)
                    failure(notValid)
                }
            }
        }
        
    }
    
    private func outhErrorHandler(dic: [String:Any]?) -> (code:Int,errorStr:String)
    {
        if let _ = dic
        {
            if let m = dic!["error"] as? NSError
            {
                print(m.localizedDescription)
                print(m.code);
                return (code:m.code,errorStr:m.localizedDescription)
            }
        }
        
        return (code:0,errorStr:"Unknown error");
    }
}

extension TFAuth {
    
    public func renewAcessToken(refreshToken:String,_ completionHandler: @escaping (_ json: [String:Any]?) -> Void) {
        guard isValidText(refreshToken) == true else {
            completionHandler(nil)
            return
        }
        
        oauthswift.renewAccessToken(withRefreshToken: refreshToken) { (result) in
            switch(result)
            {
            case.success(let(_, _, oauthParams)):
                
                if let _ = oauthParams["access_token"] as? String,
                   let _ = oauthParams["refresh_token"] as? String,
                   let _ = oauthParams["expires_in"] as? Double {
                    
                    completionHandler(oauthParams)
                } else {
                    completionHandler(nil)
                }
                break;
            case .failure(let error):
                
                print("renewAuthToken error: \(error.localizedDescription)")
                completionHandler(nil)
                break;
            }
        }
    }
    
    public func renewToken(refreshToken:String,_ completionHandler: @escaping (TFTokenModel?) -> Void) {
        guard isValidText(refreshToken) == true else {
            completionHandler(nil)
            return
        }
        
        self.renewAcessToken(refreshToken: refreshToken) { (dic) in
            
            if let dic = dic {
                
                var token_id: String?
                var token_type: String?
                var expireIn: Double?
                var expireTimestamp: Double?
                
                if let acToken = dic["access_token"] as? String, let refToken = dic["refresh_token"] as? String {
                    
                    
                    if let idtoken = dic["id_token"] as? String {
                        token_id = idtoken
                    }
                    
                    if let tokentype = dic["token_type"] as? String {
                        token_type = tokentype
                    }
                    
                    if let expiresin = dic["expires_in"] as? Double {
                        expireIn = expiresin
                        
                        let duration = expireIn
                        
                        expireTimestamp = Date().timeIntervalSince1970.rounded() + duration!
                    }
                    
                    let tokeModel = TFTokenModel(access_token: acToken, refresh_token: refToken, id_token: token_id, token_type: token_type, expires_in: expireIn,expire_timestamp: expireTimestamp)
                    
                    completionHandler(tokeModel)
                }
            }
            else {
                completionHandler(nil)
            }
        }
    }
}

extension TFAuth {
    public struct TFTokenModel {
        public var access_token: String
        public let refresh_token: String
        public let id_token: String?
        public let token_type: String?
        public let expires_in: Double?
        public let expire_timestamp: Double?
        
        mutating func expireToken() {
            access_token = "ey"
        }
        
        public func printValues() {
            print("access_token:\(access_token)")
            print("refresh_token:\(refresh_token)")
            print("id_token:\(id_token ?? "")")
            print("token_type:\(token_type ?? "")")
            print("expires_in:\(expires_in ?? -1)")
            print("expire_timestamp:\(expire_timestamp ?? -1)")
        }
    }
}

/*
public struct TFAuth1 {
    
    private static var oauthswift: OAuth2Swift!
    
    public static func setOauthswift(oauthswift: OAuth2Swift) {
        self.oauthswift = oauthswift
    }
    
    
    public static func authenticate(with userName: String, password: String, completion: @escaping (TFTokenModel?,OAuthSwiftError?) -> Void) {
        
        TFAuth1.preparingOth2WithPassword(with: userName, password: password) { (dic) in
            
            var token_id: String?
            var token_type: String?
            var expireIn: Double?
            
            if let acToken = dic["access_token"] as? String, let refToken = dic["refresh_token"] as? String {
                
                
                if let idtoken = dic["id_token"] as? String {
                    token_id = idtoken
                }
                
                if let tokentype = dic["token_type"] as? String {
                    token_type = tokentype
                }
                
                if let expiresin = dic["expires_in"] as? Double {
                    expireIn = expiresin
                }
                
                let tokeModel = TFTokenModel(access_token: acToken, refresh_token: refToken, id_token: token_id, token_type: token_type, expires_in: expireIn)
                completion(tokeModel,nil)
            }
            
            
        } failure: { (error) in
            
            print(error.localizedDescription)
            completion(nil, error)
        }
    }
    
    public static func preparingOth2WithPassword(with userName:String, password: String, jsonHandler:@escaping(_ json: [String:Any]) -> Void, failure:@escaping(_ error: OAuthSwiftError)-> Void)
    {
        guard oauthswift != nil else {
            precondition(oauthswift != nil, "oauthswift is nil, please set it")
            return
        }
        oauthswift.authorize(username: userName, password: password, scope: "openid email groups profile offline_access") { (result) in
            switch(result)
            {
            
            case .success(let (_,response ,_ )):
                if let jsonResponce = response?.data
                {
                    do {
                        let jsonResult = try JSONSerialization.jsonObject(with: jsonResponce, options: JSONSerialization.ReadingOptions.allowFragments)
                        
                        let parseJson = jsonResult as! [String:Any]
                        jsonHandler(parseJson)
                    }
                    catch {
                        let notValid = OAuthSwiftError.configurationError(message: "json not valid")
                        failure(notValid)
                    }
                }
                else{
                    let notValid = OAuthSwiftError.configurationError(message: "No user found")
                    failure(notValid)
                }
                
            case.failure(let error):
                
                let errorDic = error.errorUserInfo;
                
                if(self.outhErrorHandler(dic: errorDic).code == 401)
                {
                    let notValid = OAuthSwiftError.configurationError(message: "Invalid email or password.")
                    failure(notValid)
                }
                else if(self.outhErrorHandler(dic: errorDic).code == 400)
                {
                    failure(error)
                }
                else
                {
                    let meessages = self.outhErrorHandler(dic: errorDic)
                    let message = meessages.errorStr + "\(meessages.code)"
                    let notValid = OAuthSwiftError.configurationError(message: message)
                    failure(notValid)
                }
            }
        }
        
    }
    
    private static func outhErrorHandler(dic: [String:Any]?) -> (code:Int,errorStr:String)
    {
        if let _ = dic
        {
            if let m = dic!["error"] as? NSError
            {
                print(m.localizedDescription)
                print(m.code);
                return (code:m.code,errorStr:m.localizedDescription)
            }
        }
        
        return (code:0,errorStr:"Unknown error");
    }
}

extension TFAuth1 {
    
    public static func renewAcessToken(refreshToken:String,_ completionHandler: @escaping (_ json: [String:Any]?) -> Void) {
        guard isValidText(refreshToken) == true else {
            completionHandler(nil)
            return
        }
        
        oauthswift.renewAccessToken(withRefreshToken: refreshToken) { (result) in
            switch(result)
            {
            case.success(let(_, _, oauthParams)):
                
                if let _ = oauthParams["access_token"] as? String,
                   let _ = oauthParams["refresh_token"] as? String,
                   let _ = oauthParams["expires_in"] as? Double {
                    
                    completionHandler(oauthParams)
                } else {
                    completionHandler(nil)
                }
                break;
            case .failure(let error):
                
                print("renewAuthToken error: \(error.localizedDescription)")
                completionHandler(nil)
                break;
            }
        }
    }
    
    public static func renewToken(refreshToken:String,_ completionHandler: @escaping (TFTokenModel?) -> Void) {
        guard isValidText(refreshToken) == true else {
            completionHandler(nil)
            return
        }
        
        TFAuth1.renewAcessToken(refreshToken: refreshToken) { (dic) in
            
            if let dic = dic {
                
                var token_id: String?
                var token_type: String?
                var expireIn: Double?
                
                if let acToken = dic["access_token"] as? String, let refToken = dic["refresh_token"] as? String {
                    
                    
                    if let idtoken = dic["id_token"] as? String {
                        token_id = idtoken
                    }
                    
                    if let tokentype = dic["token_type"] as? String {
                        token_type = tokentype
                    }
                    
                    if let expiresin = dic["expires_in"] as? Double {
                        expireIn = expiresin
                    }
                    
                    let tokeModel = TFTokenModel(access_token: acToken, refresh_token: refToken, id_token: token_id, token_type: token_type, expires_in: expireIn)
                    
                    completionHandler(tokeModel)
                }
            }
            else {
                completionHandler(nil)
            }
        }
    }
}

extension TFAuth1 {
    public struct TFTokenModel {
        public let access_token: String
        public let refresh_token: String
        public let id_token: String?
        public let token_type: String?
        public let expires_in: Double?
        
        /*public mutating func changeAccessToken() {
            self.access_token = "eys"
        }
        
        public mutating func changeRefreshToken() {
            self.refresh_token = "eys"
        }*/
        
        public func printValues() {
            print("access_token:\(access_token)")
            print("refresh_token:\(refresh_token)")
            print("id_token:\(id_token ?? "")")
            print("token_type:\(token_type ?? "")")
            print("expires_in:\(expires_in ?? -1)")
        }
    }
}*/

extension TFAuth {
    /*
    public static func forgetPasswordCall(with emailOrMobile: String, smsCode: String?, password: String?,completion: @escaping (_ success: Bool,_ error: TFError?) -> Void)
    {
        let forgetPasswordURL = "/api/forgot-password";
        
        var inputParam = [String:String]();
        
        if let code = smsCode, let pass = password
        { // forget password via mobile number
            
            inputParam["mobile"] = emailOrMobile
            inputParam["password"] = pass;
            inputParam["code"] = code;
        }
        else
        {
            inputParam["email"] = emailOrMobile
        }
        
        //inputParam["mobile"] = "+92 3008062984";
        //inputParam["mobile"] = "+86 13980001031";
        let req = TFRequest(baseUrl: TFAuth.SSO_Url,path: forgetPasswordURL, method: .post, bodyPerameter: inputParam,header: TFRequest.getSSOHeader())
        
        TFNetwork.asyncCall(apiRequest: req, requiredResponce: .json, WithDelegate: nil) { (tfResp) in
            
            if tfResp.error == nil && tfResp.data != nil {
                
                let json = JSON(tfResp.data!)
                
                let success = json["success"].boolValue;
                
                if (success)
                {
                    completion(success,nil);
                }
                else
                {
                    if let msg = json["error"].string
                    {
                        completion(false,ErrorModel(errorTitle: "", errorDescp: msg.capitalized, error: nil));
                    }
                    else
                    {
                        completion(false,ErrorModel(errorTitle: "", errorDescp: "Something is going wrong", error: nil));
                    }
                }
            }
        }
        
    }*/
}
