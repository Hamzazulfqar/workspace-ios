//
//  MeeraSpaceConstans.swift
//  Meera Work Space
//
//  Created by Muhammad Arslan Khalid on 18/11/2018.
//  Copyright © 2018 Target OilField Services. All rights reserved.
//

import Foundation

class NetworkHelper: NSObject {
    @objc static let sharedInstance = NetworkHelper()
    
    override init() {
        super.init()
        
        self.setupReachability(nil, useClosures: true);
    }
    
    var reachability: Reachability?
    
    func isInternet() -> Bool {
        return self.reachability?.connection != Reachability.Connection.none
    }
    
    private func setupReachability(_ hostName: String?, useClosures: Bool) {
        
        let reachable: Reachability?
        if let hostName = hostName {
            reachable = Reachability(hostname: hostName)
            //hostNameLabel.text = hostName
        } else {
            reachable = Reachability()
            //hostNameLabel.text = "No host name"
        }
        self.reachability = reachable
        
        
        if useClosures {
            self.reachability?.whenReachable = { reachability in
                //self.updateLabelColourWhenReachable(reachability)
                print("Internet Available")
                
            }
            self.reachability?.whenUnreachable = { reachability in
                //self.updateLabelColourWhenNotReachable(reachability)
                print("Internet Not Available")
            }
        } else {
            NotificationCenter.default.addObserver(
                self,selector: #selector(reachabilityChanged(_:)), name: .reachabilityChanged,object: reachability
            )
        }
        
    }
    
    func startNotifier() {
        print("--- start notifier")
        do {
            try self.reachability?.startNotifier()
        } catch {
            
            print("Unable to start notifier")
            return
        }
    }
    
    func stopNotifier() {
        print("--- stop notifier")
        self.reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: nil)
        //self.reachability = nil
    }
    
    @objc func reachabilityChanged(_ note: Notification) {
        let reachable = note.object as! Reachability
        
        if reachable.connection != .none {
            //updateLabelColourWhenReachable(reachability)
            print("Internet Available")
        } else {
            //updateLabelColourWhenNotReachable(reachability)
            print("Internet Not Available")
        }
    }
    
}

func isValidText(_ text:String?) -> Bool
{
    if let data = text
    {
        if data == "nil"
        {
            return false
        }
        let str = data.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if str.count>0
        {
            return true
        }
        else
        {
            return false
        }
    }
    else
    {
        return false
    }
    
}

func isInternet() -> Bool {
    return NetworkHelper.sharedInstance.isInternet()
}

extension Double {
    
    public func tfGetLocalDateStringFromTimeInterval() -> String {
        let date = Date(timeIntervalSince1970: self)
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.timeZone = .current
        dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.short //Set date style
        
        return dateFormatter.string(from: date)
    }
}

extension Date {
    static var TFTimeInterval: Double {
        return (Date().timeIntervalSince1970)
    }
}
