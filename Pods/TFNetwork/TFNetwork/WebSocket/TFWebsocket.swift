//
//  TFlWebsocket.swift
//  Meera
//
//  Created by Waqas Rasheed on 01/09/2021.
//  Copyright © 2021 Waqas Rasheed. All rights reserved.
//

import Foundation
import UIKit
import Starscream

public typealias SocketConnectCallback = ((_ connect: Bool) -> Void)

public class TFWebsocket: NSObject {
    //NameDescribable
    var socketEndPoint: String {
        return config.socketURL
    }
    
    var config = TFWebSockeConfig()
    
    //MARK:- WEBSOCKET
    private var starWebSocket: WebSocket?
    private var connectivityStatus: SocketConnectStatus = .notStart
    var name: String!
    
    //let pingTimerInterval = 10.0
    //var pingTimer: Timer?
    
    var isTokenRenew = false
    var isConnected = false
    var isForceClosed = false
    
    var storeSocketConnectCallback: [SocketConnectCallback?] = [SocketConnectCallback?]()
    public var delegate = TFMulticastDelegate<TFWebsocketDelegate>()
    //var delegates: WeakCollection<TFWebsocketDelegate> = []
    
    public var isSocketConnected: Bool {
        return isConnected
    }
    
    public var webSocket: WebSocket? {
        return starWebSocket
    }
    
    override private init() {
        super.init()
        
    }
    
    public init(domainUrl: String, network: TFNetworkManager, name: String? = nil) {
        assert(Thread.isMainThread, "should main thread")
        
        super.init()
        
        self.config.configure(domainUrl: domainUrl, network: network)
        
        self.connectivityStatus = .notStart
        
        self.name = name
        
        self.internetNotifierObserver()
    }

    deinit {
        //pingTimer?.invalidate()
        //pingTimer = nil
    }
}

//MARK:- WEBSOCKET Connectivity
extension TFWebsocket {
    
    public func connectSocket(_ completionhandler: SocketConnectCallback? = nil) {
        
        assert(Thread.isMainThread, "TFWebsocket needs to be accessed on the main thread.")
        
        if completionhandler != nil {
            storeSocketConnectCallback.append(completionhandler!)
        }
        
        guard isInternet(), isValidText(self.config.tfNetwork.authToken) else {
            handleCallBack(isConnect: false)
            return
        }
        
        if let ex = self.isTokenExpired(), ex == true {
            
            if !self.config.tfNetwork.isTokenRefreshedRecently {
                self.renewToken()
                
                if isConnected {
                    handleCallBack(isConnect: true)
                }
                
                return
            }
        }
        
        gotoConnect(completionhandler)
    }
    
    fileprivate func gotoConnect(_ completionhandler: SocketConnectCallback?) {
        if ((!isConnected && self.starWebSocket == nil && connectivityStatus == .notStart) || (!isConnected && self.starWebSocket == nil && completionhandler == nil && connectivityStatus == .notStart)) {
            
            isForceClosed = false
            
            self.connectivityStatus = .inProgress
            
            let token = self.config.tfNetwork.authToken
            precondition(token != nil, "token should not b nil")
            
            print("CallSocketURl: \(socketEndPoint)")
            let urlStr = socketEndPoint + "?access_token=\(token!)"
            
            let encodedString = urlStr.encodeUrl
            let url = URL(string: encodedString!)!
            
            var request = URLRequest(url:url)
            request.setValue("Bearer \(token!)", forHTTPHeaderField: "Authorization")
            request.timeoutInterval = 20
            
            self.starWebSocket = WebSocket(request: request)
            self.starWebSocket!.delegate = self
            
            self.starWebSocket!.connect();
        }
        else {
            
            guard self.connectivityStatus == .notStart else {
                return
            }
            
            if self.storeSocketConnectCallback.count > 0 && isConnected {
                
                handleCallBack(isConnect: true)
            }
        }
    }
}

//MARK:- WEBSOCKET Session Delegates
extension TFWebsocket: WebSocketDelegate {
    
    public func didReceive(event: WebSocketEvent, client: WebSocket) {
        
        self.connectivityStatus = .notStart
        
        switch event {
        case .connected(_):
            
            isConnected = true
            isTokenRenew = false
            
            self.connectSocketHanlder()
            
            //CallLogger.shared?.write("TFlWebsocketDelegate: TFlWebsocket connected")
            delegate.invoke({
                $0.socketDidConnect(connect: isConnected, event: event, client: client)
            })
            
            print("TFWebsocket connected event")

        case .disconnected(let reason, let code):
            
            isTokenRenew = false
            //CallLogger.shared?.write("Call TFlWebsocket disconnected")
            print("Call TFWebsocket disconnected event")
            self.handlerAfterSocketDisconnect()
            
            delegate.invoke({
                $0.socketDidDisconnect(reason: reason, code: code, event: event, client: client)
            })
            
            print("websocket is disconnected: \(reason) with code: \(code)")
            
        case .text(let string):
            
            delegate.invoke({
                $0.socketDidReceivedEvent(eventJson: string, event: event,client: client)
            })
            
        case .binary(let data):
            print("Received data: \(data.count)")
        case .ping(_):
            break
        case .pong(_):
            break
        case .viabilityChanged(_):
            break
        case .reconnectSuggested(_):
            //CallLogger.shared?.write("reconnectSuggested")
            break
        case .cancelled:
            //isConnected = false
            self.handlerAfterSocketDisconnect()
            delegate.invoke({
                $0.socketDidDisconnect(reason: "Socket Disconnected with cancelled event", code: nil,event: event,client: client)
            })
            
        case .error(let error):
            
            self.handlerAfterSocketDisconnect()
            
            //CallLogger.shared?.write("TFlWebsocket Error: \(error?.localizedDescription ?? "error"), \(error.debugDescription)")
            delegate.invoke({
                $0.socketFailedWith(error: error, event: event, client: client)
            })
            
            print(error?.localizedDescription ?? "nothing");
            
            if error.debugDescription == "Optional(Starscream.HTTPUpgradeError.notAnUpgrade(500))" && !isTokenRenew {
                //renewToken only once
                self.renewToken()
            }
            
        }    }
}

//MARK:- WEBSOCKET Utility Methods
extension TFWebsocket {
    
    func handleCallBack(isConnect: Bool) {
        
        for n in 0..<storeSocketConnectCallback.count {
            
            var sc = storeSocketConnectCallback[n]
            
            sc!(isConnect)
            sc = nil
        }
        
        storeSocketConnectCallback.removeAll()
    }
    
    private func connectSocketHanlder() {
        
        //print("\(self.typeName): web socket did connect")
        //CallLogger.shared?.write("WebSocket did connect\n-------------------------------------")
        //self.incommingCallId = nil
        
        if self.storeSocketConnectCallback.count > 0 {
            
            handleCallBack(isConnect: true)
        }
    }
    
    private func handlerAfterSocketDisconnect() {
        
        isConnected = false
        starWebSocket = nil
        
        //pingTimer?.invalidate()
        
        if !isForceClosed {
            //CallLogger.shared?.write("Socket will try to connect again")
            connectSocket()
        }
        
    }
    
    public func closeSocket() {
        
        assert(Thread.isMainThread, "TFWebsocket needs to be accessed on the main thread.")
        
        //CallLogger.shared?.write("Call WebSocket will disconnect")
        print("Call WebSocket will disconnect")
        
        self.connectivityStatus = .notStart
        
        isForceClosed = true
        
        if starWebSocket != nil {
            starWebSocket!.disconnect()
            
            //handlerAfterSocketDisconnect()
        }
        
        ///starWebSocket = nil // after disconnect will call the disconnecttSocketHanlder() will set websocket nil, no need here
    }
    
    private func isTokenExpired() -> Bool? {
        
        if isValidText(self.config.tfNetwork.authToken) {
            
            if let expires_in = self.config.tfNetwork.authTokenExpires_in {
                
                if (Date.TFTimeInterval + 10800) > expires_in { //adding the 3 hours offset before actual expire
                    return true
                }
                else {
                    return false
                }
            }
        }
        return nil
    }
}

//MARK:- WEBSOCKET Send messsages
extension TFWebsocket {
    
    public func sendStringMessage(message: String) {
        
        if self.isConnected {
            
            self.starWebSocket?.write(string: message)
        }
        else {
            print("WebSocket did not connected. Plz connect the Websocket first")
        }
    }
    
    public func sendJSONMessage(data: Data) {
        
        if self.isConnected {
            
            self.starWebSocket?.write(data: data)
        }
        else {
            print("WebSocket did not connected. Plz connect the Websocket first")
        }
    }
}

//MARK:- Internet Connectivity Check
extension TFWebsocket {
    
    func internetNotifierObserver()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(networkStatusChanged(_:)), name: NSNotification.Name.reachabilityChanged, object: NetworkHelper.sharedInstance.reachability)
        NetworkHelper.sharedInstance.startNotifier()
    }
    
    @objc func networkStatusChanged(_ notification: Notification) {
        // Do something globally here!
        let reachable = notification.object as! Reachability
        
        guard !isForceClosed else {
            return
        }
        
        if reachable.connection != .none
        {
            //CallLogger.shared?.write("===networkStatusChanged called====")
            self.connectSocket()
        }
        else
        {
            self.closeSocket()
        }
    }
}

enum SocketConnectStatus {
    case notStart
    case inProgress
}

//MARK:- TFWebSocket Protocol
public protocol TFWebsocketDelegate: AnyObject {
    
    func socketDidReceivedEvent(eventJson:String, event: WebSocketEvent, client: WebSocket)
    
    func socketDidConnect(connect:Bool, event: WebSocketEvent, client: WebSocket)
    
    func socketDidDisconnect(reason:String,code: UInt16?, event: WebSocketEvent, client: WebSocket)
    
    func socketFailedWith(error:Error?, event: WebSocketEvent, client: WebSocket)
    
    func socketRenewAccessToken(renewToken:Bool,accessToken: String?, client: WebSocket?)
}


extension TFWebsocketDelegate { //optional delegates
    
    func socketDidConnect(connect:Bool, event: WebSocketEvent, client: WebSocket) {}
    
    func socketDidDisconnect(reason:String,code: UInt16?, event: WebSocketEvent, client: WebSocket) {}
    
    func socketFailedWith(error:Error?, event: WebSocketEvent, client: WebSocket) {}
    
}

/*extension TFWebsocket {
 
 public func addDelegate(delegate: TFWebsocketDelegate) {
 
 if !self.delegates.contains(where: {$0 === delegate }) {
 self.delegates.append(delegate)
 }
 }
 
 public func removeDelegate(delegate: TFWebsocketDelegate) {
 
 self.delegates.removeAll(where: {$0 ===  delegate })
 }
 
 public func removeAllDelegate(delegate: TFWebsocketDelegate) {
 self.delegates.removeAll()
 }
 }*/
