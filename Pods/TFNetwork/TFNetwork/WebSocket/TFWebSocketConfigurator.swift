//
//  TFWebSocketConfigurator.swift
//  TFNetwork
//
//  Created by Target on 01/09/2021.
//

import Foundation

struct TFWebSockeConfig {
    
    
    private static let scheme = "https://"
    
    var domainUrl:String = ""
    
    var tfNetwork: TFNetworkManager!
    
    mutating func configure(domainUrl: String, network: TFNetworkManager) {
        
        self.tfNetwork = network
        self.domainUrl = domainUrl
    }

}

extension TFWebSockeConfig {
    
    var socketURL: String {
        return "wss://api." + self.domainUrl + "/chat/ws"
    }
}
