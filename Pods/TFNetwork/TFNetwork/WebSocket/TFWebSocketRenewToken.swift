//
//  TFWebSocketRenewToken.swift
//  TFNetwork
//
//  Created by Waqas Rasheed on 08/09/2021.
//

import Foundation

extension TFWebsocket {
    
    func renewToken() {
        
        /*CallLogger.shared?.write("\n===============================\n")
         CallLogger.shared?.write("\nCall Socket Expired\n")
         CallLogger.shared?.write("\n===============================\n")*/
        guard self.config.tfNetwork != nil else { return }
        
        guard self.config.tfNetwork.isTokenRefreshedRecently == false else { return }
        
        isTokenRenew = true
        
        self.config.tfNetwork.requestNewToken { [weak self] isSuccess in
            
            guard let strongSelf = self else {return}
            
            strongSelf.delegate.invoke({
                $0.socketRenewAccessToken(renewToken: isSuccess, accessToken: strongSelf.config.tfNetwork.authToken, client: strongSelf.webSocket)
            })
            
            if isSuccess {
                
                //CallLogger.shared?.write("\n token updated and going to connect the socket \n")
                
                //CallApiManager.shared.resetCredentials()
                //CallApiManager.shared.updateTokenCallApiManager(token: UserInfo.userAccessToken.accessToken)
                
                
                strongSelf.connectSocket()
                
                //AppUtility.sentEvent(event: "Renew_Token_Successfully_Called", eventPram: [:])
                
                //guard let id = strongSelf.incommingCallId else {return}
                //guard let id = CallKitManager.shared.currentCallId else {return}
                
                
                //CallLogger.shared?.write("After Renew_Token incommingCallId checking the call Status")
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    //AppUtility.sentEvent(event: "Check_CallStatus_Token_Renewal", eventPram: [:])
                    //CallApiManager.shared.ignoreIncommingCallIfStatusAvaialble(callId: id)
                }
                
            }
            else {
                //CallLogger.shared?.write("\n renewToken token failed for call socket \n")
                //CallApiManager.shared.resetCredentials()  // if another api get the token then reset it.
                //strongSelf.connectSocket()
                //AppUtility.sentEvent(event: "Renew_Token_Failed", eventPram: [:])
                
                strongSelf.handleCallBack(isConnect: false)
            }
            
        }
        
        /*
         self.config.tfNetwork.renewAuthToken { [weak self] (isSuccess) in
         
         guard let strongSelf = self else {return}
         }*/
    }
}
