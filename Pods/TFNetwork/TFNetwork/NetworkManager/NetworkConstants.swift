//
//  NetworkConstants.swift
//  BloomingBuyer
//
//  Created by Waqas Rasheed on 18/03/2021.
//

import SwiftyJSON
import OAuthSwift

public typealias serviceCompletionerHandler = (_ result:JSON?,_ error:ErrorModel? ) ->Void
public typealias jobServiceCompletionerHandler = (_ result:Any?,_ errorTitle:String? ,_ errorDescription:String? ) ->Void

public enum ServiceType:String {
    case POST = "POST"
    case GET = "GET"
    case PUT = "PUT"
    case DELETE = "DELETE"
}

public enum ResponseType {
    case json
    case data
    case plain
}

enum ResponceType:String {
    case JSON = "JSON"
    case TEXT = "TEXT"
    case SimpleJSON = "SimpleJSON"
}

enum RequestResult: Error {
    case Failed(error:ErrorModel?)
}

extension String {
    var encodeUrl:String? {
        
        if let urlStr = self.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        {
            return urlStr
        }
        return nil
    }
    
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    
    var pathExtension: String {
        get {
            return (self as NSString).pathExtension
        }
    }
    
    func queryStringUrl (queryPrameters: [String:String]?) -> String {
        
        let queryStringParamter = queryPrameters
        
        var urlStrVar:String = self
        
        if let params = queryStringParamter
        {
            var components = URLComponents(string: urlStrVar)
            components?.queryItems = params.map{
                URLQueryItem(name: $0.key, value: $0.value)
            }
            
            if let str = components?.url?.absoluteString
            {
                urlStrVar  = str
            }
        }
        return urlStrVar
    }
}

extension NSMutableData {
    
    func appendString(string: String) {
        
        let data = string.data(using: String.Encoding.utf8)!
        append(data)
    }
}

extension URLComponents{
    var queryItemsDictionary : [String:String]{
        set (queryItemsDictionary){
            self.queryItems = queryItemsDictionary.map {
                URLQueryItem(name: $0, value: "\($1)")
            }
        }
        get{
            var params = [String: String]()
            return queryItems?.reduce([:], { (_, item) -> [String: String] in
                params[item.name] = item.value
                return params
            }) ?? ["":""]
        }
    }
}

public protocol TFNetworkManagerDelegate: AnyObject {
    
    func didFailedRenewToken() // token renew failed
    func didAccessTokenChanged(token: TFAuth.TFTokenModel) //api renew the token model due to token expire 401 error
}

public extension TFNetworkManagerDelegate {
    func didFailedRenewToken() { }
    func didAccessTokenChanged(token: TFAuth.TFTokenModel) { }
}
