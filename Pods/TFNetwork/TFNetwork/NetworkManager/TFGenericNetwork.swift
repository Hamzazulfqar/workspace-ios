//
//  TFGenericNetwork.swift
//  BloomingBuyer
//
//  Created by Waqas Rasheed on 23/03/2021.
//

import UIKit

public struct TFGenericNetwork<T: Codable>{
   
    public static func fetch(apiRequest: TFRequest, tfNetworkManager: TFNetworkManager = TFNetwork, WithDelegate delegate:UIViewController? = nil, completion:@escaping (Result<T?,ErrorModel>) -> Void) {
         
        tfNetworkManager.asyncCall(apiRequest: apiRequest, requiredResponce: .data, WithDelegate: delegate) { (tfResponse) in
            
            guard tfResponse.error == nil else {
                completion(.failure(tfResponse.error!))
                return
            }
            
            guard let data = tfResponse.data else {
                completion(.failure(ErrorModel(errorTitle: "", errorDescp: "empty data", error: nil)))
                return
            }
            
            do {
                if let d = data as? Data {
                    let json = try JSONDecoder().decode(T.self, from: d)
                    completion(.success(json))
                } else {
                    completion(.failure(ErrorModel(errorTitle: "", errorDescp: "empty data", error: nil)))
                }
            }
            catch (let error) {
                completion(.failure(ErrorModel(errorTitle: "Decoding failed", errorDescp: error.localizedDescription, error: error)))
            }
        }
    }
}

//extension TFNetworkManager {
//
//    func fetch<T: Codable> (apiRequest: TFRequest, networkManager: TFNetworkManager = TFNetwork, WithDelegate delegate:UIViewController? = nil, completion:@escaping (Result<T?,ErrorModel>) -> Void) {
//        //TFGenericNetwork<T>.fetch(apiRequest: apiRequest, networkManager: networkManager, WithDelegate: delegate, completion: completion);
//
//        TFGenericNetwork.fetch(apiRequest: apiRequest, networkManager: networkManager, WithDelegate: delegate) { (result) in
//            completion(result)
//        }
//    }
//}

