//
//  ErrorModel.swift
//  Qais Social
//
//  Created by Muhammad Arslan Khalid on 22/10/2018.
//  Copyright © 2018 Target OFS. All rights reserved.
//

public typealias TFError = ErrorModel

public struct ErrorModel: Error {
    
    public var errorTitle:String!
    public var errorDescp:String!
    public var error:Error?
    public var errorCode:Int!
    
    init() {
        self.errorDescp = ""
        self.errorDescp = ""
        self.errorCode = 0
    }
    init(errorTitle:String, errorDescp:String, error:Error?, errorCode:Int = 0) {
        self.errorTitle = errorTitle
        self.errorDescp = errorDescp
        self.error = error
        self.errorCode = errorCode
    }
}
