//
//  TFRequest.swift
//  BloomingBuyer
//
//  Created by Waqas Rasheed on 19/03/2021.
//

import Alamofire
import TUSKit
public typealias tfServiceCompletionerHandler = (_ result: TFResponse) ->Void
public typealias apiCallBack = ((DataResponse<Any,AFError>?, DataResponse<Data,AFError>?,DataResponse<String,AFError>?) -> Void)
var AuthTokenTests: String? = nil

public struct TFRequest {
    
    var baseUrl: String
    var path: String?
    var quertyString: [String: String]?
    let header: HTTPHeaders?
    let method: HTTPMethod
    let bodyPerameter: Any? //[String: Any]?
    
    public init(baseUrl: String, method: HTTPMethod = .get, bodyPerameter: Any? = nil) {
        self.baseUrl = baseUrl
        self.method = method
        self.header = nil
        self.bodyPerameter = bodyPerameter
        self.path = nil
        self.quertyString = nil
    }
    
    public init(baseUrl: String, method: HTTPMethod, bodyPerameter: Any? = nil, header: HTTPHeaders) {
        self.baseUrl = baseUrl
        self.method = method
        self.header = header
        self.bodyPerameter = bodyPerameter
        self.path = nil
        self.quertyString = nil
    }
    
    public init(baseUrl: String, path: String, method: HTTPMethod = .get, bodyPerameter: Any? = nil) {
        self.baseUrl = baseUrl
        self.method = method
        self.header = nil
        self.bodyPerameter = bodyPerameter
        self.path = path
        self.quertyString = nil
    }
    
    public init(baseUrl: String, path: String, method: HTTPMethod, bodyPerameter: Any? = nil, header: HTTPHeaders?) {
        self.baseUrl = baseUrl
        self.method = method
        self.header = header
        self.bodyPerameter = bodyPerameter
        self.path = path
        self.quertyString = nil
    }
    
    public init(baseUrl: String, path: String, method: HTTPMethod = .get, quertyString: [String: String]?) {
        self.baseUrl = baseUrl
        self.method = method
        self.header = nil
        self.bodyPerameter = nil
        self.path = path
        self.quertyString = quertyString
    }
    
    public init(baseUrl: String, path: String, method: HTTPMethod, quertyString: [String: String]?, header: HTTPHeaders) {
        self.baseUrl = baseUrl
        self.method = method
        self.header = header
        self.bodyPerameter = nil
        self.path = path
        self.quertyString = quertyString
    }
    
    public init(baseUrl: String, path: String, method: HTTPMethod, quertyString: [String: String]?, bodyPerameter:Any? = nil,header: HTTPHeaders? = nil) {
        self.baseUrl = baseUrl
        self.method = method
        self.header = header
        self.bodyPerameter = bodyPerameter
        self.path = path
        self.quertyString = quertyString
      }
    
    mutating func setQueryString(value: [String:String]) {
        self.quertyString = value
    }
    
//    mutating func setHeader(header: HTTPHeaders) {
//        self.header = header
//    }
    
    var url: URL? {
        
        if let p = path {
            let encodedUrlString = (baseUrl + p).encodeUrl ?? ""
            return URL(string:encodedUrlString.queryStringUrl(queryPrameters: quertyString))
        }
        
        let encodedUrlString = baseUrl.encodeUrl ?? ""
        return URL(string: encodedUrlString.queryStringUrl(queryPrameters: quertyString))
    }
}

public struct TFResponse {
    
    public var data: Any?
    public var responceType: ResponseType
    public var error: ErrorModel?
    public var statusCode: Int?
    
    public var request: URLRequest?
    
    init() {
        data = nil
        responceType = .json
        error = nil
        statusCode = nil
        request = nil
    }
    
    init(error: ErrorModel) {
        data = nil
        responceType = .json
        self.error = error
        statusCode = nil
        request = nil
    }
    
    public var curlDescription : String? {
        return request?.curlString
    }
}

extension TFRequest {
    
    public static func getSSOHeader(contentType: String = "application/json", basicToken: String = "mobile-app:ZXhhbXBsZS1hcHAtc2VjcmV1") -> HTTPHeaders {
        var header: HTTPHeaders  = HTTPHeaders()
        header["Authorization"] = "Basic \(basicToken.toBase64())";
        header["Content-Type"] = contentType
        return header
    }
    
    public static func getAuthHeader(authToken: String, contentType: String = "application/json") -> HTTPHeaders? {

        var header: HTTPHeaders?
        if authToken.count > 0 {
            header = HTTPHeaders()
            header!["Authorization"] = "Bearer \(authToken)"
            header!["Content-Type"] = contentType
        }
        
        return header
    }
    
    /*static func getAuthHeader(authToken: String? = nil, contentType: String = "application/json") -> HTTPHeaders? {
        
        var token = authToken ?? AuthTokenTests
        if token == nil {
            token = UserInfo.userAccessToken.accessToken
        }
        
        var header: HTTPHeaders?
        if let t = token, t.count > 0 {
            header = HTTPHeaders()
            header!["Authorization"] = "Bearer \(t)"
            header!["Content-Type"] = contentType
        }
        
        return header
    }*/
    
    public static func contentTypeHeader(contentType: String = "application/json") -> HTTPHeaders {
        
        var header: HTTPHeaders = HTTPHeaders()
        header["Content-Type"] = contentType
        return header
    }
    
    func valedateQueryStringInRequest() -> Bool {
        
        if let url = self.url {
            
            let queryItems = URLComponents(string: url.absoluteString)?.queryItems
            if let _ = queryItems?.filter({$0.name == "access_token"}).first {
                print("access_token key found in url")
                
                guard let _ = self.quertyString?.filter({$0.key == "access_token"}).first else {
                    //assertionFailure("access_token set in TFRequest property quertyString instead of url path")
                    return false
                }
            }
        }
        return true
    }
    
    func fixQueryStringRequest() -> TFRequest {
        
        var newTfReq = self
        
        if let url = self.url {
            
            var urlComponent = URLComponents(string: url.absoluteString)
            guard let queryItems = urlComponent?.queryItems else {
                return newTfReq
            }
            
            if let _ = queryItems.filter({$0.name == "access_token"}).first {
                print("access_token key found in url")
                
                if self.quertyString?.filter({$0.key == "access_token"}).first == nil {
                    print("access_token not in the TFRequest property quertyString")
                    
                    var queryString = self.quertyString;
                    
                    if queryString == nil {
                        queryString = [String: String]()
                    }

                    /*let muteAbleQueryItems = queryItems
                    
                    if let item = muteAbleQueryItems.filter({$0.name == "access_token"}).first {
                        if let token = item.value {
                            
                            let removeToken = "access_token=\(token)"
                            
                            newTfReq.baseUrl = newTfReq.baseUrl.replacingOccurrences(of: removeToken, with: "")
                            newTfReq.path = newTfReq.path?.replacingOccurrences(of: removeToken, with: "")
                        }
                    }*/

                    
                    for qit in queryItems {
                        
                        let key = qit.name
                        let val = qit.value ?? ""
                        
                        let removeValue = "\(key)=\(val)"
                        
                        newTfReq.baseUrl = newTfReq.baseUrl.replacingOccurrences(of: removeValue, with: "")
                        newTfReq.path = newTfReq.path?.replacingOccurrences(of: removeValue, with: "")
                    }
                    
                    newTfReq.baseUrl = newTfReq.baseUrl.replacingOccurrences(of: "&", with: "")
                    newTfReq.path = newTfReq.path?.replacingOccurrences(of: "&", with: "")
                    
                    newTfReq.baseUrl = newTfReq.baseUrl.replacingOccurrences(of: "?", with: "")
                    newTfReq.path = newTfReq.path?.replacingOccurrences(of: "?", with: "")
                    
                    let queryDic = urlComponent!.queryItemsDictionary
                    newTfReq.setQueryString(value: queryDic)
                    urlComponent?.queryItems?.removeAll()
                    
                    //print(newTfReq.url?.host ?? "")
                    //print(newTfReq.url?.path ?? "")
                    //print(newTfReq.url?.query ?? "")
                }
            }
        }
        return newTfReq
    }
}

extension URLRequest {
    
    var curlString: String {
        guard let url = url else { return "" }
        
        var baseCommand = #"curl "\#(url.absoluteString)""#
        
        if httpMethod == "HEAD" {
            baseCommand += " --head"
        }
        
        var command = [baseCommand]
        
        if let method = httpMethod, method != "HEAD" {
            command.append("-X \(method)")
        }
        
        if let headers = allHTTPHeaderFields {
            for (key, value) in headers where key != "Cookie" {
                command.append("-H '\(key): \(value)'")
            }
        }
        
        if let data = httpBody, let body = String(data: data, encoding: .utf8) {
            command.append("-d '\(body)'")
        }
        
        return command.joined(separator: " \\\n\t")
    }
    
}
