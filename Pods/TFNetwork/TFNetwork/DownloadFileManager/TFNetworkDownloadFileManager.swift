//
//  DownloadFileManager.swift
//  TFNetwork
//
//  Created by Muhammad Arslan Khalid on 12/08/2021.
//

import UIKit
import RxCocoa
import RxSwift

public let downloadManager = TFNetworkDownloadFileManager.shared;

public class TFNetworkDownloadFileManager: NSObject {
    
    static let shared = TFNetworkDownloadFileManager();
    private var accessToken:String!
    private var downloadFileSessionResponse:TFNetworkBackgroundDownloadManager!
    private var operationQueue = OperationQueue()
    
    
    override private init() {
        super.init()
        self.downloadFileSessionResponse = TFNetworkBackgroundDownloadManager()
        
    }
    
    public func setAccessToken(accessToken:String) {
        self.accessToken = accessToken;
        self.downloadFileSessionResponse.setAccessToken(accessToken: self.accessToken);
    }
    
    public func setbackgroundSessionHandler(backgroundSessionCompletionHandler: (() -> Void)?) {
        self.downloadFileSessionResponse.setbackgroundSessionHandler(backgroundSessionCompletionHandler: backgroundSessionCompletionHandler);
    }
}

extension TFNetworkDownloadFileManager {
    public func downloadData(fileName:String, fileUrl:URL, progress:((_ progress:Float,_ totalSize : String) ->Void)?,completion:@escaping(_ result: Data?,_ error: Error? ,_ response:URLResponse?)->Void){
        let operation = TFNetworkFileDownloadManagerOperation(
            fileName: fileName,
            fileUrl: fileUrl,
            downloadFileSessionResponse: self.downloadFileSessionResponse,
            progress: progress,
            completion: completion);
        self.operationQueue.addOperation(operation);
    }
}



class TFNetworkFileDownloadManagerOperation:Operation{
    
    
    
    private var taskIdentifier:Int!
    private var fileName:String!
    private var fileURL:URL!
    private var progress:((_ progress:Float,_ totalSize : String) ->Void)?
    private var completion:((_ result: Data?,_ error: Error? ,_ response:URLResponse?)->Void)!
    private var downloadFileSessionResponse:TFNetworkBackgroundDownloadManager!
    private var disposeBag = DisposeBag()
    
    
    var uploaded: Bool = false {
        willSet {
            willChangeValue(forKey: "isFinished")
        }
        didSet {
            didChangeValue(forKey: "isFinished")
            if uploaded == true {
                print("Custom Async operation has finished")
            }
        }
    }
    var uploading: Bool = false {
        willSet {
            willChangeValue(forKey: "isExecuting")
        }
        didSet {
            didChangeValue(forKey: "isExecuting")
            if uploading == true {
                print("Custom Async operation is executing")
            } else {
                print("Custom Async method is not executing")
            }
        }
    }
    
    deinit {
        print("TusUploaderOperation - Deinit")
    }
    
    
    init(fileName:String, fileUrl:URL,downloadFileSessionResponse:TFNetworkBackgroundDownloadManager,progress:((_ progress:Float,_ totalSize : String) ->Void)?,completion:@escaping(_ result: Data?,_ error: Error? ,_ response:URLResponse?)->Void) {
        self.fileName = fileName
        self.fileURL = fileUrl
        self.progress = progress
        self.completion = completion
        self.downloadFileSessionResponse = downloadFileSessionResponse
        
    }
    
    
    override func main() {
        self.startDownloading()
    }
    
    
    
    func startDownloading(){
        if let dic =  downloadFileSessionResponse.getDownloadDicInfo(WithFileName: self.fileName), dic[TFNetworkBackgroundDownloadManager.DownloadDicKey.isDownloading] as! Bool == true {
            
            self.addObserverForDownloadData(processTaskIdentifier: dic[TFNetworkBackgroundDownloadManager.DownloadDicKey.taskIdentifer] as! Int)
        }
        else if let dic =  downloadFileSessionResponse.getDownloadDicInfo(WithFileName: self.fileName),dic[TFNetworkBackgroundDownloadManager.DownloadDicKey.isDownloading] as! Bool == false, let data = dic[TFNetworkBackgroundDownloadManager.DownloadDicKey.resumeData] as? Data {
            
            let processTaskIdentifer =   downloadFileSessionResponse.resumeTaskWithData(data: data)
            self.addObserverForDownloadData(processTaskIdentifier: processTaskIdentifer)
        }
        else {
            
            let processTaskIdentifer = downloadFileSessionResponse.downloadData(fileName: self.fileName, fileUrl: self.fileURL)
            
            downloadFileSessionResponse.addDownloadDataInUserDefault(fileName: self.fileName, fileURL: self.fileURL, taskIdentiifer: processTaskIdentifer)
            
            self.addObserverForDownloadData(processTaskIdentifier: processTaskIdentifer)
            
        }
        
    }
    
    
    func addObserverForDownloadData(processTaskIdentifier:Int){
        self.taskIdentifier  = processTaskIdentifier;
        
        DispatchQueue.main.async {
            self.downloadFileSessionResponse.progressHandler.subscribe { [weak self] (progress, totalSize, taskIdentfier) in
                guard let self = self else {return}
                guard processTaskIdentifier == taskIdentfier else {return}
                self.progress?(progress,totalSize);
                
            } onError: { (error) in
                
            } onCompleted: {
                
            } onDisposed: {
                
            }
            .disposed(by: self.disposeBag)
            
            self.downloadFileSessionResponse.completionHandlerForPOST.subscribe { [weak self] (result, error, response, taskIdentfier) in
                guard let self = self else {return}
                guard processTaskIdentifier == taskIdentfier else {return}
                DispatchQueue.main.async {
                    self.completion(result,error,response);
                    self.completeOperation()
                }
            } onError: { (error) in
                
            } onCompleted: {
                
            } onDisposed: {
                
            }
            .disposed(by: self.disposeBag)
        }
    }
    
    
    override var isFinished: Bool {
        return uploaded
    }
    
    override var isExecuting: Bool {
        return uploading
    }
    
    override var isAsynchronous: Bool {
        return true
    }
    
    private func completeOperation() {
        uploading = false
        uploaded = true
    }
}




