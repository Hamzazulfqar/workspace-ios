//
//  DownloadFileSessionReponse.swift
//  TFNetwork
//
//  Created by Muhammad Arslan Khalid on 12/08/2021.
//

import UIKit
import RxCocoa
import RxSwift
import PromiseKit
class TFNetworkBackgroundDownloadManager: NSObject {
    
    
    private var arrDownloadTasks = [URLSessionDownloadTask]()
    private var backgroundTask: UIBackgroundTaskIdentifier = .invalid
    var completionHandlerForPOST = PublishSubject<(result: Data?,error: Error? ,response:URLResponse?, taskIdentfier:Int)>()
    var progressHandler = PublishSubject<(progress:Float,totalSize : String,taskIdentfier:Int)>()
    private var documentDirectory:URL!
    {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
    
    struct DownloadDicKey {
        static var taskIdentifer = "tfnetworkDownloadTaskIdentifier"
        static var fileName = "tfnetworkDownloadFileName"
        static var fileURL = "tfnetworkDownloadFileURL"
        static var resumeData = "tfnetworkResumeDownloadData"
        static var isDownloading = "isTFNetworkDownloadingInProgress"
        static var downloadProcesses = "tfnetworkDownloadDataProcesses"
    }
    
    private var backgroundSessionCompletionHandler: (() -> Void)?
    private var accessToken:String!
    override init() {
        super.init();
        _ = self.downloadsSession
    }
    
    
    func setbackgroundSessionHandler(backgroundSessionCompletionHandler: (() -> Void)?) {
        self.backgroundSessionCompletionHandler  = backgroundSessionCompletionHandler;
    }
    
    func setAccessToken(accessToken:String) {
        self.accessToken = accessToken;
    }
    
    
    lazy var downloadsSession: URLSession = {
        let configuration =
            URLSessionConfiguration.background(withIdentifier:
                                                "com.target.TFNetworkBackgroundSessionManager.1234_456")
        configuration.sessionSendsLaunchEvents = true
        return URLSession(configuration: configuration,
                          delegate: self,
                          delegateQueue: nil)
    }()
    
    
}

//MARK:- Utility Methods
extension TFNetworkBackgroundDownloadManager {
    func downloadData(fileName:String, fileUrl:URL) -> Int {
        var request = URLRequest(url: fileUrl)
        request.httpMethod = "GET"
        if isValidText(self.accessToken)
        {
            request.setValue("Bearer \(self.accessToken!)", forHTTPHeaderField:"Authorization" )
            
        }
        else{
            assertionFailure("access token is missing");
        }
        
        
        let task = self.downloadsSession.downloadTask(with: request)
        task.countOfBytesClientExpectsToSend = 200
        task.countOfBytesClientExpectsToReceive = 500 * 1024
        task.resume()
        self.arrDownloadTasks.append(task)
        return task.taskIdentifier
    }
    
    func checkResumeData(){
        if var arr = UserDefaults.standard.value(forKey: DownloadDicKey.downloadProcesses) as? [[String:Any]] {
            for (index,dic) in arr.enumerated() {
                if let data = dic[DownloadDicKey.resumeData] as? Data {
                    var dic = dic
                    dic[DownloadDicKey.taskIdentifer] = self.resumeTaskWithData(data: data)
                    arr[index] = dic
                    UserDefaults.standard.set(arr, forKey: DownloadDicKey.downloadProcesses)
                    UserDefaults.standard.synchronize()
                }
            }
            
        }
    }
    
    
    func resumeTaskWithData(data:Data) -> Int{
        let task = self.downloadsSession.downloadTask(withResumeData: data)
        task.resume()
        self.arrDownloadTasks.append(task)
        return task.taskIdentifier
    }
    
    
    func cancelDownloadTask(){
        guard self.arrDownloadTasks.count > 0 else {
            return
        }
        
        self.registerBackgroundTask(from: "BackgeoundDownloadingSession")
        var actions = [Promise<Bool>]()
        for val in self.arrDownloadTasks {
            actions.append(self.cancelSingleDownloadTask(task: val))
        }
        
        _ = when(fulfilled: actions)
            .done({ (data) in
                self.endBackgroundTask()
            })
        
        
        
    }
    
    
    private func cancelSingleDownloadTask(task:URLSessionDownloadTask) -> Promise<Bool>{
        
        return Promise<Bool> { seal in
            task.cancel { (resumeData) in
                if let resumeReqData = resumeData {
                    self.addResumeDataToDownloadDicInfo(data: resumeReqData, taskIdentiifer: task.taskIdentifier)
                    seal.resolve(true, nil)
                }
                else{
                    self.removeDownloadDicInfo(taskIdentiifer: task.taskIdentifier)
                    seal.resolve(true, nil)
                }
            }
        }
        
        
    }
    
    func resumePendingTasks(){
        var removeIndex = [Int]()
        if var arr = UserDefaults.standard.value(forKey: DownloadDicKey.downloadProcesses) as? [[String:Any]] {
            for (index,dic) in arr.enumerated() {
                if let data = dic[DownloadDicKey.resumeData] as? Data {
                    var dic = dic
                    dic[DownloadDicKey.taskIdentifer] = self.resumeTaskWithData(data: data)
                    arr[index] = dic
                    UserDefaults.standard.set(arr, forKey: DownloadDicKey.downloadProcesses)
                    UserDefaults.standard.synchronize()
                    
                }
                else{
                    removeIndex.append(index)
                }
            }
        }
        
        if removeIndex.count > 0 {
            if var arr = UserDefaults.standard.value(forKey: DownloadDicKey.downloadProcesses) as? [[String:Any]] {
                for index in removeIndex {
                    arr.remove(at: index)
                }
                
                if arr.count > 0 {
                    UserDefaults.standard.set(arr, forKey: DownloadDicKey.downloadProcesses)
                }
                else{
                    UserDefaults.standard.removeObject(forKey: DownloadDicKey.downloadProcesses)
                }
                
                UserDefaults.standard.synchronize()
                
            }
        }
    }
    
    func sendError(_ error: String, taskIdentifier:Int) {
        print(error)
        let userInfo = [NSLocalizedDescriptionKey : error]
        self.completionHandlerForPOST.onNext((result: nil, error: NSError(domain: "taskForPOSTMethod", code: 1, userInfo: userInfo), response: nil, taskIdentfier: taskIdentifier))
        
    }
    
    func showLocalNotification(title:String, subtitle:String, id:String,info:[String:Any])
    {
        let notificationContent = UNMutableNotificationContent()
        
        
        notificationContent.title = title
        notificationContent.body = subtitle
        notificationContent.sound = .default
        notificationContent.userInfo = info
        let triger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        let request = UNNotificationRequest(identifier: id, content: notificationContent, trigger: triger)
        UNUserNotificationCenter.current().add(request) { (error) in
            
        }
    }
    
    func registerBackgroundTask(from: String) {
        
        endBackgroundTask()
        backgroundTask = UIApplication.shared.beginBackgroundTask{ [weak self] in
            
            self?.endBackgroundTask()
        }
        
        assert(backgroundTask != .invalid)
    }
    
    func endBackgroundTask() {
        //print("Background task ended.")
        if backgroundTask != .invalid {
            UIApplication.shared.endBackgroundTask(backgroundTask)
            backgroundTask = .invalid
        }
        
    }
    
    
}

//MARK:- Local Storage Utility Methods
extension TFNetworkBackgroundDownloadManager{
    func addDownloadDataInUserDefault(fileName:String, fileURL:URL, taskIdentiifer:Int) {
        let dic:[String:Any] = [DownloadDicKey.fileName:fileName,DownloadDicKey.fileURL:fileURL.absoluteString, DownloadDicKey.taskIdentifer:taskIdentiifer, DownloadDicKey.isDownloading:true]
        if var arr = UserDefaults.standard.value(forKey: DownloadDicKey.downloadProcesses) as? [[String:Any]] {
            arr.append(dic)
            UserDefaults.standard.set(arr, forKey: DownloadDicKey.downloadProcesses)
        }
        else{
            UserDefaults.standard.set([dic], forKey: DownloadDicKey.downloadProcesses)
        }
        UserDefaults.standard.synchronize()
    }
    
    func getDownloadDic(WithTaskIdentifer taskIndentifier:Int) -> [String:Any]?{
        if let arr = UserDefaults.standard.value(forKey: DownloadDicKey.downloadProcesses) as? [[String:Any]] {
            let result = arr.filter { (dataDic) -> Bool in
                (dataDic[DownloadDicKey.taskIdentifer]  as! Int) == taskIndentifier
            }
            
            return result.first
        }
        return nil
    }
    func getDownloadDicInfo(WithFileName fileName:String) -> [String:Any]? {
        
        if let arr = UserDefaults.standard.value(forKey: DownloadDicKey.downloadProcesses) as? [[String:Any]] {
            let result = arr.filter { (dataDic) -> Bool in
                (dataDic[DownloadDicKey.fileName]  as! String) == fileName
            }
            if result.count > 0 {
                let val = result.first!
                return val
            }
        }
        return nil
        
        
    }
    
    func addResumeDataToDownloadDicInfo(data:Data, taskIdentiifer:Int){
        if var arr = UserDefaults.standard.value(forKey: DownloadDicKey.downloadProcesses) as? [[String:Any]] {
            let result = arr.filter { (dataDic) -> Bool in
                (dataDic[DownloadDicKey.taskIdentifer]  as! Int) == taskIdentiifer
            }
            
            if result.count > 0, var dic = result.first {
                dic[DownloadDicKey.resumeData] = data
                dic[DownloadDicKey.isDownloading] = false
                
                let index = arr.firstIndex { (dataDic) -> Bool in
                    (dataDic[DownloadDicKey.taskIdentifer]  as! Int) == taskIdentiifer
                }
                
                if let index = index {
                    arr[index] = dic
                }
                
                UserDefaults.standard.set(arr, forKey: DownloadDicKey.downloadProcesses)
                UserDefaults.standard.synchronize()
                
            }
        }
    }
    
    func removeDownloadDicInfo(taskIdentiifer:Int) {
        if var arr = UserDefaults.standard.value(forKey: DownloadDicKey.downloadProcesses) as? [[String:Any]] {
            arr.removeAll { (dic) -> Bool in
                (dic[DownloadDicKey.taskIdentifer]  as! Int) == taskIdentiifer
            }
            if arr.count > 0 {
                UserDefaults.standard.set(arr, forKey: DownloadDicKey.downloadProcesses)
            }
            else{
                UserDefaults.standard.removeObject(forKey: DownloadDicKey.downloadProcesses)
            }
            
            UserDefaults.standard.synchronize()
        }
    }
    
    func writePdfDataToLocalFile(taskIdentifier:Int, data:Data) {
        guard let downloadData = self.getDownloadDic(WithTaskIdentifer: taskIdentifier) else {
            return
        }
        let reqValue = self.documentDirectory.appendingPathComponent(downloadData[DownloadDicKey.fileName] as! String)
        if !FileManager.default.fileExists(atPath: reqValue.absoluteString) {
            
            
            
            do{
                try data.write(to: reqValue)
            }
            catch {
                print(error.localizedDescription);
            }
        }
    }
    
}


//MARK:- URL Session Download Data Delegates
extension TFNetworkBackgroundDownloadManager:URLSessionDownloadDelegate,URLSessionTaskDelegate {
    
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask,
                    didWriteData bytesWritten: Int64, totalBytesWritten: Int64,
                    totalBytesExpectedToWrite: Int64) {
        
        let progress =  Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        let totalSize =
            ByteCountFormatter.string(fromByteCount: totalBytesExpectedToWrite,
                                      countStyle: .file)
        self.progressHandler.onNext((progress: progress, totalSize: totalSize, taskIdentfier: downloadTask.taskIdentifier))
        
    }
    
    
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
        self.arrDownloadTasks.removeAll { (task) -> Bool in
            task.taskIdentifier == downloadTask.taskIdentifier
        }
        
        guard (downloadTask.error == nil) else {
            sendError("\(downloadTask.error!.localizedDescription)", taskIdentifier: downloadTask.taskIdentifier)
            self.removeDownloadDicInfo(taskIdentiifer: downloadTask.taskIdentifier)
            return
        }
        
        guard let statusCode = (downloadTask.response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
            
            let errorString = ApiError.init(statusCode: ((downloadTask.response
                                                            as? HTTPURLResponse)?.statusCode)!)
            
            print("StatusCode: \((downloadTask.response as? HTTPURLResponse)!.statusCode), Reason: \(errorString)")
            
            sendError("StatusCode: \((downloadTask.response as? HTTPURLResponse)!.statusCode), Reason: \(errorString)", taskIdentifier: downloadTask.taskIdentifier)
            self.removeDownloadDicInfo(taskIdentiifer: downloadTask.taskIdentifier)
            return
        }
        
        do{
            let data =  try Data(contentsOf: location)
            self.writePdfDataToLocalFile(taskIdentifier: downloadTask.taskIdentifier, data: data)
            self.removeDownloadDicInfo(taskIdentiifer: downloadTask.taskIdentifier)
            self.completionHandlerForPOST.onNext((result: data, error: nil, response: downloadTask.response, taskIdentfier: downloadTask.taskIdentifier))
            
        }
        catch let error {
            self.removeDownloadDicInfo(taskIdentiifer: downloadTask.taskIdentifier)
            self.completionHandlerForPOST.onNext((result: nil, error: error, response: downloadTask.response, taskIdentfier: downloadTask.taskIdentifier))
            
        }
        
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        self.registerBackgroundTask(from: "BackgeoundDownloadingSession")
        self.arrDownloadTasks.removeAll { (downloadTask) -> Bool in
            downloadTask.taskIdentifier == task.taskIdentifier
        }
        if error != nil {
            let userInfo = (error! as NSError).userInfo
            
            if let resumeData = userInfo[NSURLSessionDownloadTaskResumeData] as? Data {
                self.addResumeDataToDownloadDicInfo(data: resumeData, taskIdentiifer: task.taskIdentifier)
                
            }
            else{
                self.removeDownloadDicInfo(taskIdentiifer: task.taskIdentifier)
            }
        }
        else{
            
            //  self.showLocalNotification(title: "Downlod Status", subtitle: "Downloading completed", id: "\(task.taskIdentifier)", info: self.getDownloadDic(WithTaskIdentifer: task.taskIdentifier) ?? [String:Any]())
            
        }
        self.endBackgroundTask()
        
        
        
    }
    
    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        DispatchQueue.main.async {
            if let completionHandler = self.backgroundSessionCompletionHandler {
                self.backgroundSessionCompletionHandler = nil
                completionHandler()
            }
        }
    }
    
}


enum ApiError: Int {
    case Info = 100
    case Success = 200
    case Redirect = 300
    case Client = 400
    case ServerError = 500
    case Unknown = 999
    
    
    init(statusCode: Int) {
        switch statusCode {
        case 100...199: self = .Info
        case 200...299: self = .Success
        case 300...399: self = .Redirect
        case 400...499: self = .Client
        case 500...599: self = .ServerError
        default: self = .Unknown
        }
    }
}
