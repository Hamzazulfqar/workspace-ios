//
//  UserInfo.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation

//Defaults Keys
let userDefaultsWorkspaceIdKey = "userDefaultsWorkspaceIdKey"
class RememberMe: NSObject {
    
    var saveEmail: String? {
        get {
            return UserDefaults.standard.value(forKey: "saveEmail") as? String
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "saveEmail")
        }
    }
    
    var savePassword: String? {
        get {
            return UserDefaults.standard.value(forKey: "savePassword") as? String
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "savePassword")
        }
    }
    
    static func removeSaveData()
    {
        let arrUserInfoKeys = ["saveEmail","savePassword"];
        
        for keys in arrUserInfoKeys {
            UserDefaults.standard.removeObject(forKey: keys)
        }
    }
}

class UserInfo: NSObject {
    
    static var defaultWorkspaceId: String? {
        get {
            return UserDefaults.standard.value(forKey: userDefaultsWorkspaceIdKey) as? String
        }
        set {
            UserDefaults.standard.set(newValue, forKey: userDefaultsWorkspaceIdKey)
        }
    }
    
    static var id:String {
        get {
            let data =  UserDefaults.standard.value(forKey: "user_id") as? String
            return data != nil ? data!:""
        } set {
            UserDefaults.standard.set(newValue, forKey: "user_id")
        }
    }
    
    static var userId:String
    {
        get
        {
            let data =  UserDefaults.standard.value(forKey: "userId") as? String
            return data != nil ? data!:""
        }
        set
        {
            UserDefaults.standard.set(newValue, forKey: "userId")
        }
    }
    
    static var organisationID:String?
    {
        get
        {
            let data =  UserDefaults.standard.value(forKey: "organisationID") as? String
            return data
        }
        set
        {
            UserDefaults.standard.set(newValue, forKey: "organisationID")
        }
    }
    
    static var userOrganisationCount:Int
    {
        get
        {
            let data = UserDefaults.standard.integer(forKey: "userOrganisationCount")
            //let data =  UserDefaults.standard.value(forKey: "userOrganisationCount") as? String
            return data
        }
        set
        {
            UserDefaults.standard.set(newValue, forKey: "userOrganisationCount")
            //UserDefaults.standard.set(newValue, forKey: "userOrganisationCount")
        }
    }
    
    static var userName:String {
        get{
            if (UserDefaults.standard.object(forKey: "userName") != nil)
            {
                return UserDefaults.standard.object(forKey: "userName") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "userName")
            UserDefaults.standard.synchronize()
        }
    }
    
    static var pictureURLString:String {
        get{
            if ( UserDefaults.standard.object(forKey: "pictureURL") != nil)
            {
                return UserDefaults.standard.object(forKey: "pictureURL") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "pictureURL")
            UserDefaults.standard.synchronize()
        }
    }
    
    static var userSub:String {
        get{
            if ( UserDefaults.standard.object(forKey: "subject") != nil)
            {
                return UserDefaults.standard.object(forKey: "subject") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "subject")
            UserDefaults.standard.synchronize()
        }
    }
    
    static var name:String {
        get{
            if ( UserDefaults.standard.object(forKey: "name") != nil)
            {
                return UserDefaults.standard.object(forKey: "name") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "name")
        }
    }
    
    static var email:String {
        get{
            if ( UserDefaults.standard.object(forKey: "email") != nil)
            {
                return UserDefaults.standard.object(forKey: "email") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "email")
        }
    }
    
    static var phoneNumber:String {
        get{
            if ( UserDefaults.standard.object(forKey: "phoneNumber") != nil)
            {
                return UserDefaults.standard.object(forKey: "phoneNumber") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "phoneNumber")
        }
    }
    
    static var password:String {
        get{
            if ( UserDefaults.standard.object(forKey: "password") != nil)
            {
                return UserDefaults.standard.object(forKey: "password") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "password")
        }
    }
    
    static var deviceToken:String {
        get{
            if ( UserDefaults.standard.object(forKey: "deviceToken") != nil)
            {
                return UserDefaults.standard.object(forKey: "deviceToken") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "deviceToken")
        }
    }
    
    static var userRoles:Array<String> {
        get{
            if ( UserDefaults.standard.object(forKey: "userRoles") != nil)
            {
                return UserDefaults.standard.object(forKey: "userRoles") as! Array
            }
            else
            {
                return [];
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "userRoles")
        }
    }
    
    static var voipToken:String {
        get{
            if ( UserDefaults.standard.object(forKey: "voipToken") != nil)
            {
                return UserDefaults.standard.object(forKey: "voipToken") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "voipToken")
        }
    }
    
    static var fcmToken:String {
        get{
            if ( UserDefaults.standard.object(forKey: "fcmToken") != nil)
            {
                return UserDefaults.standard.object(forKey: "fcmToken") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "fcmToken")
        }
    }
    
    static var lastFcmToken:String {
        get{
            if ( UserDefaults.standard.object(forKey: "lastFcmToken") != nil)
            {
                return UserDefaults.standard.object(forKey: "lastFcmToken") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "lastFcmToken")
        }
    }
    
    static var isPremium:Bool {
        get{
            
            return UserDefaults.standard.bool(forKey: "isPremium")
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "isPremium")
        }
    }
    
    
    
    
    
    static var userAccessToken:AccessTokenModel = AccessTokenModel()
    
    static var userRefreshAccessToken:RefreshAccessTokenModel = RefreshAccessTokenModel()
    
    static var rememberMe: RememberMe = RememberMe()
    
    static func removeSaveData()
    {
        let arrUserInfoKeys = ["userName","firstName","lastName","password","driverId","userRoles","userId","userRoles","email","pictureURL","residentCardNo","subject","name","phoneNumber","deviceToken","isPremium","organisationID","AppTheme","userOrganisationCount"];
        
        for keys in arrUserInfoKeys {
            UserDefaults.standard.removeObject(forKey: keys)
        }
        
        
        self.removeSSOToken()
        //AppLocalDataCache.removeData()
        UserDefaults.standard.synchronize()
        
    }
    
    static func removeSSOToken() {
        AccessTokenModel.removeSaveData();
        RefreshAccessTokenModel.removeSaveData();
    }
    
    static func removeSaveDataForServerChange()
    {
        let arrUserInfoKeys = ["organisationID"];
        
        for keys in arrUserInfoKeys {
            UserDefaults.standard.removeObject(forKey: keys)
        }
        
        //AppLocalDataCache.removeData()
        UserDefaults.standard.synchronize()
        
    }
    
    static var UserInfoDic:[String:Any]?
    {
//        if AppUtility.hasValidText(UserInfo.userId)
//        {
//            var dic = [String:Any]()
//            dic["sub"] = UserInfo.userSub
//            dic["name"] = UserInfo.name
//            dic["email"] = UserInfo.email
//            return dic
//        }
        
        return nil
    }
    
}


//MARK:- User AccessToken Model
class AccessTokenModel:NSObject {
    var accessToken:String {
        get{
            if ( UserDefaults.standard.object(forKey: "accessToken") != nil)
            {
                let token = UserDefaults.standard.object(forKey: "accessToken") as! String
                return token
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "accessToken")
            token_date = Date()
            //MARK: TODO
            
            //NotificationServiceGroup.accessToken = accessToken
        }
    }
    
    var token_date: Date? {
        get{
            return UserDefaults.standard.object(forKey: "token_date") as? Date
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "token_date")
        }
    }
    
    var expires_in: Double? {
        get{
            //return 60
            return UserDefaults.standard.object(forKey: "expires_in") as? Double
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "expires_in")
        }
    }
    
    var accessTokenID:String {
        get{
            if ( UserDefaults.standard.object(forKey: "accessTokenID") != nil)
            {
                return UserDefaults.standard.object(forKey: "accessTokenID") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "accessTokenID")
        }
    }
    
    var expireDate:String {
        get{
            if ( UserDefaults.standard.object(forKey: "expireDate") != nil)
            {
                return UserDefaults.standard.object(forKey: "expireDate") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "expireDate")
        }
    }
    
    
    
    static func removeSaveData()
    {
        let arrUserInfoKeys = ["accessToken"];
        
        for keys in arrUserInfoKeys {
            UserDefaults.standard.removeObject(forKey: keys)
        }
        
        
        UserDefaults.standard.synchronize()
        
    }
    
    
    
}




//MARK:- User Refresh Access Token Model
class RefreshAccessTokenModel:NSObject {
    var refreshAccessToken:String {
        get{
            if ( UserDefaults.standard.object(forKey: "refreshAccessToken") != nil)
            {
                return UserDefaults.standard.object(forKey: "refreshAccessToken") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "refreshAccessToken")
            //MARK: TODO
//            NotificationServiceGroup.refreshAccessToken = refreshAccessToken
        }
    }
    
    var refreshAccessTokenID:String {
        get{
            if ( UserDefaults.standard.object(forKey: "refreshAccessTokenID") != nil)
            {
                return UserDefaults.standard.object(forKey: "refreshAccessTokenID") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "refreshAccessTokenID")
        }
    }
    
    var expireDateRefreshToken:String {
        get{
            if ( UserDefaults.standard.object(forKey: "expireDateRefreshToken") != nil)
            {
                return UserDefaults.standard.object(forKey: "expireDateRefreshToken") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "expireDateRefreshToken")
        }
    }
    
    static func removeSaveData()
    {
        let arrUserInfoKeys = ["expireDateRefreshToken","refreshAccessTokenID","refreshAccessToken"];
        
        for keys in arrUserInfoKeys {
            UserDefaults.standard.removeObject(forKey: keys)
        }
        UserDefaults.standard.synchronize()
    }
    
    
}

extension UserInfo {
    static var isLoggedIn: Bool {
        return UserInfo.userAccessToken.accessToken != ""
    }
}
