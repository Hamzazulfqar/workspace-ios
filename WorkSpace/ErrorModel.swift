//
//  ErrorModel.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation

public struct ErrorModel: Error {
    
    var errorTitle:String!
    var errorDescp:String!
    var error:Error?
    var errorCode:Int!
    
    init() {
        self.errorDescp = ""
        self.errorDescp = ""
        self.errorCode = 0
    }
    
    init(errorTitle:String, errorDescp:String, error:Error?, errorCode:Int = 0) {
        self.errorTitle = errorTitle
        self.errorDescp = errorDescp
        self.error = error
        self.errorCode = errorCode
    }
}
