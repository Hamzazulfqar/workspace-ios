//
//  WorkspaceModel.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation
import SwiftyJSON


class WorkspaceModel: NSObject {

    var isWsAdmin = false
    var star = false
    var archived = false
    var id: String!
    var name:String!
    var typ : String!
    var wsDescription: String!
    var startDate: String!
    var endDate: String!
    var deletedDate: String!
    var coverImage = ItemImageModel();
    var wsCategory:WSCategoryModel!
    var createdBy:UserModel!
    var organizationID:String!
    var wsUsers:[WSUsers] =  [WSUsers]()
    
    override init() {
        
    }
    
    
    init(json:JSON) {
        
        isWsAdmin = json["id"].boolValue;
        star = json["star"].boolValue;
        archived = json["archived"].boolValue;
        id = json["id"].string;
        name = json["name"].string;
        typ = json["typ"].string;
        wsDescription = json["description"].string;
        organizationID = json["organizationID"].string
        wsCategory = WSCategoryModel(json: json["category"])
        startDate = json["startDate"].string;
        endDate = json["endDate"].string;
        deletedDate = json["deletedDate"].string
        createdBy =   UserModel(json: json["createdBy"])
        
        if let arr = json["wsUsers"]["wsUsers"].array {
            self.wsUsers.append(contentsOf: arr.map({WSUsers(json: $0)}))
        }
           
        coverImage = ItemImageModel(path: json["coverURL"].string);

    }
    
    deinit {
        print("deinit Workspace model")
    }
}


class WSUsers:NSObject
{
   var user:UserModel!
   
   override init() {
       
   }
   init(json:JSON) {
       user = UserModel(json: json["user"])
   }
}


class MeetingCountModel : NSObject {
    var title : String!
    var workspaceId : String!
    var workspaceName : String!
    
    override init() {
          
      }
      
      init(json:JSON) {
          self.title = json["title"].string
          self.workspaceId = json["workspaceId"].string
        self.workspaceName = json["workspaceName"].string

      }
    
}
