//
//  ImageUploadModel.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation
import SwiftyJSON

class ItemImageModel: NSObject, NSCoding {
    
    var fileURL:URL!
    
    var fileName:String!
    var image:UIImage!
    var isCoverImage:Bool = false
    var attachment:String!
    var isImage:Bool
    {
        if let attachment = attachment , AppUtility.hasValidText(attachment)
        {
            return attachment.isImageFile
        }
        else
        {
            return true
        }
        
    }
    
    init(image:UIImage)
    {
        fileName = AppUtility.getDynamicFileName()
        isCoverImage = false
        self.image = image
    }
    
    init(fileURL:URL)
    {
        fileName = fileURL.lastPathComponent;
        isCoverImage = false
        self.fileURL = fileURL;
        self.image = nil;
    }
    
    init(isCoverImage:Bool) {
        fileName = AppUtility.getDynamicFileName()
        self.isCoverImage = isCoverImage
    }
    
    init(path:String?) {
        attachment = path
    }
    
    override init() {
        
    }
    
    init(fileURL:URL?,isCoverImage:Bool?, attachment:String? ) {
        
        self.fileURL = fileURL
        self.isCoverImage = isCoverImage ?? false
        self.attachment = attachment
        
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let fileURL = aDecoder.decodeObject(forKey: "fileURL") as? URL
        let isCoverImage = aDecoder.decodeObject(forKey: "isCoverImage") as? Bool
        let attachment = aDecoder.decodeObject(forKey: "attachment") as? String
        
        
        self.init(fileURL:fileURL, isCoverImage:isCoverImage, attachment:attachment)
        
        
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(fileURL, forKey: "fileURL")
        aCoder.encode(isCoverImage, forKey: "isCoverImage")
        aCoder.encode(attachment, forKey: "attachment")
    }
    
    
}
