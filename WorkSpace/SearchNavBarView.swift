//
//  SearchNavBarView.swift
//  Meera
//
//  Created by Zuhair Hussain on 20/04/2020.
//  Copyright © 2020 Muhammad Arslan Khalid. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


@objc protocol SearchNavBarDelegate: class {
    @objc optional func searchNavBarView(_ navigatonBar: SearchNavBarView, didTapLeftButton button: UIButton)
    @objc optional func searchNavBarView(_ navigatonBar: SearchNavBarView, didTapRightButton1 button: UIButton)
    @objc optional func searchNavBarView(_ navigatonBar: SearchNavBarView, didTapRightButton2 button: UIButton)
    @objc optional func searchNavBarView(_ navigatonBar: SearchNavBarView, didTapRightButton3 button: UIButton)
    @objc optional func searchNavBarView(_ navigatonBar: SearchNavBarView, didTapRightButton4 button: UIButton)
                                                                                 
    @objc optional func searchNavBarViewShouldBeginEditing(_ navigatonBar: SearchNavBarView) -> Bool
    @objc optional func searchNavBarView(_ navigatonBar: SearchNavBarView, textDidChange text: String)
    @objc optional func searchNavBarViewDidCancelSearch(_ navigatonBar: SearchNavBarView)
}


//@IBDesignable
class SearchNavBarView: UIView {
    
    
    // MARK: - Outlets
    var view: UIView!
    @IBOutlet weak var statusBarView: UIView!
    @IBOutlet weak var viewNavBar: UIView!
    
    @IBOutlet weak var viewLeftBarButton: UIView!
    @IBOutlet weak var viewRightBarButton1: UIView!
    @IBOutlet weak var viewRightBarButton2: UIView!
    @IBOutlet weak var viewRightBarButton3: UIView!
    @IBOutlet weak var viewRightBarButton4: UIView!
    
    @IBOutlet weak var viewBadgeCircleRightBarButton1: UIView!
    @IBOutlet weak var viewBadgeCircleRightBarButton2: UIView!
    @IBOutlet weak var viewBadgeCircleRightBarButton3: UIView!
    @IBOutlet weak var viewBadgeCircleRightBarButton4: UIView!
    
    @IBOutlet weak var leftIconImageView: UIImageView!
    @IBOutlet weak var rightIconImageView1: UIImageView!
    @IBOutlet weak var rightIconImageView2: UIImageView!
    @IBOutlet weak var rightIconImageView3: UIImageView!
    @IBOutlet weak var rightIconImageView4: UIImageView!
    
    @IBOutlet weak var imgIconSearch: UIImageView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var viewSearchBar: UIView!
    @IBOutlet weak var viewContainerSearchBar: UIView!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight1: UIButton!
    @IBOutlet weak var btnRight2: UIButton!
    @IBOutlet weak var btnRight3: UIButton!
    @IBOutlet weak var btnRight4: UIButton!
    @IBOutlet weak var imgLogo: UIImageView!
    
    @IBOutlet weak var txtSearchBar: UITextField!
    
    @IBOutlet weak var viewSeparatorRightButton: UIView!
    @IBOutlet weak var viewSeparatorLeftButton: UIView!
    
    @IBOutlet weak var viewBadgeRightBarButton1: UIView!
    @IBOutlet weak var viewBadgeRightBarButton2: UIView!
    @IBOutlet weak var viewBadgeRightBarButton3: UIView!
    @IBOutlet weak var viewBadgeRightBarButton4: UIView!
    
    @IBOutlet weak var lblBadgeRightBarButton1: UILabel!
    @IBOutlet weak var lblBadgeRightBarButton2: UILabel!
    @IBOutlet weak var lblBadgeRightBarButton3: UILabel!
    @IBOutlet weak var lblBadgeRightBarButton4: UILabel!
    
    @IBOutlet weak var viewSearchButton: UIView!
    
    @IBOutlet weak var cstSafeAreaViewHeight: NSLayoutConstraint!
    @IBOutlet weak var cstNavigationBarHeight: NSLayoutConstraint!
    
    @IBOutlet weak var delegate: SearchNavBarDelegate?
    let disposeBag = DisposeBag()
    
    // MARK: - Properties
    private var leftIconImageForLightMode: UIImage?
    private var leftIconImageForDarkMode: UIImage?
    
    private var rightIconImage1ForLightMode: UIImage?
    private var rightIconImage2ForLightMode: UIImage?
    private var rightIconImage3ForLightMode: UIImage?
    private var rightIconImage4ForLightMode: UIImage?
    
    private var rightIconImage1ForDarkMode: UIImage?
    private var rightIconImage2ForDarkMode: UIImage?
    private var rightIconImage3ForDarkMode: UIImage?
    private var rightIconImage4ForDarkMode: UIImage?
    
    private var searchBarPlaceholderText = "Search"
    
    struct BarColors {
        var bgLight = UIColor.white
        var bgDark = AppColor.darkThemeBlack
        
        var titleLight = #colorLiteral(red: 0.3610000014, green: 0.3689999878, blue: 0.3689999878, alpha: 1)
        var titleDark = UIColor.white
        
        var cancelButtonLight = #colorLiteral(red: 0.09399999678, green: 0.1140000001, blue: 0.1330000013, alpha: 1)
        var cancelButtonDark = UIColor.white
    }
    
    
    var barColors = BarColors()
    
    var theme: AppTheme = .light
    
    @IBInspectable var title: String? {
        get {
            return lblTitle.text
        }
        set {
            lblTitle.text = newValue
            view?.layoutIfNeeded()
        }
    }
    @IBInspectable var leftIconImage: UIImage? {
        get {
            return leftIconImageForLightMode
        }
        set {
            leftIconImageForLightMode = newValue
            leftIconImageView.image = newValue
            viewLeftBarButton.isHidden = newValue == nil
            viewSeparatorLeftButton.isHidden = !viewLeftBarButton.isHidden
            view?.layoutIfNeeded()
        }
    }
    @IBInspectable var leftIconDarkImage: UIImage? {
        get {
            return leftIconImageForDarkMode
        }
        set {
            leftIconImageForDarkMode = newValue
        }
    }
    @IBInspectable var rightIconImage1: UIImage? {
        get {
            return rightIconImage1ForLightMode
        }
        set {
            rightIconImage1ForLightMode = newValue
            rightIconImageView1.image = newValue
            viewRightBarButton1.isHidden = newValue == nil
//            viewSeparatorRightButton.isHidden = !viewRightBarButton1.isHidden || !viewRightBarButton2.isHidden || !viewRightBarButton3.isHidden || !viewRightBarButton4.isHidden
            view?.layoutIfNeeded()
        }
    }
    @IBInspectable var rightIconImage2: UIImage? {
        get {
            return rightIconImage2ForLightMode
        }
        set {
            rightIconImage2ForLightMode = newValue
            rightIconImageView2.image = newValue
            viewRightBarButton2.isHidden = newValue == nil
//            viewSeparatorRightButton.isHidden = !viewRightBarButton1.isHidden || !viewRightBarButton2.isHidden || !viewRightBarButton3.isHidden || !viewRightBarButton4.isHidden
            view?.layoutIfNeeded()
        }
    }
    @IBInspectable var rightIconImage3: UIImage? {
        get {
            return rightIconImage3ForLightMode
        }
        set {
            rightIconImage3ForLightMode = newValue
            rightIconImageView3.image = newValue
            viewRightBarButton3.isHidden = newValue == nil
//            viewSeparatorRightButton.isHidden = !viewRightBarButton1.isHidden || !viewRightBarButton2.isHidden || !viewRightBarButton3.isHidden || !viewRightBarButton4.isHidden
            view?.layoutIfNeeded()
        }
    }
    
    @IBInspectable var rightIconImag4: UIImage? {
        get {
            return rightIconImage4ForLightMode
        }
        set {
            rightIconImage4ForLightMode = newValue
            rightIconImageView4.image = newValue
            viewRightBarButton4.isHidden = newValue == nil
//            viewSeparatorRightButton.isHidden = !viewRightBarButton1.isHidden || !viewRightBarButton2.isHidden || !viewRightBarButton3.isHidden || !viewRightBarButton4.isHidden
            view?.layoutIfNeeded()
        }
    }
    //    @IBInspectable var barBackgroundColor: UIColor {
    //        get {
    //            return BarColors.lightTheme
    //        }
    //        set {
    //            BarBgColors.lightTheme = newValue
    //        }
    //    }
    @IBInspectable var rightIconDarkImage1: UIImage? {
        get {
            return rightIconImage1ForDarkMode
        }
        set {
            rightIconImage1ForDarkMode = newValue
        }
    }
    @IBInspectable var rightIconDarkImage2: UIImage? {
        get {
            return rightIconImage2ForDarkMode
        }
        set {
            rightIconImage2ForDarkMode = newValue
        }
    }
    @IBInspectable var rightIconDarkImage3: UIImage? {
        get {
            return rightIconImage3ForDarkMode
        }
        set {
            rightIconImage3ForDarkMode = newValue
        }
    }
    @IBInspectable var rightIconDarkImage4: UIImage? {
        get {
            return rightIconImage4ForDarkMode
        }
        set {
            rightIconImage4ForDarkMode = newValue
        }
    }
    @IBInspectable var searchBarPlaceholder: String? {
        get {
            return searchBarPlaceholderText
        }
        set {
            txtSearchBar.attributedPlaceholder = NSAttributedString(string: searchBarPlaceholderText,
                                                                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.placeholderGray])
            view?.layoutIfNeeded()
        }
    }
    
    @IBInspectable var badgeRightBarButton1: Int {
        get {
            return Int(lblBadgeRightBarButton1.text ?? "0") ?? 0
        }
        set {
            lblBadgeRightBarButton1.text = newValue < 1000 ? "\(newValue)" : "999+"
            viewBadgeRightBarButton1.isHidden = newValue <= 0
            rightIconImageView1.isHidden = !viewBadgeRightBarButton1.isHidden
            if viewBadgeRightBarButton1.isHidden == false {
                viewBadgeCircleRightBarButton1.isHidden = true
            }
            view?.layoutIfNeeded()
        }
    }
    @IBInspectable var badgeRightBarButton2: Int {
        get {
            return Int(lblBadgeRightBarButton2.text ?? "0") ?? 0
        }
        set {
            lblBadgeRightBarButton2.text = newValue < 1000 ? "\(newValue)" : "999+"
            viewBadgeRightBarButton2.isHidden = newValue <= 0
            rightIconImageView2.isHidden = !viewBadgeRightBarButton2.isHidden
            if viewBadgeRightBarButton2.isHidden == false {
                viewBadgeCircleRightBarButton2.isHidden = true
            }
            view?.layoutIfNeeded()
        }
    }
    @IBInspectable var badgeRightBarButton3: Int {
        get {
            return Int(lblBadgeRightBarButton3.text ?? "0") ?? 0
        }
        set {
            lblBadgeRightBarButton3.text = newValue < 1000 ? "\(newValue)" : "999+"
            viewBadgeRightBarButton3.isHidden = newValue <= 0
            rightIconImageView3.isHidden = !viewBadgeRightBarButton3.isHidden
            if viewBadgeRightBarButton3.isHidden == false {
                viewBadgeCircleRightBarButton3.isHidden = true
            }
            view?.layoutIfNeeded()
        }
    }
    @IBInspectable var badgeRightBarButton4: Int {
        get {
            return Int(lblBadgeRightBarButton4.text ?? "0") ?? 0
        }
        set {
            lblBadgeRightBarButton4.text = newValue < 1000 ? "\(newValue)" : "999+"
            viewBadgeRightBarButton4.isHidden = newValue <= 0
            rightIconImageView4.isHidden = !viewBadgeRightBarButton4.isHidden
            if viewBadgeRightBarButton4.isHidden == false {
                viewBadgeCircleRightBarButton4.isHidden = true
            }
            view?.layoutIfNeeded()
        }
    }
    
    func showCircleRightBadge1() {
        if viewBadgeRightBarButton1.isHidden == true {
            viewBadgeCircleRightBarButton1.isHidden = false
            view?.layoutIfNeeded()
        }
    }
    func showCircleRightBadge2() {
        if viewBadgeRightBarButton2.isHidden == true {
            viewBadgeCircleRightBarButton2.isHidden = false
            view?.layoutIfNeeded()
        }
    }
    func showCircleRightBadge3() {
        if viewBadgeRightBarButton3.isHidden == true {
            viewBadgeCircleRightBarButton3.isHidden = false
            view?.layoutIfNeeded()
        }
    }
    func showCircleRightBadge4() {
        if viewBadgeRightBarButton4.isHidden == true {
            viewBadgeCircleRightBarButton4.isHidden = false
            view?.layoutIfNeeded()
        }
    }
    
    
    func hideCircleRightBadge1() {
        viewBadgeCircleRightBarButton1.isHidden = true
        view?.layoutIfNeeded()
    }
    func hideCircleRightBadge2() {
        viewBadgeCircleRightBarButton2.isHidden = true
        view?.layoutIfNeeded()
    }
    func hideCircleRightBadge3() {
        viewBadgeCircleRightBarButton3.isHidden = true
        view?.layoutIfNeeded()
    }
    func hideCircleRightBadge4() {
        viewBadgeCircleRightBarButton4.isHidden = true
        view?.layoutIfNeeded()
    }
    
    
    func updateTheme() {
        
        if theme == .light {
            
            txtSearchBar.textColor = UIColor.black
            lblTitle.textColor = barColors.titleLight
            statusBarView.backgroundColor = barColors.bgLight
            viewNavBar.backgroundColor = barColors.bgLight
            imgIconSearch.image = "search_lite_theme".bundleImage
            leftIconImageView.image = leftIconImageForLightMode
            rightIconImageView1.image = rightIconImage1ForLightMode
            rightIconImageView2.image = rightIconImage2ForLightMode
            rightIconImageView3.image = rightIconImage3ForLightMode
            rightIconImageView4.image = rightIconImage4ForLightMode
            
            viewContainerSearchBar.backgroundColor = UIColor(hexString: "F1F3F5")
            txtSearchBar.attributedPlaceholder = NSAttributedString(string: searchBarPlaceholderText,
                                                                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.placeholderGray])
            btnCancel.setTitleColor(barColors.cancelButtonLight , for: .normal)
            imgLogo.image = "fluxble_nav_bar_logo".bundleImage
            
        } else {
            txtSearchBar.textColor = UIColor.white
            lblTitle.textColor = barColors.titleDark
            statusBarView.backgroundColor = barColors.bgDark
            viewNavBar.backgroundColor = barColors.bgDark
            imgIconSearch.image = "search_icon_dark_theme".bundleImage
            leftIconImageView.image = leftIconImageForDarkMode
            rightIconImageView1.image = rightIconImage1ForDarkMode
            rightIconImageView2.image = rightIconImage2ForDarkMode
            rightIconImageView3.image = rightIconImage3ForDarkMode
            rightIconImageView4.image = rightIconImage4ForDarkMode
            
            viewContainerSearchBar.backgroundColor = UIColor(hexString: "3A3B3C")
            txtSearchBar.attributedPlaceholder = NSAttributedString(string: searchBarPlaceholderText,
                                                                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.7)])
            btnCancel.setTitleColor(barColors.cancelButtonDark, for: .normal)
            imgLogo.image = "fluxble_nav_bar_logo_dark".bundleImage
            
        }
        txtSearchBar.keyboardAppearance = theme == .light ? .light : .dark
        
    }
    
    // MARK: - View Methods
    init() {
        let h = AppConstants.safeArea.top > 20 ? AppConstants.safeArea.top : 20
        super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: h + 44))
        xibSetup()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    func xibSetup() {
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        
        let h = AppConstants.safeArea.top > 20 ? AppConstants.safeArea.top : 20
        cstSafeAreaViewHeight.constant = h
        
        view.layoutIfNeeded()
        
        txtSearchBar.delegate = self
        if txtSearchBar.attributedPlaceholder == nil {
            searchBarPlaceholder = "Search"
        }
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.addObserverForThemeChange()
        }
        
    }
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return nibView
    }
    
    func addObserverForThemeChange() {
        AppObservers.appTheme.distinctUntilChanged().subscribe(onNext: { [weak self] (currentTheme) in
            guard let self = self else { return }
            
            self.theme = currentTheme
            self.updateTheme()
            
        }).disposed(by: self.disposeBag)
    }
}


extension SearchNavBarView {
    
    @IBAction func didTapLeftBarButton(_ sender: UIButton) {
        delegate?.searchNavBarView?(self, didTapLeftButton: sender)
    }
    @IBAction func didTapSearchButton(_ sender: UIButton) {
        txtSearchBar.becomeFirstResponder()
        viewSearchBar.isHidden = false
        stackView.isHidden = true
        
        viewSearchBar.alpha = 1
    }
    @IBAction func didTapCancelSearchButton(_ sender: UIButton) {
        txtSearchBar.text = ""
        txtSearchBar.resignFirstResponder()
        viewSearchBar.hideAnimated()
        stackView.showAnimated()
        delegate?.searchNavBarViewDidCancelSearch?(self)
    }
    @IBAction func didTapRightBarButton1(_ sender: UIButton) {
        delegate?.searchNavBarView?(self, didTapRightButton1: sender)
    }
    @IBAction func didTapRightBarButton2(_ sender: UIButton) {
        delegate?.searchNavBarView?(self, didTapRightButton2: sender)
    }
    @IBAction func didTapRightBarButton3(_ sender: UIButton) {
        delegate?.searchNavBarView?(self, didTapRightButton3: sender)
    }
    
    @IBAction func didTapRightBarButton4(_ sender: UIButton) {
        delegate?.searchNavBarView?(self, didTapRightButton4: sender)
    }
    
    @IBAction func didTextChange(_ sender: UITextField) {
        delegate?.searchNavBarView?(self, textDidChange: sender.text ?? "")
    }
}

extension SearchNavBarView: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return delegate?.searchNavBarViewShouldBeginEditing?(self) ?? true
    }
}
