//
//  ServiceAPILayer.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation
import UIKit
import SwiftyJSON
import TFNetwork


public typealias APICompletionerHandler = (_ result:Any?,_ errorModel:ErrorModel?) ->Void

let APIMeeting = ServiceAPILayer.sharedInstance;

class ServiceAPILayer: NSObject {
    
    @objc static let sharedInstance = ServiceAPILayer()
    
    
    func getWorkSpaceInfoList(with completion: @escaping APICompletionerHandler)
    {
        var parameter = [String:Any]();
        parameter["query"] = Q_WorkSpaceListInfo;
        parameter["variables"] = ["subject": UserInfo.userSub]
        
        let request = TFRequest(baseUrl: baseUrl, method: .post, bodyPerameter: parameter)
        TFNetwork.asyncCall(apiRequest: request, requiredResponce: .json) { (response) in
            
            guard let data = response.data else{
                completion(nil, response.error as? ErrorModel)
                          return;
                      }
            
            let josnn = JSON(data);
                        
                            guard (josnn["data"]["workspacesBySubject"]["workspaces"].array != nil) else {
            
                                completion(nil, ErrorModel(errorTitle: "Meera", errorDescp: "data not found", error: nil, errorCode:0));
                                return;
                            }
            
                            let jsonWorkSpace = josnn["data"]["workspacesBySubject"]["workspaces"].arrayValue;
            
                            print(jsonWorkSpace);
            
                            if jsonWorkSpace.count > 0
                            {
                                var arrWorkSpaces = [WorkSpaceSummaryModel]();
            
                                for j in jsonWorkSpace
                                {
                                    if (!AppUtility.hasValidText(j["deletedDate"].string))
                                    {
                                        arrWorkSpaces.append(WorkSpaceSummaryModel(json: j));
                                    }
                                    else{
                                        print("deletedDate exist")
                                    }
            
            
                                }
                                //arrWorkSpaces.append(contentsOf: jsonWorkSpace.map({WorkSpaceSummaryModel(json: $0)}));
            
                                completion(arrWorkSpaces,nil);
                            }
                            else
                            {
                                //workspace data not found
                                completion([WorkSpaceSummaryModel](),nil);
                                //completion(nil, ErrorModel(errorTitle: "Meera", errorDescp: "No Record Found", error: nil, errorCode:0));
                            }
            
            }
        
//        .asyncCallApiForJSONResult(WithBaseUrl: baseUrl, WithQueryParameter: parameter, WithDelegate: nil) { (json, errorModel) in
            
//            if let josnn = json
//            {
//                guard (josnn["data"]["workspacesBySubject"]["workspaces"].array != nil) else {
//
//                    completion(nil, ErrorModel(errorTitle: "Meera", errorDescp: "data not found", error: nil, errorCode:0));
//                    return;
//                }
//
//                let jsonWorkSpace = josnn["data"]["workspacesBySubject"]["workspaces"].arrayValue;
//
//                print(jsonWorkSpace);
//
//                if jsonWorkSpace.count > 0
//                {
//                    var arrWorkSpaces = [WorkSpaceSummaryModel]();
//
//                    for j in jsonWorkSpace
//                    {
//                        if (!AppUtility.hasValidText(j["deletedDate"].string))
//                        {
//                            arrWorkSpaces.append(WorkSpaceSummaryModel(json: j));
//                        }
//                        else{
//                            print("deletedDate exist")
//                        }
//
//
//                    }
//                    //arrWorkSpaces.append(contentsOf: jsonWorkSpace.map({WorkSpaceSummaryModel(json: $0)}));
//
//                    completion(arrWorkSpaces,nil);
//                }
//                else
//                {
//                    //workspace data not found
//                    completion([WorkSpaceSummaryModel](),nil);
//                    //completion(nil, ErrorModel(errorTitle: "Meera", errorDescp: "No Record Found", error: nil, errorCode:0));
//                }
//
//
//            }

        }
    }
    

