//
//  MeeraWorkSpaceUtility.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation

import UIKit
import OAuthSwift
import SwiftyJSON
import FittedSheets
//import Firebase
import Alamofire

let AppUtility = MeeraWorkSpaceUtility.sharedInstance
class MeeraWorkSpaceUtility: NSObject {
  
    @objc static let sharedInstance = MeeraWorkSpaceUtility()
    var calander = Calendar.current
    var arrWorkspaces = [WorkSpaceSummaryModel]()
    
    var reachability: Reachability?
    
    lazy var dateFormatter: DateFormatter = {
        
        let dt = DateFormatter();
        return dt;
    }();
}


//MARK:- Validation Methods
extension MeeraWorkSpaceUtility
{
    func hasValidText(_ text:String?) -> Bool
    {
        if let data = text
        {
            if data == "nil"
            {
                return false
            }
            let str = data.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            if str.count>0
            {
                return true
            }
            else
            {
                return false
            }
        }
        else
        {
            return false
        }
        
    }
    
    func isEmail(_ email:AnyObject  ) -> Bool
    {
        let strEmailMatchstring = "\\b([a-zA-Z0-9%_.+\\-]+)@([a-zA-Z0-9.\\-]+?\\.[a-zA-Z]{2,6})\\b"
        let regExPredicate = NSPredicate(format: "SELF MATCHES %@", strEmailMatchstring)
        
        if(!isEmpty(email) && regExPredicate.evaluate(with: email))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    func isEmpty(_ thing : AnyObject? )->Bool {
        
        
        if(thing as? Data == nil)
        {
            return false;
        }
        else if (thing == nil)
        {
            return false
        }
        else if ((thing as! Data).count == 0)
        {
            return false
        }
        else if((thing! as! NSArray).count == 0)
        {
            return false
        }
        return true;
        
        
    }
    
    func validateMobileNumber(value: String) -> Bool {
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$";
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
}


//MARK:- General Methods
extension MeeraWorkSpaceUtility
{
    
    func getCompressedImage(image: UIImage, maxSize: Int, minSize: Int, times: Int) -> Data?
    {
        
        var maxQuality: CGFloat = 1.0
        var minQuality: CGFloat = 0.0
        var bestData: Data?
        for _ in 1...times {
            let thisQuality = (maxQuality + minQuality) / 2
            guard let data = image.jpegData(compressionQuality: thisQuality) else { return nil }
            let thisSize = data.count
            if thisSize > maxSize {
                maxQuality = thisQuality
            } else {
                minQuality = thisQuality
                bestData = data
                if thisSize > minSize {
                    return bestData
                }
            }
        }
        
        return bestData
        
    }
    
    func resetDataAll() {
        
        //AppLocalDataCache.removeData(needToRemoveOrgizations: false)
        
        UserInfo.removeSaveDataForServerChange()
       
        /*
        CallApiManager.API.updateURLs()
        MeeraFSGRPCClient.shared.tempCLient = nil
    */
//    MSTusUpload.shared.clearTusSession()
        UserInfo.userId = "";
        UserInfo.defaultWorkspaceId = nil
        UserInfo.removeSaveData();
        AppUtility.arrWorkspaces.removeAll();
//        AppCache.clearAppCache();
        //MARK: TODO
/*
        AppSocket.disconnectWebSocket()
        
        CallApiManager.shared.webSocketManger.disConnect()
        
        AppLocalDataCache.removeData()
        
        NoteUTIL.deleteInstanceID()
 */
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        
        AppUtility.stopNotifier()
        
        //MARK: TODO
        /*
         CDOrganization.deleteAll()
         */
        
        
    }
    
    func getDynamicFileName() -> String
    {
        let  fileName = "\((Date().timeIntervalSinceReferenceDate)).jpg"
        return fileName
    }
    
    func getDateFromString(date:String) -> String?
    {
        let dateFormat = AppUtility.dateFormatter
        dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
        
        var postTime = dateFormat.date(from: date)
        
        if postTime == nil
        {
            dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            postTime = dateFormat.date(from: date)
        }
        else
        {
            return dateFormat.dateFormat
        }
        
        if postTime == nil
        {
            dateFormat.dateFormat = "YYYY-MM-dd'T'HH:mm:ssZ"
            postTime = dateFormat.date(from: date)
        }
        else
        {
            return dateFormat.dateFormat
        }
        
        if postTime == nil
        {
            dateFormat.dateFormat = "HH:mm:ss.SSSXXX"
            postTime = dateFormat.date(from: date)
        }
        else
        {
            return dateFormat.dateFormat
        }
        
        return nil
        
        
    }
    
}


//MARK:- Reachability Utiity Methods
extension MeeraWorkSpaceUtility {
    
    func setupReachability(_ hostName: String?, useClosures: Bool) {
       
        let reachable: Reachability?
        if let hostName = hostName {
            reachable = Reachability(hostname: hostName)
            //hostNameLabel.text = hostName
        } else {
            reachable = Reachability()
            //hostNameLabel.text = "No host name"
        }
        self.reachability = reachable
       
        
        if useClosures {
            self.reachability?.whenReachable = { reachability in
                //self.updateLabelColourWhenReachable(reachability)
                print("Internet Available")
                
            }
            self.reachability?.whenUnreachable = { reachability in
                //self.updateLabelColourWhenNotReachable(reachability)
                print("Internet Not Available")
            }
        } else {
            NotificationCenter.default.addObserver(
                self,selector: #selector(reachabilityChanged(_:)), name: .reachabilityChanged,object: reachability
            )
        }
 
    }
    
    func startNotifier() {
        print("--- start notifier")
        do {
            try self.reachability?.startNotifier()
        } catch {
            
            print("Unable to start notifier")
            return
        }
    }
    
    func stopNotifier() {
        print("--- stop notifier")
        self.reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: nil)
        //self.reachability = nil
    }
    
    @objc func reachabilityChanged(_ note: Notification) {
        let reachable = note.object as! Reachability
        
        if reachable.connection != .none {
            //updateLabelColourWhenReachable(reachability)
            print("Internet Available")
        } else {
            //updateLabelColourWhenNotReachable(reachability)
            print("Internet Not Available")
        }
    }
    
    func isInternet() -> Bool {
        return self.reachability?.connection != Reachability.Connection.none
    }
}
