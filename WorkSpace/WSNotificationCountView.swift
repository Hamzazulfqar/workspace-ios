//
//  WSNotificationCountView.swift
//  Meera
//
//  Created by Yasir  Khan on 11/06/2020.
//  Copyright © 2020 Muhammad Arslan Khalid. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol WSNotificationCountViewDelegate: class {
    func didTapNewNotificationButton(_ sender: UIButton)
    func didTapDueTasksButton(_ sender: UIButton)
    func didTapMeetingsTodayButton(_ sender: UIButton)
    func didTapEventsTodayButton(_ sender: UIButton)
}


@IBDesignable
class WSNotificationCountView: UIView {
    
    // MARK: - Outlets
    @IBOutlet weak var stackViewTrailing: NSLayoutConstraint!
    @IBOutlet weak var stackViewLeading: NSLayoutConstraint!
    var view: UIView!
    @IBOutlet weak var lblNotificationCount: UILabel!
    @IBOutlet weak var lblDueTaskCount: UILabel!
    @IBOutlet weak var lblMeetingCount: UILabel!
    @IBOutlet weak var lblEventCount: UILabel!
    
    @IBOutlet weak var lblNotificationText: UILabel!
    @IBOutlet weak var lblDueTaskText: UILabel!
    @IBOutlet weak var lblMeetingText: UILabel!
    @IBOutlet weak var lblEventText: UILabel!
    
    @IBOutlet weak var btnNewNotifications: UIButton!
    @IBOutlet weak var btnDueTasks: UIButton!
    @IBOutlet weak var btnMeetingsToday: UIButton!
    @IBOutlet weak var btnEventsToday: UIButton!
    
    @IBOutlet weak var viewContainerNotifications: UIView!
    @IBOutlet weak var viewContainerDueTasks: UIView!
    @IBOutlet weak var viewContainerMeetingsToday: UIView!
    @IBOutlet weak var viewContainerEventsToday: UIView!
    
    @IBOutlet weak var viewSeparatorNotifications: UIView!
    @IBOutlet weak var viewSeparatorDueTasks: UIView!
    @IBOutlet weak var viewSeparatorMeetingsToday: UIView!
    
    var isSelectionEnabled = false
    let disposeBag = DisposeBag()
    
    weak var delegate: WSNotificationCountViewDelegate?
    
    enum SelectedState {
        case none, notification, tasks, meeting, events
    }
    var selectedState = SelectedState.none
    
    
    
    var theme: AppTheme = ApplicationManager.shared.appTheme
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        nibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        nibSetup()
    }
    
    private func nibSetup() {
        backgroundColor = .clear
        
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        if Device.IS_IPAD {
            stackViewLeading.constant = 100
            stackViewTrailing.constant = 100
        } else {
            stackViewLeading.constant = 0
            stackViewTrailing.constant = 0
        }
        loadData()
        addSubview(view)
    }
    
    func loadData(){
        if Device.IS_IPAD {
            lblNotificationText.text = "New Notifications"
            lblDueTaskText.text = "Due Tasks"
            lblMeetingText.text = "Meetings Today"
            lblEventText.text = "Events Today"
        } else {
        lblNotificationText.text = "New\nNotifications"
        lblDueTaskText.text = "Due\nTasks"
        lblMeetingText.text = "Meetings\nToday"
        lblEventText.text = "Events\nToday"
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.addObserverForThemeChange()
        }
        
    }
    func resetViews() {
        [viewContainerNotifications, viewContainerDueTasks, viewContainerMeetingsToday, viewContainerEventsToday].forEach { $0?.backgroundColor = .clear }
        [lblNotificationText, lblDueTaskText, lblMeetingText, lblEventText].forEach { $0?.textColor = theme == .light ? .black : .white }
        [viewSeparatorNotifications, viewSeparatorDueTasks, viewSeparatorMeetingsToday].forEach { $0?.isHidden = false }
        
        lblNotificationCount.textColor = "88C24B".color
        lblDueTaskCount.textColor = "FFC108".color
        lblMeetingCount.textColor = "FF5821".color
        lblEventCount.textColor = "E91F64".color
    }
    
    
    func selectNotifcationView() {
        resetViews()
        viewContainerNotifications.backgroundColor = "88C24B".color
        lblNotificationText.textColor = .white
        lblNotificationCount.textColor = .white
        viewSeparatorNotifications.isHidden = true
        selectedState = .notification
    }
    func selectDueTasksView() {
        resetViews()
        viewContainerDueTasks.backgroundColor = "FFC108".color
        lblDueTaskText.textColor = .white
        lblDueTaskCount.textColor = .white
        viewSeparatorNotifications.isHidden = true
        viewSeparatorDueTasks.isHidden = true
        selectedState = .tasks
    }
    func selectMeetingsView() {
        resetViews()
        viewContainerMeetingsToday.backgroundColor = "FF5821".color
        lblMeetingText.textColor = .white
        lblMeetingCount.textColor = .white
        viewSeparatorDueTasks.isHidden = true
        viewSeparatorMeetingsToday.isHidden = true
        selectedState = .meeting
    }
    func selectEventsView() {
        resetViews()
        viewContainerEventsToday.backgroundColor = "E91F64".color
        lblEventText.textColor = .white
        lblEventCount.textColor = .white
        viewSeparatorMeetingsToday.isHidden = true
        selectedState = .events
    }
    
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return nibView
    }
    
    func addObserverForThemeChange() {
        AppObservers.appTheme.distinctUntilChanged().subscribe(onNext: { [weak self] (currentTheme) in
            guard let self = self else { return }
            
            self.theme = currentTheme
            self.updateTheme()
            
        }).disposed(by: self.disposeBag)
    }
    
    func updateTheme() {
        
        if ApplicationManager.shared.appTheme == .light {
            view.backgroundColor = .white
            lblNotificationText.textColor = UIColor.black
            lblDueTaskText.textColor = UIColor.black
            lblMeetingText.textColor = UIColor.black
            lblEventText.textColor = UIColor.black
            self.view.viewWithTag(301)?.backgroundColor = #colorLiteral(red: 0.9137254902, green: 0.9176470588, blue: 0.9254901961, alpha: 1)
            self.view.viewWithTag(302)?.backgroundColor = #colorLiteral(red: 0.9137254902, green: 0.9176470588, blue: 0.9254901961, alpha: 1)
            self.view.viewWithTag(303)?.backgroundColor = #colorLiteral(red: 0.9137254902, green: 0.9176470588, blue: 0.9254901961, alpha: 1)
            
            
        } else {
            lblNotificationText.textColor = UIColor.white
            lblDueTaskText.textColor = UIColor.white
            lblMeetingText.textColor = UIColor.white
            lblEventText.textColor = UIColor.white
            self.view.viewWithTag(301)?.backgroundColor = #colorLiteral(red: 0.2196078431, green: 0.2274509804, blue: 0.2274509804, alpha: 1)
            self.view.viewWithTag(302)?.backgroundColor = #colorLiteral(red: 0.2196078431, green: 0.2274509804, blue: 0.2274509804, alpha: 1)
            self.view.viewWithTag(303)?.backgroundColor = #colorLiteral(red: 0.2196078431, green: 0.2274509804, blue: 0.2274509804, alpha: 1)

            view.backgroundColor = #colorLiteral(red: 0.01568627451, green: 0.01568627451, blue: 0.01960784314, alpha: 1)
        }
        
        if selectedState == .notification {
            selectNotifcationView()
        } else if selectedState == .tasks {
            selectDueTasksView()
        } else if selectedState == .meeting {
            selectMeetingsView()
        } else if selectedState == .events {
            selectEventsView()
        }
    }
    
    @IBAction func didTapNewNotificationButton(_ sender: UIButton) {
        if isSelectionEnabled {
            selectNotifcationView()
        }
        delegate?.didTapNewNotificationButton(sender)
    }
    @IBAction func didTapDueTasksButton(_ sender: UIButton) {
        if isSelectionEnabled {
            selectDueTasksView()
        }
        delegate?.didTapDueTasksButton(sender)
    }
    @IBAction func didTapMeetingsTodayButton(_ sender: UIButton) {
        if isSelectionEnabled {
            selectMeetingsView()
        }
        delegate?.didTapMeetingsTodayButton(sender)
    }
    @IBAction func didTapEventsTodayButton(_ sender: UIButton) {
        if isSelectionEnabled {
            selectEventsView()
        }
        delegate?.didTapEventsTodayButton(sender)
    }
    
    
}
