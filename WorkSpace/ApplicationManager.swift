//
//  ApplicationManager.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation
import UIKit

public class ApplicationManager {

    public static let shared = ApplicationManager()
    
    public weak var delegate: UIApplicationDelegate?
    
    public var appTheme : AppTheme!
    
    public var domainName : String = ""
    
    
}
