//
//  AppBottomBar.swift
//  Meera
//
//  Created by Muhammad Arslan Khalid on 20/04/2020.
//  Copyright © 2020 Muhammad Arslan Khalid. All rights reserved.
//

import UIKit
import Foundation
import RxSwift
import RxCocoa

class AppBottomBar: UIView {
    
    var view: UIView!
    
    
    
    @IBOutlet var tabBtns:[UIButton]!
    @IBOutlet var tabTitles:[UILabel]!
    @IBOutlet private weak var tabBackgroundView:UIView!
    @IBOutlet weak var badgeCollaboration:UIView!
    @IBOutlet weak var lblBadgeCollaboration:UILabel!
    weak var delegate:AppBottomBarDelegate!
    
    @IBOutlet weak var appTabView: UIView!
    
    
    weak var disposeBag = DisposeBag()
    
    //  private struct BottomTabImages {
    //        static var tab1 = TabImage(selectedImg: "workspace_enable_bar", deSelectedImg: "workspace_disabled")
    //        static var tab2 = TabImage(selectedImg: "task_manager_enable_bar", deSelectedImg: "task_manager_disabled")
    //        static var tab3 = TabImage(selectedImg: "chat_enable_bar", deSelectedImg: "chat_disabled")
    //        static var tab4 = TabImage(selectedImg: "calendar_enable_bar", deSelectedImg: "calendar_disabled")
    //        static var tab5 = TabImage(selectedImg: "meeting_enable_bar", deSelectedImg: "meeting_disabled")
    //    }
    
    private var currentTabbItems: BottomTabImages
    {
        return AppTheme.currentTheme.currentThemeTabIcons
    }
    
    
    
    
    
    
    
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        // 3. Setup view from .xib file
        
        xibSetup()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        
        
        // 3. Setup view from .xib file
        xibSetup()
        
    }
    
}


//MARK:- Btn Actions
extension AppBottomBar
{
    @IBAction func btnTabbarSelected(sender:UIButton)
    {
        self.tabSelected(index: sender.tag)
        self.delegate?.tabSelected(index: sender.tag)
    }
}

//MARK:- Helping Methods
extension AppBottomBar
{
    func xibSetup() {
        view = loadViewFromNib()
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        self.appTabView.isHidden = !UIApplication.isMeeraOIA()
        addSubview(view)
        
        self.addObserverForChangeTheme()
        self.addObserverForUnreadCollaborationCount()
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "AppBottomBar", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    
    func tabSelected(index:Int)
    {
        switch index {
        case 0:
            self.setTabImages(tab1: BottomTabData(tabImg: currentTabbItems.tab1.selectedImg, contentMode: .bottom, color: currentTabbItems.tab1.selectedTxtColor),
                              tab2: BottomTabData(tabImg: currentTabbItems.tab2.deSelectedImg, contentMode:.center, color: currentTabbItems.tab2.deSelectedTxtColor),
                              tab3: BottomTabData(tabImg: currentTabbItems.tab3.deSelectedImg, contentMode: .center, color: currentTabbItems.tab3.deSelectedTxtColor),
                              tab4: BottomTabData(tabImg: currentTabbItems.tab4.deSelectedImg, contentMode: .center, color: currentTabbItems.tab4.deSelectedTxtColor),
                              tab5: BottomTabData(tabImg: currentTabbItems.tab5.deSelectedImg, contentMode: .center, color: currentTabbItems.tab5.deSelectedTxtColor))
            break;
        case 1:
            self.setTabImages(tab1: BottomTabData(tabImg: currentTabbItems.tab1.deSelectedImg, contentMode: .center, color: currentTabbItems.tab1.deSelectedTxtColor),
                              tab2: BottomTabData(tabImg: currentTabbItems.tab2.selectedImg, contentMode: .bottom, color: currentTabbItems.tab2.selectedTxtColor),
                              tab3: BottomTabData(tabImg: currentTabbItems.tab3.deSelectedImg, contentMode: .center, color: currentTabbItems.tab3.deSelectedTxtColor),
                              tab4: BottomTabData(tabImg: currentTabbItems.tab4.deSelectedImg, contentMode: .center, color: currentTabbItems.tab4.deSelectedTxtColor),
                              tab5: BottomTabData(tabImg: currentTabbItems.tab5.deSelectedImg, contentMode: .center, color: currentTabbItems.tab5.deSelectedTxtColor))
            break;
        case 2:
            self.setTabImages(tab1: BottomTabData(tabImg: currentTabbItems.tab1.deSelectedImg, contentMode: .center, color: currentTabbItems.tab1.deSelectedTxtColor),
                              tab2: BottomTabData(tabImg: currentTabbItems.tab2.deSelectedImg, contentMode: .center, color: currentTabbItems.tab2.deSelectedTxtColor),
                              tab3: BottomTabData(tabImg: currentTabbItems.tab3.selectedImg, contentMode: .bottom, color: currentTabbItems.tab3.selectedTxtColor),
                              tab4: BottomTabData(tabImg: currentTabbItems.tab4.deSelectedImg, contentMode: .center, color: currentTabbItems.tab4.deSelectedTxtColor),
                              tab5: BottomTabData(tabImg: currentTabbItems.tab5.deSelectedImg, contentMode: .center, color: currentTabbItems.tab5.deSelectedTxtColor))
            break;
        case 3:
            self.setTabImages(tab1: BottomTabData(tabImg: currentTabbItems.tab1.deSelectedImg, contentMode: .center, color: currentTabbItems.tab1.deSelectedTxtColor),
                              tab2: BottomTabData(tabImg: currentTabbItems.tab2.deSelectedImg, contentMode: .center, color: currentTabbItems.tab2.deSelectedTxtColor),
                              tab3: BottomTabData(tabImg: currentTabbItems.tab3.deSelectedImg, contentMode: .center, color: currentTabbItems.tab3.deSelectedTxtColor),
                              tab4: BottomTabData(tabImg: currentTabbItems.tab4.selectedImg, contentMode: .bottom, color: currentTabbItems.tab4.selectedTxtColor),
                              tab5: BottomTabData(tabImg: currentTabbItems.tab5.deSelectedImg, contentMode: .center, color: currentTabbItems.tab5.deSelectedTxtColor))
            break;
        case 4:
            self.setTabImages(tab1: BottomTabData(tabImg: currentTabbItems.tab1.deSelectedImg, contentMode: .center, color: currentTabbItems.tab1.deSelectedTxtColor),
                              tab2: BottomTabData(tabImg: currentTabbItems.tab2.deSelectedImg, contentMode:.center, color: currentTabbItems.tab2.deSelectedTxtColor),
                              tab3: BottomTabData(tabImg: currentTabbItems.tab3.deSelectedImg, contentMode: .center, color: currentTabbItems.tab3.deSelectedTxtColor),
                              tab4: BottomTabData(tabImg: currentTabbItems.tab4.deSelectedImg, contentMode: .center, color: currentTabbItems.tab4.deSelectedTxtColor),
                              tab5: BottomTabData(tabImg: currentTabbItems.tab5.selectedImg, contentMode: .bottom, color: currentTabbItems.tab5.selectedTxtColor))
            break;
        default:
            break;
        }
    }
    
    func setTabImages(tab1:BottomTabData, tab2:BottomTabData, tab3:BottomTabData,tab4:BottomTabData,tab5:BottomTabData)
    {
        
        self.tabBtns[0].setImage(tab1.tabImg.bundleImage, for: UIControl.State.normal)
        self.tabTitles[0].textColor = tab1.color
        //self.tabBtns[0].contentVerticalAlignment = tab1.contentMode
        self.tabBtns[1].setImage(tab2.tabImg.bundleImage, for: UIControl.State.normal)
        self.tabTitles[1].textColor = tab2.color
        //self.tabBtns[1].contentVerticalAlignment = tab2.contentMode
        self.tabBtns[2].setImage(tab3.tabImg.bundleImage, for: UIControl.State.normal)
        self.tabTitles[2].textColor = tab3.color
        //self.tabBtns[2].contentVerticalAlignment = tab3.contentMode
        self.tabBtns[3].setImage(tab4.tabImg.bundleImage, for: UIControl.State.normal)
        self.tabTitles[3].textColor = tab4.color
        //self.tabBtns[3].contentVerticalAlignment = tab4.contentMode
        self.tabBtns[4].setImage(tab5.tabImg.bundleImage, for: UIControl.State.normal)
        self.tabTitles[4].textColor = tab5.color
        //self.tabBtns[4].contentVerticalAlignment = tab5.contentMode
    }
    
    func addObserverForChangeTheme()
    {
        DispatchQueue.main.async {
        AppObservers.appTheme
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] (currentTheme) in
                guard let self = self else {return}
                
                self.tabBackgroundView.backgroundColor = currentTheme.tabBackgrounColor
//                self.tabSelected(index: appDel.rootTabbarController?.selectedIndex ?? 0)
                
            })
            .disposed(by: self.disposeBag!)
        }
    }
    
    func addObserverForUnreadCollaborationCount()
    {
        AppObservers.unReadCollaborationCount
            .subscribe(onNext: { [weak self] (unreadCount) in
                guard let self = self else {return}
                
                DispatchQueue.main.async {
                
                    if unreadCount.total > 0
                    {
                        self.badgeCollaboration.isHidden = false
                        self.lblBadgeCollaboration.text = "\(unreadCount.total)"
                    }
                    else
                    {
                        self.badgeCollaboration.isHidden = true
                        self.lblBadgeCollaboration.text = "0"
                        
                    }
                    
                }

                
            }).disposed(by: self.disposeBag!)
    }
    
}


struct TabImage {
    var selectedImg:String
    var deSelectedImg:String
    var selectedTxtColor:UIColor
    var deSelectedTxtColor:UIColor
}

struct BottomTabData {
    var tabImg:String
    var contentMode:UIControl.ContentVerticalAlignment
    var color:UIColor
}



protocol AppBottomBarDelegate:class {
    func tabSelected(index:Int)
}





