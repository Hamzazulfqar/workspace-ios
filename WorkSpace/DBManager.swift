//
//  DBManager.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation
import UIKit
import CoreData

let DB_MANAGER = DBManager.sharedInstance


class DBManager: NSObject {
    @objc static let sharedInstance = DBManager()

    
    fileprivate lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "Meera");
        
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }();
    
    lazy var backgroundContext: NSManagedObjectContext = {
        
        return persistentContainer.newBackgroundContext();
    }()
    
    lazy var mainContext: NSManagedObjectContext = {
        
        return persistentContainer.viewContext;
    }()
}


extension DBManager
{
    func saveContext(type: ContextType) {
        let context = type == .main ? mainContext : backgroundContext

       // context.performAndWait {
            
            if context.hasChanges {
                do {
                    try context.save()
                } catch {
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
       // }
    }
    
    func saveContext()
    {
        self.saveContext(type: .main);
        self.saveContext(type: .background);
    }
}

extension NSManagedObject
{
    class var entityName : String {
        
        let components = NSStringFromClass(self)
        return components
    }
    
    class func fetchRequestObj() -> NSFetchRequest<NSFetchRequestResult> {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: self.entityName)
        return request
    }
    
    class func fetchRequestObjWithKey(key: String, ascending: Bool = true) -> NSFetchRequest<NSFetchRequestResult> {
        
        let request = fetchRequestObj()
        request.sortDescriptors = [NSSortDescriptor(key: key, ascending: ascending)]
        return request
    }
}
extension NSManagedObjectContext
{
    func insert<T: NSManagedObject>(entity: T.Type) -> T {
        
        let entityName = entity.entityName
        return NSEntityDescription.insertNewObject(forEntityName: entityName, into: self) as! T
    }
    
    func fetchAll<T: NSManagedObject>(entity: T.Type, fetchConfiguration: ((NSFetchRequest<NSManagedObject>) -> Void)?) -> [NSManagedObject]? {
       
        let dataFetchRequest = NSFetchRequest<NSManagedObject>(entityName: entity.entityName)
    
        fetchConfiguration?(dataFetchRequest)
        var result = [NSManagedObject]()
        do {
            result = try self.fetch(dataFetchRequest)
        } catch {
            print("Failed to fetch feed data, critical error: \(error)")
        }
        return result
    }
    
    func fetchWithPredicate<T: NSManagedObject>(entity: T.Type,predicate: NSPredicate, fetchConfiguration: ((NSFetchRequest<NSManagedObject>) -> Void)?) -> [NSManagedObject]? {
        
        let en = entity.entityName;
        let dataFetchRequest = NSFetchRequest<NSManagedObject>(entityName: en)
        
        fetchConfiguration?(dataFetchRequest);
        dataFetchRequest.predicate = predicate;
        
        var result = [NSManagedObject]()
        do {
            result = try self.fetch(dataFetchRequest)
        } catch {
            print("Failed to fetch feed data, critical error: \(error)")
        }
        return result
    }
}


enum ContextType {
    case main
    case background
}
