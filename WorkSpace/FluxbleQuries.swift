//
//  FluxbleQuries.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation

struct FluxbleQuries {

    static let wsByOrganizationID = "query meWorkspacesByOrganization($orgId: String!) { meWorkspacesByOrganization(organizationID: $orgId) { workspaces { id name description organizationID coverURL category { id name color } deletedDate startDate endDate createdBy { id profile { fullName subject pictureURL title email } } wsUsers { wsUsers { user { id profile { userID fullName subject pictureURL title email } } } } } } }"
    
    static let allDueTasksCount = "query dueTaskNumber { dueTaskNumber } "
    
    static let meAgendas = "query meAgendas($data: AgendaFilterEx) { meAgendas(where: $data) { totalCount agendas { id title description location startDate endDate location agendaUsers { agendaUsers { user { id subject profile { fullName photo pictureURL } } category { id name color } } } } } meAgendaCategories { id name color } } "
}
