//
//  Helper.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation
import UIKit
import TFNetwork
import SwiftyJSON

var OrgID : String!

enum SelectedWorkSpace : Int {
    case all = 0
    case managedByMe
    case star
    case archived
    case others
}

class HelperWorkspaceTabVC: NSObject {
    
    
    var allWorkspaces = [WorkspaceModel]()
    var archivedWS = [WorkspaceModel]()
    var managedByMeWS = [WorkspaceModel]()
    var starWS = [WorkspaceModel]()
    var otherWS = [WorkspaceModel]()
    var workspaces = [WorkspaceModel]() {
        didSet {
            archivedWS = workspaces.filter({$0.archived == true})
            managedByMeWS = workspaces.filter({$0.createdBy.userID == UserInfo.userId})
            starWS = workspaces.filter({$0.star == true})
            otherWS = workspaces.filter({$0.isWsAdmin == false && $0.createdBy.userID != UserInfo.userId})
            allWorkspaces = workspaces
        }
    }
    
    var selectedWS : SelectedWorkSpace = .all
    
}

//MARK:- Helper Methods
extension HelperWorkspaceTabVC {
    
    func checkWorkspaceFilter() {
        switch selectedWS {
              case .all:
                  break
              case .managedByMe:
                  break
              case .star:
                    break
              case .archived:
                    break
              default:
                    break
        }
    }
}





//MAKR:- API Calls


extension HelperWorkspaceTabVC {
    
    func getAllWorkSpaces( completion: @escaping (_ error: TFNetwork.ErrorModel?) -> Void) {
        var parameter = [String:Any]()
        var inputPram = [String:Any]()
        inputPram["orgId"] = OrgID //AppObservers.selectedOrganization.value.id
        parameter["query"] = apiGetWorkSpacesByOrgId
        parameter["variables"] = inputPram
             
        Network.asyncCallApiForJSONResult(WithBaseUrl: baseUrl, WithQueryParameter: parameter, WithDelegate: nil) { (json, error) in
                                  
                 
        if let j = json {
                    
            guard (j["data"]["meWorkspacesByOrganization"]["workspaces"].array != nil) else {
                return
            }
            let myWorkspaces = j["data"]["meWorkspacesByOrganization"]["workspaces"].arrayValue;
            self.workspaces.removeAll()
            for j in myWorkspaces {
                self.workspaces.append(WorkspaceModel(json: j))
            }

            }
            
            completion(error )

        }
    }
    
}
//
//extension HelperWorkspaceTabVC {
//
//    func getAllWorkSpaces( completion: @escaping (_ error: TFNetwork.ErrorModel?) -> Void) {
//        var parameter = [String:Any]()
//        var inputPram = [String:Any]()
//        inputPram["orgId"] = OrgID //AppObservers.selectedOrganization.value.id
//        parameter["query"] = apiGetWorkSpacesByOrgId
//        parameter["variables"] = inputPram
//
//        let request = TFRequest(baseUrl: baseUrl, method: .post, bodyPerameter: parameter)
//        TFNetwork.asyncCall(apiRequest: request, requiredResponce: .json, WithDelegate: nil) { (response) in
//            guard let data = response.data else  {
//                completion(response.error)
//                return
//            }
//
//            let j = JSON(data)
//
//
//                guard (j["data"]["meWorkspacesByOrganization"]["workspaces"].array != nil) else {
//                    return
//                }
//                let myWorkspaces = j["data"]["meWorkspacesByOrganization"]["workspaces"].arrayValue;
//                self.workspaces.removeAll()
//                for j in myWorkspaces {
//                    self.workspaces.append(WorkspaceModel(json: j))
//                }
//
//
//        }
//    }
//}
//
