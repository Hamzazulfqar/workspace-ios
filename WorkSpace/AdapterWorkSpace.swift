//
//  AdapterWorkSpace.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation
import UIKit

class AdaptarWorkSpaceTab: NSObject {
    
    private weak var collectionView:UICollectionView!
    private weak var delegate:AdaptarWorkSpaceTabDelgate!
    var workspaces:[WorkspaceModel] = [WorkspaceModel]()
    var theme = AppTheme.light
    
    var parent: UIViewController? {
           return collectionView.viewController
       }
    
    init(collectionView : UICollectionView, delegate : AdaptarWorkSpaceTabDelgate, theme: AppTheme) {
        self.collectionView = collectionView
        self.delegate = delegate
        super.init()
        
        self.theme = theme
        registerCollectionViewCell()
    }
    
    
    func reloadCollectionView() {
        self.collectionView.reloadData()
    }
}


//MARK:- Btn Actions
extension AdaptarWorkSpaceTab {
    
}


//MARK:- Helping Methods
extension AdaptarWorkSpaceTab {
    

}

extension AdaptarWorkSpaceTab: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func registerCollectionViewCell()
    {
        
        self.collectionView.register(UINib(nibName: "WorkspaceCell", bundle: Bundle(for: WorkspaceCell.self)), forCellWithReuseIdentifier: WorkspaceCell.identifier)
        
       // self.collectionView.registerCell(ofType: WorkspaceCell.self, withReuseIdentifier: WorkspaceCell.identifier)
        
        configureCollectionView()
        
    }
    
    func configureCollectionView(){
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return workspaces.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: WorkspaceCell.identifier, for: indexPath) as! WorkspaceCell
        cell.setData(workspaces[indexPath.row])
//        cell.setTheme(theme)
        cell.setTheme(ApplicationManager.shared.appTheme)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            if indexPath.row % 3 == 0 {
                cell.cstViewContainerLeading.constant = 10
                cell.cstViewContainerTrailing.constant = 0
            } else if indexPath.row % 3 == 2 {
                cell.cstViewContainerLeading.constant = 0
                cell.cstViewContainerTrailing.constant = 10
            } else {
                cell.cstViewContainerLeading.constant = 10
                cell.cstViewContainerTrailing.constant = 10
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate.didSelectWorkSpace(indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if Device.IS_IPAD {
            
            let width = (UIScreen.width - 5) / 3
            let h = (width - 20) * 19 / 71
            
            
            // (0.0, 0.0, 810.0, 1080.0) iPad 7th gen
//            if UIScreen.main.bounds.size.width == 810 || UIScreen.main.bounds.size.height == 810 {
//                return CGSize(width: 145, height: 185)
//            }
            return CGSize(width: width, height: h + 66)
        }
        
        let h = (UIScreen.width - 20) * 19 / 71
        return CGSize(width: UIScreen.width, height: h + 12)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return Device.IS_IPAD == true ? 3 : 8
//    }
    
}

protocol AdaptarWorkSpaceTabDelgate:class {
    func didSelectWorkSpace(indexPath:IndexPath)
    
}




