//
//  AppConstants.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa


let meOnCallAlert = "You are already on a call. Please disconnect  and try again later"
let otherUserCallAlert = "is already on another call. Please try later"
var AuthTokenTests: String? = nil {
    didSet {
//        TFNetwork.updateToken(token: nil)
    }
}
let kAlertTitle = Bundle.appDisplayName()




struct AppConstants {
    
    static var safeArea: UIEdgeInsets {
        var padding: UIEdgeInsets = UIEdgeInsets.zero
//        if #available(iOS 11.0, *) {
//            if let b = appDel.window?.safeAreaInsets {
//                padding = b
//            }
//        }
        return padding
    }

    static var navigationBarHeight: CGFloat {

        var topSafeArea = AppConstants.safeArea.top
        if topSafeArea < 20 {
            topSafeArea = 20
        }
        return topSafeArea + 44
    }
    
    static let placeholder = UIImage(named: "placeholder-image")
    static let userPlaceholder = UIImage(named: "user_placeholder")
    static let userPlaceholderSquare = UIImage(named: "user_placeholder_square")
}

struct AppColor {
    
    
    
    
    static var blue:UIColor = UIColor(hexString: "#1979FF") //UIColor(named: "AppBlueColor")!
    static var blackTitle:UIColor = UIColor(hexString: "#171D22") // UIColor(named: "blackTitle")!
    static var greySubtitle:UIColor = UIColor(hexString: "#717677") //UIColor(named: "greySubtitleColor")!
    static var appGray:UIColor = UIColor(hexString: "#5C5E5E") //UIColor(named: "AppGray")!
    
    
    static var lightThemeTitleGrey:UIColor = UIColor(hexString: "747778")
    static var lightThemeCellBg:UIColor = UIColor(hexString: "F0F0F0")
    
    static var darkThemeBlack:UIColor = UIColor(hexString: "16161A")
    static var darkThemeTitleGrey:UIColor = UIColor(hexString: "a6abae")
    static var darkThemeBarBlack:UIColor = UIColor(hexString: "2A2B2C")
    static var darkThemeCellBg:UIColor = UIColor(hexString: "000000")
//    static var darkThemeSeperator:UIColor = "323335".color!
    static var darkThemeSeperator:UIColor = UIColor(hexString: "323335")
    static var darkSearchBar:UIColor = UIColor(named: "323335")!
//    static var darkThemeHeadingGrey:UIColor = "e5e6ec".color!
    static var darkThemeHeadingGrey:UIColor = UIColor(hexString: "e5e6ec")
    
}


struct AppObservers {
    static var appTheme = BehaviorRelay<AppTheme>(value: AppTheme.currentTheme)
    static var unReadCollaborationCount = BehaviorRelay<CollaborationUnreadCount>(value: CollaborationUnreadCount())
    static var selectedOrganization:BehaviorRelay<OrganisationModel> = BehaviorRelay<OrganisationModel>(value: ServerSelectionManager.getUserOrganization()?.organisation ?? OrganisationModel())
    
    static var newNotificationsCount = PublishSubject<Int>()
//    static var appTheme = BehaviorRelay<AppTheme>(value: AppTheme.currentTheme)
}

class CollaborationUnreadCount:NSObject
{
    var member:Int = 0
    var group:Int = 0
    var total:Int {
        return member + group
    }
}


struct AppImages {
    static let placeHolderUserImg = "user_placeholder".bundleImage
    static let placeHolderGeneralImg = "placeholder-image".bundleImage
    static let wsPlaceHolderGeneralImg = "workspace_bg".bundleImage
    
}
