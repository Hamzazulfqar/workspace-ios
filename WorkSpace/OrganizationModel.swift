//
//  OrganizationModel.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation
import SwiftyJSON

class OrganisationModel: NSObject,NSCoding {

    var id:String!
    var name:String!
    
    init(json:JSON) {
        self.id = json["ID"].string
        self.name = json["Name"].string
    }
    
    init(jsonServer:JSON) {
        
        let intID = jsonServer["id"].int
        self.id = String(intID!)
        self.name = jsonServer["name"].string
    }
    
    init(id:String?, name:String?) {
        self.id = id
        self.name = name
        
    }
    
    override init() {
        super.init()
    }
    
    static func personalOrganisation() -> OrganisationModel
    {
        let org = OrganisationModel()
        org.id = "0"
        org.name = "Personal Workspaces"
        return org
    }
    
    required convenience init(coder aDecoder: NSCoder) {
       
        let id = aDecoder.decodeObject(forKey: "id") as? String
        let name = aDecoder.decodeObject(forKey: "name") as? String
        
        self.init(
            id:id,
            name:name
        )
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
       
    }
    
    
}
