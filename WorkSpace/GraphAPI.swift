//
//  GraphAPI.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation

let Q_WorkSpaceListInfo = "query allworkSpaces($subject: String!) { workspacesBySubject(subject: $subject) { totalCount workspaces { id name typ description startDate endDate coverURL deletedDate metadata organizationID createdBy { subject profile { userID email pictureURL fullName displayName subject } } category { id name color } } } }  "

let Q_UserSkills = "query allUserSkills { allUserSkills { userSkills { skill { id skillName } } } }"
