//
//  FluxbleAPIManager.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation
import SDWebImage
import Alamofire
import SwiftyJSON
import TFNetwork

class FluxbleApiManager: NSObject {
    
    static let shared = FluxbleApiManager()
    var sdIMageCache = SDImageCache.shared
    //SDImageCache(namespace: "FluxbleManagerCache")
    var networkManagaer:TFNetworkManager!
    var channelToken = [String:String]()
    
}

extension FluxbleApiManager{
    
    func getNewNotificationsCount() {
        let urlString = REST_API_URL + "/v2/notifications/count"
        var headers: HTTPHeaders  = HTTPHeaders()
        guard let url = URL(string: urlString) else { return }
        headers["Authorization"] = "Bearer \(UserInfo.userAccessToken.accessToken)"
        
        AF.request(url, method: .get, parameters: nil, headers: headers).responseString { (response) in
            if let v = response.value, let count = Int(v) {
                AppObservers.newNotificationsCount.onNext(count)
            }
            
        }
    }
    
    func getEventsCount(completion: @escaping (_ error: TFNetwork.ErrorModel?, _ agendas: [AgendaModel]?) -> Void) {
        
        let inputParamters: [String: Any] = ["leftDate" : Date().stringValue(format: "yyyy-MM-dd") + "T00:00:00Z", "rightDate": Date().addingTimeInterval(60*60*24).stringValue(format: "yyyy-MM-dd") + "T00:00:00Z"]
        
        let parameter: [String: Any] = [
            "query": FluxbleQuries.meAgendas,
            "variables": ["data":inputParamters]
        ]
        
        let request = TFRequest(baseUrl: baseUrl, method: .post, bodyPerameter: parameter)
        
        
        networkManagaer.asyncCall(apiRequest: request, requiredResponce: .json) { (response) in
            
            var agendas = [AgendaModel]()
            guard let data = response.data else {
                completion(response.error,agendas)
                return
                
            }
            
            let json = JSON(data)
            
            if let data = json["data"]["meAgendas"]["agendas"].array {
                agendas = data.map({ (json) -> AgendaModel in
                    AgendaModel(json: json)
                })
            }else{
                completion(response.error , agendas)
            }
            
        }
        
    }
    
    
    //        Network.asyncCallApiForJSONResult(WithBaseUrl: baseUrl, WithQueryParameter: parameter, WithDelegate: nil) { (json, error) in
    //
    //            var agendas = [AgendaModel]()
    //            if error == nil {
    //                if let data = json?["data"]["meAgendas"]["agendas"].array {
    //                    agendas = data.map({ (json) -> AgendaModel in
    //                        AgendaModel(json: json)
    //                    })
    //                }
    //            }
    //            completion(error, agendas)
    //
    //        }
    
    
    func getTodayMeetingCount (workspaceId :String, completion: @escaping (_ count:Int?, _ resultArray : [MeetingCountModel]?, _ error: TFNetwork.ErrorModel?) -> Void) {
        
        let url = "\(REST_API_URL_WS_Meeting)/v2/workspaces/meetings?date=\(Date().stringValue(format: "yyyy-MM-dd")  + "T00:00:00.000Z")&endDate=\(Date().addingTimeInterval(60*60*24).stringValue(format: "yyyy-MM-dd") + "T00:00:00.000Z" )&page=0&size=100&workspaceIds=\(workspaceId)"
        
        let request = TFRequest(baseUrl: baseUrl, method: .get, bodyPerameter: nil);
        
        networkManagaer.asyncCall(apiRequest: request, requiredResponce: .json) { (response) in
            var meetingCount = 0
            var allMeetings = [MeetingCountModel]()
            
            guard let data = response.data else{
                completion(0,allMeetings,response.error)
                return;
            }
            
            let json = JSON(data);
            meetingCount = json["totalElements"].intValue
            
            guard let info = json["content"].array else {
                completion(0,[],nil)
                return;
            }
            
            allMeetings = info.map({ (json) -> MeetingCountModel in
                
                completion(meetingCount,allMeetings,nil)
                return MeetingCountModel(json: json)
                
            }
            )
            
            
            
        }
        
        
        
        //        Network.asyncCallRestFullApi(baseUrl: REST_API_URL_WS_Meeting, pathUrl: "/v2/workspaces/meetings?date=\(Date().stringValue(format: "yyyy-MM-dd")  + "T00:00:00.000Z")&endDate=\(Date().addingTimeInterval(60*60*24).stringValue(format: "yyyy-MM-dd") + "T00:00:00.000Z" )&page=0&size=100&workspaceIds=\(workspaceId)", parameter: nil, serviceMethod: ServiceType.GET, success:
        //            { (json) in
        //                var meetingCount = 0
        //                var allMeetings = [MeetingCountModel]();
        //
        //                guard let data = json["content"].array else {
        //                    completion(0,[],nil)
        //                    return;
        //                }
        //                meetingCount = json["totalElements"].intValue
        //                allMeetings = data.map({ (json) -> MeetingCountModel in
        //                    MeetingCountModel(json: json)
        //                })
        //
        //                completion(meetingCount,allMeetings,nil)
        //
        //
        //        }) { (error) in
        //            completion(0,[],error)
        //
        //        }
    }
    
    func getAllDueTaskCount(completion:@escaping (_ count:Int?,_ error:TFNetwork.ErrorModel?) -> Void )
    {
        var parameter = [String:Any]()
        parameter["query"] = FluxbleQuries.allDueTasksCount
        
        
        let request = TFRequest(baseUrl: baseUrl, method: .post, bodyPerameter: parameter);
        
        networkManagaer.asyncCall(apiRequest : request, requiredResponce: .json) { (response) in
            
            guard let data = response.data else{
                completion(nil,response.error)
                return;
            }
            
            let json = JSON(data);
            
            if let countValue = json["data"]["dueTaskNumber"].int {
                completion(countValue,nil)
                return
            }
            completion(0,nil)
            
        }
        
        //        Network.asyncCallApiForJSONResult(WithBaseUrl: baseUrl, WithQueryParameter: parameter, WithDelegate: nil) { (json, error) in
        //
        //            guard let json = json else {
        //                completion(nil,error)
        //                return
        //            }
        //
        //            if let countValue = json["data"]["dueTaskNumber"].int {
        //                completion(countValue,nil)
        //                return
        //            }
        //            completion(0,nil)
        //        }
    }
    
    
    func getAllNewNotificationCount(workspaceIds: [String]?,orgId: String?, completion: @escaping (_ count : Int?, _ wsIds : [String], _ error: TFNetwork.ErrorModel?) -> Void) {
        
        
        let url = "\(REST_API_URL)/v2/notifications/all?page=0&size=50&sort=createdAt,desc"
        let request = TFRequest(baseUrl: url, method: .get, bodyPerameter: nil)
        networkManagaer.asyncCall(apiRequest: request, requiredResponce: .json) { (response) in
            var stringIds = [String]()
            var allNotifications = [NotificationModel]();
            
            
            guard let data = response.data else{
                completion(0,[],nil)
                return;
            }
            
            let json = JSON(data);
            
            guard let info = json["content"].array else {
                completion(0,[],nil)
                return;
            }
            
            allNotifications = info.map({ (json) -> NotificationModel in
                NotificationModel(json: json)
            })
            
            let currentOrgNotifications = allNotifications.filter({$0.data.orgId == orgId || $0.data.orgId != nil })
            
            let filteredNotifications = currentOrgNotifications.filter { (n) -> Bool in
                (n.data.module == .meeting || n.data.module == .workspace || n.data.module == .taskManagement) && (n.viewed == false)
            }
            
            stringIds = filteredNotifications.map({($0.data.workspaceId ?? "")})
            completion(filteredNotifications.count,stringIds,nil)
            return
        }
        completion(0,[],nil)
        
        
        //        Network.asyncCallRestFullApi(baseUrl: REST_API_URL, pathUrl: "/v2/notifications/all?page=0&size=50&sort=createdAt,desc", parameter: nil, serviceMethod: ServiceType.GET, success:
        //            { (json) in
        //                //var notificationCount = 0
        //                var stringIds = [String]()
        //                var allNotifications = [NotificationModel]();
        //
        //                guard let data = json["content"].array else {
        //                    completion(0,[],nil)
        //                    return;
        //                }
        //                //let pp = json["totalElements"].intValue
        //                //notificationCount = json["size"].intValue
        //
        //                allNotifications = data.map({ (json) -> NotificationModel in
        //                    NotificationModel(json: json)
        //                })
        //
        //                let currentOrgNotifications = allNotifications.filter({$0.data.orgId == orgId || $0.data.orgId != nil })
        //
        //                let filteredNotifications = currentOrgNotifications.filter { (n) -> Bool in
        //                    (n.data.module == .meeting || n.data.module == .workspace || n.data.module == .taskManagement) && (n.viewed == false)
        //                }
        //
        //
        //                stringIds = filteredNotifications.map({($0.data.workspaceId ?? "")})
        //                completion(filteredNotifications.count,stringIds,nil)
        //
        //
        //        }) { (error) in
        //            completion(0,[],error)
        //
        //        }
        
    }
    
    
    func getWsByOrgId(orgID:String,completionHandler:@escaping (_ workspaces:[WorkSpaceSummaryModel]?,_ error:TFNetwork.ErrorModel?)-> Void )
    {
        var parameter = [String:Any]()
        parameter["query"] = FluxbleQuries.wsByOrganizationID
        parameter["variables"] = ["orgId":orgID]
        
        
        
        let request = TFRequest(baseUrl: baseUrl, method: .post, bodyPerameter: parameter)
        networkManagaer.asyncCall(apiRequest: request, requiredResponce: .json, WithDelegate: nil) { (response) in
            guard let data = response.data else {
                completionHandler(nil,response.error)
                return
            }
            
            let json = JSON(data)
            var we = [WorkSpaceSummaryModel]()
            
            if let arrWs = json["data"]["meWorkspacesByOrganization"]["workspaces"].array{
                we = arrWs.map({ (json) -> WorkSpaceSummaryModel in
                    WorkSpaceSummaryModel(json: json)
                })
            }
            completionHandler(we,nil)
            return
            
        }
        
        //        Network.asyncCallApiForJSONResult(WithBaseUrl: baseUrl, WithQueryParameter: parameter, WithDelegate: nil) { (json, error) in
        //            guard let json = json else {
        //                completionHandler(nil,error)
        //                return
        //            }
        //
        //            var ws = [WorkSpaceSummaryModel]()
        //            if let arrWs = json["data"]["meWorkspacesByOrganization"]["workspaces"].array
        //            {
        //                ws = arrWs.map({ (json) -> WorkSpaceSummaryModel in
        //                    WorkSpaceSummaryModel(json: json)
        //                })
        //            }
        //            completionHandler(ws,nil)
        //        }
    }
}
