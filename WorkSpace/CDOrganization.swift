//
//  CDOrganization.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation
import UIKit
import CoreData

@objc(CDOrganization)
class CDOrganization: NSManagedObject {

    @NSManaged public var orgId: String?
    @NSManaged public var orgName: String?
    
    @NSManaged public var s_name: String?
    @NSManaged public var label: String?
    @NSManaged public var redirectUrl: String?
    @NSManaged public var collaborationUrl: String? // direct and group call
    @NSManaged public var jitsiUrl: String? // venue meeting
    @NSManaged public var organizations_num: NSNumber?
    @NSManaged public var isConnect: Bool
}

extension CDOrganization {
    
    static func saveNewOrganization(org: RUserOrganization, isConnect: Bool)
    {
        guard org.server.redirectUrl != nil else
        {
            return;
        }
        
        var newOrg = self.isOrganizationExist(orgId: org.organisation.id,redirectUrl: org.server.redirectUrl)
        
        if newOrg == nil
        {
            // Create New
            newOrg = CDOrganization.createNewOrganization()
            
            newOrg!.orgId = org.organisation.id
            newOrg!.orgName = org.organisation.name
            newOrg!.s_name = org.server.name
            
            newOrg!.redirectUrl = org.server.redirectUrl
            newOrg!.collaborationUrl = org.server.collaborationUrl
            newOrg!.jitsiUrl = org.server.jitsiUrl
            newOrg!.organizations_num = NSNumber(value: org.server.organizations_num)
            
            newOrg!.isConnect = isConnect
            
        }
        else
        {
            //Update
            newOrg!.orgId = org.organisation.id
            newOrg!.orgName = org.organisation.name
            newOrg!.s_name = org.server.name
            
            newOrg!.redirectUrl = org.server.redirectUrl
            newOrg!.collaborationUrl = org.server.collaborationUrl
            newOrg!.jitsiUrl = org.server.jitsiUrl
            newOrg!.organizations_num = NSNumber(value: org.server.organizations_num)
            
            newOrg!.isConnect = isConnect
            
        }
        
        DB_MANAGER.saveContext(type: .main);
    }
    
    private static func createNewOrganization()-> CDOrganization
    {
        let server = DB_MANAGER.mainContext.insert(entity: CDOrganization.self);
        return server;
    }
    
    private static func isOrganizationExist(orgId: String, redirectUrl: String) -> CDOrganization?
    {
        let predicate = NSPredicate(format:"orgId == '\(orgId)' && redirectUrl == '\(redirectUrl)'");
        
        let filteredArray = DB_MANAGER.mainContext.fetchWithPredicate(entity: CDOrganization.self, predicate: predicate, fetchConfiguration: nil);
       
        if let arr = filteredArray as? [CDOrganization], arr.count > 0
        {
            return arr[0];
        }
        
        return nil;
    }
    
    static func getConnectedOrganization() -> CDOrganization?
    {
        let predicate = NSPredicate(format:"isConnect == true");
        
        let filteredArray = DB_MANAGER.mainContext.fetchWithPredicate(entity: (CDOrganization.self), predicate: predicate, fetchConfiguration: nil) as! [CDOrganization];
        
        if filteredArray.count > 0
        {
            return filteredArray[0];
        }
        
        return nil;
    }
    
    static func deleteAll()
    {
        let filteredArray = DB_MANAGER.mainContext.fetchAll(entity: CDOrganization.self, fetchConfiguration: nil) as? [CDOrganization];

        if let arr = filteredArray, arr.count > 0
        {
            for obj in arr
            {
                DB_MANAGER.mainContext.delete(obj);
            }
            
            DB_MANAGER.saveContext(type: .main);
        }
        
    }
}

extension CDOrganization {
    
    func asRUserOrganization() -> RUserOrganization {
        
        return RUserOrganization(cdorg: self)
    }
}
