//
//  QaisAPIs.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation

class QaisApis: NSObject {

}

let directory : String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]




let apiGetWorkSpacesByOrgId = "query meWorkspacesByOrganization($orgId: String!) { meWorkspacesByOrganization(organizationID: $orgId) { workspaces { isWsAdmin star archived id name typ description startDate endDate coverURL deletedDate organizationID metadata wsUsers { wsUsers { user { id subject profile { userID email pictureURL fullName displayName subject } } } } createdBy { subject profile { userID email pictureURL fullName displayName subject } } category { id name color } } } } "

let apiDeleteProjectAttachments = "mutation deleteProjectAttachements($input: [DeleteProjectAttachementInput!]!) { deleteProjectAttachements(input: $input)}"

let apiGetSkillLevel = "query{ allSkillLevels(offset: 0, limit: 1000) { skillLevels { id levelTitle } } } "

let apiGetAllSkill = "query { allSkills { skills { id skillName } } }"
