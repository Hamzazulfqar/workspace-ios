//
//  WorkspaceTabVC.swift
//  Meera
//
//  Created by Muhammad Arslan Khalid on 23/03/2020.
//  Copyright © 2020 Muhammad Arslan Khalid. All rights reserved.
//

import UIKit
import FittedSheets
import RxSwift
import RxCocoa
import MBProgressHUD
import TFNetwork

public protocol WorkspaceDelegate: class {
    func didTapTasks()
    func didTapDocuments()
    func openWorkSpaceFilterView()
    func updateFilterStatus()
    func didSelectWorkSpace()
    func didTapNewNotification()
    func didTapDueTasks()
    func didTapMeetingsToday()
    func didTapEventsToday()
    func selectOrganizaation()
    func themeChanged()
    //didTapDueTasksButton  didTapMeetingsTodayButton  didTapEventsTodayButton
}

public class WorkspaceTabVC: UIViewController {
    
    // @IBOutlet weak var topConstraint:NSLayoutConstraint!
    @IBOutlet weak var notificationCountView: WSNotificationCountView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
//    @IBOutlet weak var workSpaceFilterView: UIView!
    @IBOutlet weak var lblTask: UILabel!
    @IBOutlet weak var lblDocument: UILabel!
//
    @IBOutlet weak var viewContainerTasks: CardView!
    @IBOutlet weak var viewContainerDocument: CardView!

    private var adataptar:AdaptarWorkSpaceTab!
    private var helper = HelperWorkspaceTabVC()
    var meeetingCountArray = [MeetingCountModel]()
    var notificationCountArray = [String]()
    var eventCountArray = [AgendaModel]()
    let disposeBag = DisposeBag()
    public weak var delegate : WorkspaceDelegate?
    public var domain = ""
    
       @IBOutlet weak var totalWSLabel: UILabel!
      @IBOutlet weak var filerImageView: UIImageView!
    @IBOutlet weak var topNavBar: SearchNavBarView!
   
    public  var theme: AppTheme = .light {
        didSet {
            updateTheme()
        }
    }
    
    var showBackButton = true
    var firstTimeLoad = true
    public var networkManagaer:TFNetworkManager!
   public var orgID : String!
    
    
    public init(networkManagaer:TFNetworkManager , orgID : String , appTheme : AppTheme , domain : String) {
        self.networkManagaer = networkManagaer
        FluxbleApiManager.shared.networkManagaer = networkManagaer;
        let bundle = Bundle(for: WorkspaceTabVC.self)
        self.orgID = orgID
        OrgID = orgID
        super.init(nibName: "WorkspaceTabVC", bundle: bundle);
        theme = appTheme
        ApplicationManager.shared.appTheme = appTheme
        ApplicationManager.shared.domainName = domain
        
        updateTheme()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    //
    public override func viewDidLoad() {
        super.viewDidLoad()
        configureAdaptar()
        self.addObserverForThemeChange()
//
        addObserverForNewNotificationsCount()
        self.addObserverOfTxtChanged()
//
        topNavBar.delegate = self
        topNavBar.leftIconImage = showBackButton ? "back_grey_small".bundleImage : "side_menu_icon".bundleImage
        topNavBar.leftIconDarkImage = showBackButton ? "back_white_small".bundleImage : "side_menu_icon_dark".bundleImage
        self.observeOrganizationValueChanged()
//
        notificationCountView.delegate = self
        self.internetChangedNotifier()
    }
//
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        appDel.topMostController = self
        FluxbleApiManager.shared.getNewNotificationsCount()
    }
//
    public override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if UIDevice.current.orientation.isLandscape {
            self.adataptar.reloadCollectionView()
            print("Landscape")

        } else {
            print("Portrait")
            self.adataptar.reloadCollectionView()

        }

    }
//
//
//
    func addObserverForNewNotificationsCount() {
        AppObservers.newNotificationsCount.subscribe(onNext: { [weak self] (value) in
            guard let self = self else {return}
            self.topNavBar.badgeRightBarButton1 = value

        }).disposed(by: self.disposeBag)
    }
//
    func addObserverForThemeChange() {
        AppObservers.appTheme.distinctUntilChanged().subscribe(onNext: { [weak self] (currentTheme) in
            guard let self = self else {return}
            self.theme = currentTheme
            self.adataptar.theme = currentTheme
            self.collectionView.reloadData()

        }).disposed(by: self.disposeBag)
    }

        @IBAction func didTapTasksButton(_ sender: UIButton) {
            print("didTapTasksButton action")
            delegate?.didTapTasks()
//        var tasksVC: UIViewController
//
//        if Device.IS_IPAD {
//            let controller = TasksVC(nibName: TasksVC.name, bundle: nil)
//            controller.theme = theme
//            tasksVC = controller
//        } else {
//            let controller = TasksVC(nibName: TasksVC.name, bundle: nil)
//            controller.theme = theme
//            tasksVC = controller
//        }
//        self.navigationController?.pushViewController(tasksVC, animated: true)

    }
    
    

    
    
    
    @IBAction func didTapDocumentsButton(_ sender: UIButton) {
        print("didTapDocumentsButton action")
        delegate?.didTapDocuments()
//        let fileManagerVc = FileWorkspaceListingVC(nibName: FileWorkspaceListingVC.name, bundle: nil)
//        self.navigationController?.pushViewController(fileManagerVc, animated: true)
    }
//
//}
//
//

//
//
////MARK:- Helping Mwthods
//extension WorkspaceTabVC
//{
    func observeOrganizationValueChanged()
    {
        AppObservers.selectedOrganization
            .distinctUntilChanged()
            .subscribe { [weak self] (newValue) in
                guard let self = self else {return}
                self.updateTheme()
                self.loadData()
        }
            .disposed(by: self.disposeBag)
    }
//
    func configureAdaptar()
    {
        self.adataptar = AdaptarWorkSpaceTab(collectionView: self.collectionView, delegate: self, theme: theme)
    }
//
    @objc func openWorkSpaceFilterView() {
        print("openWorkSpaceFilterView called")
        delegate?.openWorkSpaceFilterView()
//
//        DispatchQueue.main.async {
//            let wsListVC = WorkspaceFilterVC()
//            wsListVC.totalCount = [self.helper.workspaces.count, self.helper.managedByMeWS.count, self.helper.starWS.count, self.helper.archivedWS.count, self.helper.otherWS.count]
//            wsListVC.delegate = self
//            wsListVC.selectedWS = self.helper.selectedWS
//            let sheetVC = SheetViewController(controller: wsListVC)
//            sheetVC.setSizes([.fixed(390)])
//            // let sheetVC = SheetViewController(controller: wsListVC, sizes: [.fullScreen, .fixed(350), .halfScreen, .halfScreen])
//            sheetVC.blurBottomSafeArea = false
//            sheetVC.adjustForBottomSafeArea = true
//            sheetVC.topCornersRadius = 15
//            sheetVC.dismissOnBackgroundTap = false
//            sheetVC.extendBackgroundBehindHandle = true
//            sheetVC.handleColor = self.theme == .light ? .lightGray : AppColor.darkThemeCellBg
//            sheetVC.containerView.backgroundColor = .clear
//            self.present(sheetVC, animated: false, completion: nil)
//        }
//
    }
//
    func loadData() {

        if helper.workspaces.count == 0{
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }

        helper.getAllWorkSpaces { [weak self] (error) in
            guard let self = self else {return}

            MBProgressHUD.hide(for: self.view, animated: true)
            self.firstTimeLoad = false
            if error != nil {
                MBProgressHUD.hide(for: self.view, animated: true)
                Alert.showServerErrors(controller: self, errorModel: error!)
                return
            }
            self.updateFilterStatus()
            self.getTaskCount()
            self.getTodayMeetingCount()
            self.getEventCount()
            self.getAllNotificationCount()
        }
    }
//
    func getTaskCount()  {
        print("getTaskCount called")
        FluxbleApiManager.shared.getAllDueTaskCount { (count, error) in
            if error == nil {
                self.notificationCountView.lblDueTaskCount.text = count ?? 0 < 9 ? "0\(count ?? 0)" : "\(count ?? 0)"
                if count ?? 0 == 0 {
                    self.notificationCountView.lblDueTaskCount.text = "0"
                }
            }
        }
    }
//
    func getTodayMeetingCount() {
        print("getTodayMeetingCount called")
        let wsIds = self.helper.workspaces.compactMap({$0.id}).joined(separator: ",")
        FluxbleApiManager.shared.getTodayMeetingCount(workspaceId: wsIds) { (count,meetingArray,error) in
            if error == nil {
                self.notificationCountView.lblMeetingCount.text = count ?? 0 < 9 ? "0\(count ?? 0)" : "\(count ?? 0)"
                if count ?? 0 == 0 {
                    self.notificationCountView.lblMeetingCount.text = "0"
                }
                self.meeetingCountArray = meetingArray ?? [MeetingCountModel]()
            }
        }
    }
//
    func getEventCount() {
        print("getEventCount called")
        FluxbleApiManager.shared.getEventsCount { (error, result) in
            if error == nil {
                self.notificationCountView.lblEventCount.text = result!.count < 9 ? "0\(result?.count )" : "\(result?.count)"
                if result?.count == 0 {
                    self.notificationCountView.lblEventCount.text = "0"
                }
                self.eventCountArray = result!
            }
        }
    }
//
    func getAllNotificationCount() {
        print("getAllNotificationCount called")
        let workspworkspacesIds = self.adataptar.workspaces.map({$0.id}) as? [String]
        let orgid = UserInfo.organisationID

        FluxbleApiManager.shared.getAllNewNotificationCount(workspaceIds: workspworkspacesIds, orgId: orgid) { (count,wsIds, error) in
            if error == nil {
                self.notificationCountView.lblNotificationCount.text = count ?? 0 < 9 ? "0\(count ?? 0 )" : "\(count ?? 0)"
                if count ?? 0 == 0 {
                    self.notificationCountView.lblNotificationCount.text = "0"
                }
                self.notificationCountArray = wsIds
            }
        }
    }
//
    func updateFilterStatus() {
        print("updateFilterStatus called")
        delegate?.updateFilterStatus()
        self.adataptar.workspaces = self.helper.workspaces
        self.adataptar.reloadCollectionView()
/*
        switch self.helper.selectedWS {
        case .all:
            //  self.totalWSLabel.text = "\(self.helper.workspaces.count)"
            //self.filerImageView.image = UIImage(named: "workspace_icon")
            self.adataptar.workspaces = self.helper.workspaces


        case .managedByMe:
            //  self.totalWSLabel.text = "\(self.helper.managedByMeWS.count)"
            // self.filerImageView.image = UIImage(named: "managed_by_me")
            self.adataptar.workspaces = self.helper.managedByMeWS


        case .star:
            //  self.totalWSLabel.text = "\(self.helper.starWS.count)"
            // self.filerImageView.image = UIImage(named: "starred_workspaces")
            self.adataptar.workspaces = self.helper.starWS

        case .archived:
            //  self.totalWSLabel.text = "\(self.helper.archivedWS.count)"
            //  self.filerImageView.image = UIImage(named: "declined")
            self.adataptar.workspaces = self.helper.archivedWS


        default:
            // self.totalWSLabel.text = "\(self.helper.otherWS.count)"
            // self.filerImageView.image = UIImage(named: "other_workspace")
            self.adataptar.workspaces = self.helper.otherWS

        }

        self.helper.allWorkspaces = self.adataptar.workspaces
        self.adataptar.reloadCollectionView()
*/

    }
    
    
    func getAllNewNotificationCount(workspaceIds: [String]?,orgId: String?, completion: @escaping (_ count : Int?, _ wsIds : [String], _ error: ErrorModel?) -> Void) {
        
        Network.asyncCallRestFullApi(baseUrl: REST_API_URL, pathUrl: "/v2/notifications/all?page=0&size=50&sort=createdAt,desc", parameter: nil, serviceMethod: ServiceType.GET, success:
            { (json) in
                //var notificationCount = 0
                var stringIds = [String]()
                var allNotifications = [NotificationModel]();
                
                guard let data = json["content"].array else {
                    completion(0,[],nil)
                    return;
                }
                //let pp = json["totalElements"].intValue
                //notificationCount = json["size"].intValue
                
                allNotifications = data.map({ (json) -> NotificationModel in
                    NotificationModel(json: json)
                })
                
                let currentOrgNotifications = allNotifications.filter({$0.data.orgId == orgId || $0.data.orgId != nil })
                
                let filteredNotifications = currentOrgNotifications.filter { (n) -> Bool in
                    (n.data.module == .meeting || n.data.module == .workspace || n.data.module == .taskManagement) && (n.viewed == false)
                }
                
                
                
                
                stringIds = filteredNotifications.map({($0.data.workspaceId ?? "")})
                completion(filteredNotifications.count,stringIds,nil)
                
                
        }) { (error) in
            completion(0,[],error as? ErrorModel)
            
        }
        
    }
    
//
    func selectOrganization() {
        print("Select organization called")
        delegate?.selectOrganizaation()
//
//        guard let organizations = appDel.rootTabbarController?.organizations, let selectedOrganizationIndex = appDel.rootTabbarController?.selectedOrganizationIndex else { return }
//        let organisationListVC = OrganizationListVC(organisations: organizations, selectedOrg: organizations[selectedOrganizationIndex]) { (organizationModel) in
//            guard let organizationModel = organizationModel else {
//                return
//            }
//
//
//            AppObservers.selectedOrganization.accept(organizationModel)
//            if let index = appDel.rootTabbarController?.getIndexOfOrg(org: organizationModel) {
//                appDel.rootTabbarController?.selectedOrganizationIndex = index
//            }
//        }
//
//
//        DispatchQueue.main.async {
//            let sheetVC = SheetViewController(controller: organisationListVC)
//            sheetVC.setSizes([.fixed(CGFloat((44 * organizations.count) + 160)),.fullScreen,.halfScreen])
//            sheetVC.blurBottomSafeArea = false
//            sheetVC.adjustForBottomSafeArea = true
//            sheetVC.topCornersRadius = 15
//            sheetVC.dismissOnBackgroundTap = false
//            sheetVC.extendBackgroundBehindHandle = true
//            sheetVC.dismissOnBackgroundTap = true
//            sheetVC.handleColor = self.theme == .light ? .lightGray : AppColor.darkThemeCellBg
//            self.navigationController?.present(sheetVC, animated: true, completion: nil)
//        }

    }
//
//
    func updateTheme() {

        if theme == .light {
            self.view.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
            self.separatorView.backgroundColor = #colorLiteral(red: 0.9137254902, green: 0.9176470588, blue: 0.9254901961, alpha: 1)
            lblTask.textColor = UIColor.AppWSLightModeBlackColor
            lblDocument.textColor = UIColor.AppWSLightModeBlackColor
            self.viewContainerTasks.backgroundColor = .white
            self.viewContainerDocument.backgroundColor = .white
            self.view.viewWithTag(204)?.backgroundColor = .white
            self.view.viewWithTag(203)?.backgroundColor = #colorLiteral(red: 0.9137254902, green: 0.9176470588, blue: 0.9254901961, alpha: 1)
            self.viewContainerTasks.showShaddow = true
            self.viewContainerDocument.showShaddow = true
            self.notificationCountView.theme = .light


        } else {
            self.view.backgroundColor = AppColor.darkThemeBlack
            self.separatorView.backgroundColor = #colorLiteral(red: 0.2196078431, green: 0.2274509804, blue: 0.2274509804, alpha: 1)
            lblTask.textColor = UIColor.AppWSDarkModeWhiteColor
            lblDocument.textColor = UIColor.AppWSDarkModeWhiteColor
            self.viewContainerTasks.backgroundColor = #colorLiteral(red: 0.09411764706, green: 0.09411764706, blue: 0.1098039216, alpha: 1)
            self.viewContainerDocument.backgroundColor = #colorLiteral(red: 0.09411764706, green: 0.09411764706, blue: 0.1098039216, alpha: 1)
            self.view.viewWithTag(204)?.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            self.view.viewWithTag(203)?.backgroundColor = #colorLiteral(red: 0.2196078431, green: 0.2274509804, blue: 0.2274509804, alpha: 1)
            self.notificationCountView.theme = .dark
            


        }
        if Device.IS_IPAD {
            if showCreateWorkspaceButton() {
                topNavBar.viewRightBarButton4.isHidden = false
            } else {
                topNavBar.viewRightBarButton4.isHidden = true
            }
        } else {
            topNavBar.viewRightBarButton4.isHidden = true
        }
        
        
    }
//
    func showCreateWorkspaceButton()-> Bool {
        if UserInfo.userRoles.contains("target-subscription-store:\(AppObservers.selectedOrganization.value.id ?? ""):Admin") {
            return true
        }

        if UserInfo.userRoles.contains("target-subscription-store:\(AppObservers.selectedOrganization.value.id ?? ""):Creator") {
            return true
        }
        return false
    }
//
//
//
//
//}
//

}

//MARK:- TextField Search Methods
extension WorkspaceTabVC
{
    func addObserverOfTxtChanged()
    {
        self.topNavBar.txtSearchBar.rx.controlEvent(UIControl.Event.editingChanged)
            .asObservable()
            .withLatestFrom(self.topNavBar.txtSearchBar.rx.text)
            .subscribe(onNext: { (text) in

                guard  AppUtility.hasValidText(text) else {
                    self.adataptar.workspaces = self.helper.workspaces
                    self.helper.allWorkspaces = self.adataptar.workspaces
                    self.adataptar.reloadCollectionView()
                    return
                }

                self.adataptar.workspaces = self.helper.workspaces.filter{(user) -> Bool in
                    return user.name.lowercased().contains(text!.lowercased())
                }
                self.helper.allWorkspaces = self.adataptar.workspaces
                self.adataptar.reloadCollectionView()
            }).disposed(by: self.disposeBag)

    }
}

////MARK: - Actions
extension WorkspaceTabVC: WSNotificationCountViewDelegate {

    func didTapNewNotificationButton(_ sender: UIButton) {
        print("didTapNewNotificationButton")
        delegate?.didTapNewNotification()
//        let controller = NotificationsVC(nibName: NotificationsVC.name, bundle: nil)
//        controller.theme = theme
//        let navigationController = UINavigationController(rootViewController: controller)
//        navigationController.isNavigationBarHidden = true
//        self.present(navigationController, animated: true, completion: nil)
    }
    
    //didTapDueTasksButton  didTapMeetingsTodayButton  didTapEventsTodayButton
    func didTapDueTasksButton(_ sender: UIButton) {
        print("didTapDueTasksButton")
        delegate?.didTapDueTasks()
//        self.didTapTasksButton(sender)

    }
    func didTapMeetingsTodayButton(_ sender: UIButton) {
        print("didTapMeetingsTodayButton")
        delegate?.didTapMeetingsToday()
//        (tabBarController as? MeeraTabbarVC)?.setSelectedTab(2)
    }
    func didTapEventsTodayButton(_ sender: UIButton) {
        print("didTapEventsTodayButton")
        delegate?.didTapEventsToday()
//        (tabBarController as? MeeraTabbarVC)?.setSelectedTab(3)
    }

}
//MARK:- AdaptarWorkspaceTabDelegate Methods
extension WorkspaceTabVC:AdaptarWorkSpaceTabDelgate
{
    func didSelectWorkSpace(indexPath: IndexPath) {
        print(" didSelectWorkSpace tapped")
        delegate?.didSelectWorkSpace()
//        let vc = WorkspaceDetailVC()
//        vc.wsId = adataptar.workspaces[indexPath.row].id
//        vc.meetingCount = meeetingCountArray.filter({$0.workspaceId == adataptar.workspaces[indexPath.row].id}).count
//        vc.eventCount = eventCountArray.count
//        vc.newNotificationCount = notificationCountArray.filter({$0 == adataptar.workspaces[indexPath.row].id}).count
//        self.navigationController?.pushViewController(vc, animated: true)
    }

}
//
////MARK:- AdaptarWorkspaceFilterDelegate Methods
//
//extension WorkspaceTabVC :AdaptarWorkspaceFilterDelegate
//{
//
//    func didSelectAtWs(selectedWS : SelectedWorkSpace) {
//        self.helper.selectedWS = selectedWS
//        self.updateFilterStatus()
//    }
//
//}
//
////MARK:- SearchNavBar Delegate
extension WorkspaceTabVC: SearchNavBarDelegate {

    func searchNavBarView(_ navigatonBar: SearchNavBarView, didTapLeftButton button: UIButton) {
//        if showBackButton {
//            goBack()
//        } else {
//
//            let controller = ProfileMenuVC(theme: theme) { (type) in
//                switch type {
//                case .connectOnWeb:
//                    guard let requestUrl = NSURL(string: internalWebSiteUrl + "/\(AppObservers.selectedOrganization.value.id ?? "")/ws-task-manager") else {
//                        return
//                    }
//                    UIApplication.shared.open(requestUrl as URL, options: [:], completionHandler: nil)
//                    break
//                case .designOrganization:
//                    guard AppUtility.hasValidText(AppObservers.selectedOrganization.value.id) else {
//                        return
//                    }
//                    //self.selectUserOrganization()
//                    let selectOrg = ServerSelectionManager.getSelectedOrganization()?.asRUserOrganization()
//                    ServerSelectionManager.getUserOrganizationList(selectedOrg: selectOrg, delegate: self)
//
//                    break
//                case .planner:
//                    let controller  = PlannerMeetingTabVC(nibName: UIDevice.current.userInterfaceIdiom == .pad ? "PlannerMeetingTabVC_iPad" : "PlannerMeetingTabVC", bundle: nil)
//                    self.present(controller, animated: true, completion: nil)
//                    break
//                case .profile:
//                    self.loadProfile()
//                    break
//                case .preferences:
////                    let controller = PreferenceListingVC(nibName: PreferenceListingVC.name, bundle: nil)
////                    self.present(controller, animated: true, completion: nil)
//
//                    let vc = LanguageRegionVC()
//                    vc.modalPresentationStyle = .overCurrentContext
//                    self.present(vc, animated: true, completion: nil)
//                    break
//                case .changeTheme:
//
//                    let selectTheme = SelectThemeVC(nibName: SelectThemeVC.name, bundle: nil)
//                    let sheetVC = SheetViewController(controller: selectTheme)
//                    sheetVC.setSizes([.fixed(250)])
//                    sheetVC.blurBottomSafeArea = false
//                    sheetVC.adjustForBottomSafeArea = false
//                    sheetVC.topCornersRadius = 15
//                    sheetVC.extendBackgroundBehindHandle = true
//                    sheetVC.dismissOnBackgroundTap = true
//                    sheetVC.handleColor = self.theme == .light ? .lightGray : AppColor.darkThemeCellBg
//
//                    self.present(sheetVC, animated: true, completion: nil)
//
//                    break
//                case .changePassword:
//                    let controller = ChangePasswordVC(nibName: ChangePasswordVC.name, bundle:nil)
//                    self.present(controller, animated: true, completion: nil)
//                    break
//
//                case .signOut:
//                    AppUtility.logoutApp(delegate: self)
//                    break
//                case .workspaces:
//                    guard AppUtility.hasValidText(AppObservers.selectedOrganization.value.id) else {
//                        return
//                    }
//                    self.present(AppUtility.getWorkspaceController(), animated: true, completion: nil)
//                    break;
//                }
//            }
//            self.present(controller, animated: true, completion: nil)
//        }
    }

    func searchNavBarViewShouldBeginEditing(_ navigatonBar: SearchNavBarView) -> Bool {
        return true
    }
    func searchNavBarView(_ navigatonBar: SearchNavBarView, textDidChange text: String) {
        print("textDidChange to \(text)")
    }
    func searchNavBarViewDidCancelSearch(_ navigatonBar: SearchNavBarView) {
        self.adataptar.workspaces = self.helper.workspaces
        self.helper.allWorkspaces = self.adataptar.workspaces
        self.adataptar.reloadCollectionView()
    }
    func searchNavBarView(_ navigatonBar: SearchNavBarView, didTapRightButton1 button: UIButton) {
/*
        let controller = NotificationsVC(nibName: NotificationsVC.name, bundle: nil)
        controller.theme = theme
        let navigationController = UINavigationController(rootViewController: controller)
        navigationController.isNavigationBarHidden = true
        self.present(navigationController, animated: true, completion: nil)
        
        */

//        let controller = NotificationListingVC(nibName: NotificationListingVC.name, bundle: nil)
//        let navigationController = UINavigationController(rootViewController: controller)
//        navigationController.isNavigationBarHidden = true
//        self.present(navigationController, animated: true, completion: nil)
//        print("didTapSearchBarButton1")
//        print("didTapSearchBarButton1")
    }
    func searchNavBarView(_ navigatonBar: SearchNavBarView, didTapRightButton2 button: UIButton) {
        guard let requestUrl = NSURL(string: internalWebSiteUrl + "/\(AppObservers.selectedOrganization.value.id ?? "")") else {
            return
        }
        UIApplication.shared.open(requestUrl as URL, options: [:], completionHandler: nil)

        //        let controller = OpenWebSiteVC(nibName: OpenWebSiteVC.name, bundle: nil)
        //        controller.urlString = internalWebSiteUrl
        //        let navigationController = UINavigationController(rootViewController: controller)
        //        navigationController.isNavigationBarHidden = true
        //        self.present(navigationController, animated: true, completion: nil)
        //        print("didTapSearchBarButton2")
        //AppObservers.selectedOrganization.value.id
    }

    func searchNavBarView(_ navigatonBar: SearchNavBarView, didTapRightButton3 button: UIButton) {
/*
        self.openWorkSpaceFilterView()

        print("didTapRightButton")
 
 */
    }

    func searchNavBarView(_ navigatonBar: SearchNavBarView, didTapRightButton4 button: UIButton) {
/*
        if Device.IS_IPAD {

            if showCreateWorkspaceButton() {
                let controller = CreateWorspaceVC_iPad(nibName: CreateWorspaceVC_iPad.name, bundle: nil)
                self.present(controller, animated: true, completion: nil)
            }
        }
        print("didTapRightButton")
 
 */
    }

}
//
//
//// MARK: - Utility Methods
//extension WorkspaceTabVC {
//
//    func loadProfile() {
//
//        let profileCtrl = MeeraspaceProfileViewController(nibName: "MeeraspaceProfileViewController", bundle: nil)
//        appUtility.rooNavController = UINavigationController(rootViewController: profileCtrl)
//        appUtility.rooNavController.isNavigationBarHidden = true
//        //present(appUtility.rooNavController, animated: true, completion: nil)
//        appDel.rootNavController?.present(appUtility.rooNavController, animated: true, completion: nil)
//    }
//
//}
//
//
extension WorkspaceTabVC {
    func internetChangedNotifier()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(networkStatusChanged(_:)), name: NSNotification.Name.reachabilityChanged, object: AppUtility.reachability)
        AppUtility.startNotifier()

    }


    @objc func networkStatusChanged(_ notification: Notification) {
        // Do something globally here!

        let reachable = notification.object as! Reachability

        if reachable.connection != .none
        {
            if !firstTimeLoad {
                self.loadData()
            }
        }
        else
        {

        }


    }
}

