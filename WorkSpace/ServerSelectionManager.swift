//
//  ServerSelectionManager.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation
import FittedSheets
import SwiftyJSON

struct ServerSelectionManager {
    
    static func getUserOrganization() -> RUserOrganization? {
        
        if let decoded = UserDefaults.standard.data(forKey: "RUserOrganization") {
            
            if let orgobjc = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? RUserOrganization {
                return orgobjc
            }
            
            if let orgobjc = try? NSKeyedUnarchiver.unarchivedObject(ofClass: RUserOrganization.self, from: decoded) {
                return orgobjc
            }
        }
        
        return nil
    }
}


class RUserOrganization: NSObject, NSCoding {
    
    var organisation: OrganisationModel = OrganisationModel()
    var server: UserServer!
    
    override init() {
        organisation = OrganisationModel()
        server = UserServer()
    }
    
    init(cdorg: CDOrganization) {
        self.organisation = OrganisationModel(id: cdorg.orgId, name: cdorg.orgName)
        self.server = UserServer(redirectUrl: cdorg.redirectUrl, jitsiUrl: cdorg.jitsiUrl, collaborationUrl: cdorg.collaborationUrl, organizations_num: cdorg.organizations_num?.intValue, name: cdorg.s_name, organisation: nil)
    }
    
    init(organisation: OrganisationModel?, server: UserServer?) {
        self.organisation = organisation ?? OrganisationModel()
        self.server = server ?? UserServer()
    }
    
    init(organizationJson:JSON, json: JSON) {
        
        self.server = UserServer(json: json)
        self.organisation = OrganisationModel(jsonServer: organizationJson)
    }
    
    init(dic: [String: String]) {
        self.organisation = OrganisationModel(id: "0", name: "")
        self.server = UserServer(dic: dic)
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        
        let organisation = aDecoder.decodeObject(forKey: "organisation") as? OrganisationModel
        let server = aDecoder.decodeObject(forKey: "server") as? UserServer
        
        self.init(organisation: organisation, server: server)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(organisation, forKey: "organisation")
        aCoder.encode(server, forKey: "server")
        
    }
}


//MARK:- RUserOrganization AND UserServer CLASS
class UserServer: NSObject, NSCoding {
    
    let name: String!
    let redirectUrl: String!
    let jitsiUrl: String!
    let collaborationUrl: String!
    var organizations_num: Int = 0
    var organisation: [OrganisationModel] = [OrganisationModel]()
    
    override init() {
        
        self.name = ""
        self.redirectUrl = ""
        self.jitsiUrl = ""
        self.collaborationUrl = ""
        self.organizations_num = 0
        //self.organisation = [OrganisationModel]()
        super.init()
    }
    
    init(redirectUrl: String?, jitsiUrl: String?, collaborationUrl: String?, organizations_num: Int?, name: String?, organisation: [OrganisationModel]?) {
        
        self.name = name
        self.redirectUrl = redirectUrl
        self.jitsiUrl = jitsiUrl
        self.collaborationUrl = collaborationUrl
        self.organizations_num = organizations_num ?? 0
        self.organisation = organisation ?? [OrganisationModel]()
    }
    
    init(json:JSON) {
        
        self.name = json["name"].string
        self.redirectUrl = json["redirectUrl"].string
        self.jitsiUrl = json["jitsiUrl"].string
        self.collaborationUrl = json["collaborationUrl"].string
        self.organizations_num = json["organizations_num"].intValue
        
        if let arrOrg = json["organization"].array {
            self.organisation = arrOrg.map({OrganisationModel(jsonServer: $0)});
        }
    }
    
    init(dic: [String: String]) {
        self.name = dic["name"]
        self.redirectUrl = dic["workspace"]
        self.jitsiUrl = dic["jitsiUrl"]
        self.collaborationUrl = dic["collaborationJITSIUrl"]
        self.organizations_num = 0
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        
        let name = aDecoder.decodeObject(forKey: "name") as? String
        let redirectUrl = aDecoder.decodeObject(forKey: "redirectUrl") as? String
        let jitsiUrl = aDecoder.decodeObject(forKey: "jitsiUrl") as? String
        let collaborationUrl = aDecoder.decodeObject(forKey: "collaborationUrl") as? String
        let organizations_num = aDecoder.decodeObject(forKey: "organizations_num") as? Int
        let organisation = aDecoder.decodeObject(forKey: "organisation") as? [OrganisationModel]
        
        self.init(redirectUrl: redirectUrl, jitsiUrl: jitsiUrl, collaborationUrl: collaborationUrl, organizations_num: organizations_num, name: name, organisation: organisation)
        
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(name, forKey: "name")
        aCoder.encode(redirectUrl, forKey: "redirectUrl")
        aCoder.encode(jitsiUrl, forKey: "jitsiUrl")
        aCoder.encode(collaborationUrl, forKey: "collaborationUrl")
        aCoder.encode(organizations_num, forKey: "organizations_num")
        aCoder.encode(organisation, forKey: "organisation")
        
    }
    
}
