//
//  WorkSpaceConstants.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//


import Foundation
import SwiftyJSON
import OAuthSwift



var HTTPS = "https://"

var MainServer = ApplicationManager.shared.domainName; ///"dev.meeraspace.com";
var internalWebSiteUrl = "https://internal.meeraspace.com/"

var Rest_BasicURL = "https://api.\(MainServer)"

var baseUrl = HTTPS + MainServer + "/graphql"

var REST_API_URL = Rest_BasicURL + "/chat/api"

var REST_API_URL_WS_Meeting = Rest_BasicURL + "/ws-meeting/api"

var imgbaseUrl = Rest_BasicURL + "/fm/download/"

let userPlaceHolderImage = UIImage(named: "user_placeholder")!

