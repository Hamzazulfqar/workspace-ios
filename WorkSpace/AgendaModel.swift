//
//  AgendaModel.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation
import SwiftyJSON

class AgendaModel {
    var id = ""
    var title = ""
    var description = ""
    var location = ""
    var startDate: Date?
    var endDate: Date?
    var users = [BasicUserModel]()
    //var ksl
    
    init() {}
    init(json: JSON) {
        id = json["id"].stringValue
        title = json["title"].stringValue
        description = json["description"].stringValue
        location = json["location"].stringValue
        startDate = json["startDate"].stringValue.toDate
        endDate = json["endDate"].stringValue.toDate
        
        users = json["agendaUsers"]["agendaUsers"].arrayValue.map({ (data) -> BasicUserModel in
            let user = BasicUserModel()
            user.id = data["user"]["id"].stringValue
            user.name = data["user"]["profile"]["fullName"].stringValue
            user.photo = data["user"]["profile"]["photo"].stringValue
            return user
        })
        
        
        
    }
    
    
}


class BasicUserModel {
    var id = ""
    var name = ""
    var photo = ""
    
    init() {}
}


extension Array where Element: AgendaModel {
    func agendas(on date: Date) -> [AgendaModel] {
        
        var agendas = [AgendaModel]()
        
        let minDate = (date.stringValue(format: "yyyy-MM-dd", timeZone: .current) + "T00:00:00").toDate!
        let maxDate = (date.stringValue(format: "yyyy-MM-dd", timeZone: .current) + "T23:59:59").toDate!
        
        self.forEach { (agenda) in
            if let startDate = agenda.startDate, let endDate = agenda.endDate {
                if startDate > minDate && endDate < maxDate {
                    agendas.append(agenda)
                }
            }
        }
        
        return agendas
    }
}
