//
//  WSCategoryModel.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation
import SwiftyJSON


class WSCategoryModel: NSObject, NSCoding {

    var id:String!
    var name:String!
    lazy var color:UIColor! = {
        if AppUtility.hasValidText(self.colorHex)
        {
            return UIColor.init(hexString: self.colorHex!);
        }
        
        return nil
    }();
    
    private var colorHex:String?
    
    
    override init() {
        super.init()
    }
    
    init(json:JSON) {
        self.id = json["id"].string
        self.name = json["name"].string
        self.colorHex = json["color"].string
    }
    
    init(name:String) {
        self.name = name
        self.id = "0"
    }
    
    
    init(id:String?, name:String?, colorHex:String?) {
        self.id = id
        self.name = name
        self.colorHex = colorHex
    }
    
    
//  static func == (lhs:WSCategoryModel, rhs:WSCategoryModel) -> Bool
//    {
//       return lhs.name == rhs.name
//    }
    required convenience init(coder aDecoder: NSCoder) {
        let id = aDecoder.decodeObject(forKey: "id") as? String
        let name = aDecoder.decodeObject(forKey: "name") as? String
        let colorHex = aDecoder.decodeObject(forKey: "colorHex") as? String
        
        self.init(id:id, name:name, colorHex:colorHex)
        
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(colorHex, forKey: "colorHex")
    }
    
    
    override func isEqual(_ object: Any?) -> Bool {
           if let object = object as? WSCategoryModel {
               return self.name == object.name
           }
           return false
       }
}
