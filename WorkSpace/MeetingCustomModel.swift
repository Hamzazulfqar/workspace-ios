//
//  MeetingCustomModel.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation
import SwiftyJSON

class WorkSpaceSummaryModel: NSObject, NSCoding {

    var id: String!
    var name:String!
    var wsDescription: String!
    var startDate: String!
    var endDate: String!
    var deletedDate: String!
    var coverImage = ItemImageModel();
    var ischecked = false
    var wsCategory:WSCategoryModel!
    var createdBy:UserModel!
    var wsUsers:[UserModel] = [UserModel]()
    var organizationID:String!
    var attachmentURL:URL?
    {
        if AppUtility.hasValidText(coverImage.attachment)
        {
           //  return (coverImage.attachment! + "?access_token=\(UserInfo.userAccessToken.accessToken)").imgURL
            return coverImage.attachment!.imgURL
        }
        
        return nil
    }
    
    override init() {
        
    }
    
    init(workspace: WorkspaceModel) {
        self.id = workspace.id
        self.name = workspace.name
        self.wsDescription = workspace.wsDescription
        self.startDate = workspace.startDate
        self.endDate = workspace.endDate
        self.deletedDate = workspace.deletedDate
        self.coverImage = workspace.coverImage
        self.wsCategory = workspace.wsCategory
        self.createdBy = workspace.createdBy
        self.organizationID = workspace.organizationID
        
    }
    
    
    init(json:JSON) {
        
        id = json["id"].string;
        name = json["name"].string;
        wsDescription = json["description"].string;
        organizationID = json["organizationID"].string
        wsCategory = WSCategoryModel(json: json["category"])
        
        startDate = json["startDate"].string;
        endDate = json["endDate"].string;
        deletedDate = json["deletedDate"].string;
        
        createdBy =   UserModel(json: json["createdBy"])
        if let wsUsers = json["wsUsers"]["wsUsers"].array
        {
            self.wsUsers = wsUsers.map({ (json) -> UserModel in
                UserModel(json: json["user"])
            })
        }
        
        if wsUsers.contains(createdBy) == false
        {
            wsUsers.append(createdBy)
        }
        
        
        coverImage = ItemImageModel(path: json["coverURL"].string);
        
    }
    
    
    init(id:String?, name:String?, wsDescription:String?, startDate:String?,endDate:String?, deletedDate:String?, coverImage:ItemImageModel?, wsCategory:WSCategoryModel?,createdBy:UserModel?, wsUsers:[UserModel]?, organizationID:String?  ) {
        self.id = id
        self.name = name
        self.wsDescription = wsDescription
        self.startDate = startDate
        self.endDate = endDate
        self.deletedDate = deletedDate
        self.coverImage = coverImage ?? ItemImageModel()
        self.wsCategory = wsCategory
        self.createdBy = createdBy
        self.wsUsers = wsUsers ?? [UserModel]()
        self.organizationID = organizationID
    }
    
    
    required convenience init(coder aDecoder: NSCoder) {
        let id = aDecoder.decodeObject(forKey: "id") as? String
        let name = aDecoder.decodeObject(forKey: "name") as? String
        let wsDescription = aDecoder.decodeObject(forKey: "wsDescription") as? String
        let startDate = aDecoder.decodeObject(forKey: "startDate") as? String
        let endDate = aDecoder.decodeObject(forKey: "endDate") as? String
        let deletedDate = aDecoder.decodeObject(forKey: "deletedDate") as? String
        let coverImage = aDecoder.decodeObject(forKey: "coverImage") as? ItemImageModel
        let wsCategory = aDecoder.decodeObject(forKey: "wsCategory") as? WSCategoryModel
        let createdBy = aDecoder.decodeObject(forKey: "createdBy") as? UserModel
        let wsUsers = aDecoder.decodeObject(forKey: "wsUsers") as? [UserModel]
        let organizationID = aDecoder.decodeObject(forKey: "organizationID") as? String
        
        self.init(
            id:id,
            name:name,
            wsDescription:wsDescription,
            startDate:startDate,
            endDate:endDate,
            deletedDate:deletedDate,
            coverImage:coverImage,
            wsCategory: wsCategory,
            createdBy:createdBy,
            wsUsers:wsUsers,
            organizationID:organizationID
        )
        
        
        
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(wsDescription, forKey: "wsDescription")
        aCoder.encode(startDate, forKey: "startDate")
        aCoder.encode(endDate, forKey: "endDate")
        aCoder.encode(deletedDate, forKey: "deletedDate")
        aCoder.encode(coverImage, forKey: "coverImage")
        aCoder.encode(wsCategory, forKey: "wsCategory")
        aCoder.encode(createdBy, forKey: "createdBy")
        aCoder.encode(wsUsers, forKey: "wsUsers")
        aCoder.encode(organizationID, forKey: "organizationID")
    }
    
    
//    static func == (lhs:WorkSpaceSummaryModel, rhs:WorkSpaceSummaryModel) -> Bool
//    {
//        return lhs.id == rhs.id
//    }
    
    override func isEqual(_ object: Any?) -> Bool {
              if let object = object as? WorkSpaceSummaryModel {
                  return self.id == object.id
              }
              return false
          }
    
    deinit {
        print("deinit WorkSpaceSummaryModel")
    }
}


class NotificationModel: NSObject, Codable {
    
    enum Module: String {
        case message = "message"
        case taskManagement = "taskManagement"
        case meeting = "meeting"
        case workspace = "workspace"
        case okr = "okr"
        case collaboration = "collaboration"
        case unknown = "unknown"
    }
    
    struct WSMessage: Codable {
        let workspaceId: String
        let totalMessages: Int
        
        init(json: JSON) {
            self.workspaceId = json["workspaceId"].stringValue
            self.totalMessages = json["totalMessages"].intValue
        }
        
        static func decode(json: Data) -> WSMessage? {
            do {
                return try JSONDecoder().decode(WSMessage.self, from: json)
            } catch let error {
                print("WSMessage: Decoding failed. Error: \(error)")
            }
            return nil
        }
    }
    
    struct Message: Codable {
        let channelId: String
        let type: String
        let unreadMessages: Int
        
        init(json: JSON) {
            self.channelId = json["channelId"].stringValue
            self.type = json["type"].stringValue
            self.unreadMessages = json["unreadMessages"].intValue
        }
        
        static func decode(json: Data) -> Message? {
            do {
                return try JSONDecoder().decode(Message.self, from: json)
            } catch let error {
                print("Message: Decoding failed. Error: \(error)")
            }
            return nil
        }
        
        
    }
    
    struct NotificationData: Codable {
        let moduleName: String
        let orgId: String?
        let wsMessages: [WSMessage]?
        let messages: [Message]?
        let totalMessages: Int?
        let workspaceId: String?
        let taskId: String?
        let taskID: String?
        let url: String?
        let workspaceName: String?
        let category: String?
        let subModuleName: String?
        let startDate: String?
        let action: String?
        var response: String?
        let invitationId: String?
        
        var module: Module {
            return Module(rawValue: moduleName) ?? .unknown
        }
        
        static func decode(json: Data) -> NotificationData? {
            do {
                return try JSONDecoder().decode(NotificationData.self, from: json)
            } catch let error {
                print("NotificationData: Decoding failed. Error: \(error)")
            }
            return nil
        }
        
        init(json: JSON) {
            moduleName = json["moduleName"].stringValue
            orgId = json["orgId"].string
            totalMessages = json["totalMessages"].int
            workspaceId = json["workspaceId"].string
            taskId = json["taskId"].string
            taskID = json["taskID"].string
            url = json["url"].string
            workspaceName = json["workspaceName"].string
            category = json["category"].string
            subModuleName = json["subModuleName"].string
            startDate = json["startDate"].string
            action = json["action"].string
            response = json["response"].string
            invitationId = json["invitationId"].string
            
            if let wsmessagesArr = json["wsMessages"].array {
                self.wsMessages = wsmessagesArr.map { WSMessage(json: $0)}
            } else {
                wsMessages = nil
            }
            
            if let messagesArr = json["messages"].array {
                self.messages = messagesArr.map { Message(json: $0)}
            } else {
                self.messages = nil
            }
        }
    }
    
    struct Sender: Codable {
        let email: String
        let valid: Bool
        let name: String
        let sub: String
        
        init(json: JSON) {
            email = json["email"].stringValue
            valid = json["valid"].boolValue
            name = json["name"].stringValue
            sub = json["sub"].stringValue
        }
    }
    
    enum Product: String {
        case task = "tasks"
        case cadre = "CADRE"
        case unknown = "unknown"
    }
    
    var id: String!
    var createdAt: String?
    var productName: String?
    var title: String!
    var body: String!
    var viewed: Bool!
    
    var product: Product {
        return Product(rawValue: productName ?? "unknown") ?? .unknown
    }
    
    var data: NotificationData
    let from: Sender?

    private let createdDateDouble: Double?

    var createdAtTimeStamp: Double {
        
        if let dateTime = createdAt {
            let dateTime1 = Date.convertDateTimeOnlyIntoUTCTimeStr(dateStr: dateTime);
            return dateTime1.millisecondsSince1970;
        }
        else if let dateTime = createdDateDouble {
            return dateTime;
            
        } else {
            let dateTime1 = Date.convertDateTimeOnlyIntoUTCTimeStr(dateStr: "1985-12-16T05:03:19.402Z");
            return dateTime1.millisecondsSince1970;
        }
    }
    
    static func decode(json: Data) -> NotificationModel? {
        do {
            return try JSONDecoder().decode(NotificationModel.self, from: json)
        } catch let error {
            print("NotificationModel: Decoding failed. Error: \(error)")
        }
        return nil
    }
    
   /* init(id: String,productName:String) {
        
        self.id = id;
        self.productName = productName;
    }*/
    
    init(json:JSON) {
        
        self.id = json["id"].string;
        self.createdAt = json["createdAt"].string
        self.createdDateDouble = json["createdAt"].double

        self.productName = json["productName"].string;
        self.title = json["title"].string;
        self.body = json["body"].string;
        self.viewed = json["viewed"].bool;
        
        self.data = NotificationData(json: json["data"])
        
        if let _ = json["from"]["sub"].string {
            self.from = Sender(json: json["from"])
        } else {
            self.from = nil
        }
        
    }
    
    lazy var NoteTime: String = {
        
        //let unixTimestamp = 1480134638.0
        let date = self.getDateFromTimeStamp(timeStamp: createdAtTimeStamp,dtFormatter: "HH:mm");
        return date
    }();
    
    var bgColor: UIColor!
    {
        if (viewed)
        {
            return UIColor.white
        }
        else
        {
            return UIColor.colorWithRGB(254, 255, 236, opacity: 1.0);
        }
    }
    
    var isShowDote: Bool!
    {
        if (viewed)
        {
            return true
        }
        else
        {
            return false;
        }
    }
    
    lazy var NoteTimeWS: String = {
        
        //let unixTimestamp = 1480134638.0
        let date = self.getDateFromTimeStamp(timeStamp: createdAtTimeStamp,dtFormatter: "hh:mm a");
        return date
    }();
    
    
    lazy var NoteDate: String = {
        
        let date = self.getDateFromTimeStamp(timeStamp: createdAtTimeStamp,dtFormatter: "dd-MM-yyyy");
        return date
    }();
    
    lazy var NoteDateMonthName: String = {
        
        let date = self.getDateFromTimeStamp(timeStamp: createdAtTimeStamp,dtFormatter: "dd-MMM-yyyy");
        return date
    }();
    
    lazy var NoteDateMonthNameWS: String = {
        
        let date = self.getDateFromTimeStamp(timeStamp: createdAtTimeStamp,dtFormatter: "MMM dd, yyyy");
        return date
    }();
    
    lazy var NoteDateDayName: String = {
        
        let date = self.getDateFromTimeStamp(timeStamp: createdAtTimeStamp,dtFormatter: "EEEE");
        return date
    }();
    
    var elapsedTimeWS: String! {
        
        return NoteTime;
    }
    
    var elapsedTime: String! {
        
        let timeAgo = self.TimesAgo(timeStamp: createdAtTimeStamp);
        
        let todayDate = Date.dateToString(date: Date());
        
        if (todayDate == NoteDate)
        {
            return timeAgo;
        }
        
        return NoteTime;
    }
    
    
    func getDateFromTimeStamp(timeStamp : Double?, dtFormatter: String!) -> String {
        if(timeStamp == nil){
            return "";
        }
        let date = Date(timeIntervalSince1970: timeStamp!/1000.0);
        let dayTimePeriodFormatter = AppUtility.dateFormatter;
        dayTimePeriodFormatter.dateFormat = dtFormatter;
        let dateString = dayTimePeriodFormatter.string(from: date)
        return dateString
    }
    
    func TimesAgo(timeStamp : Double?) -> String {
        if(timeStamp == nil){
            return "";
        }
        let date = Date(timeIntervalSince1970: timeStamp!/1000.0)
        let timgeAgo = date.getElapsedInterval();
        return timgeAgo;
    }
    
//    deinit {
//        print("deinit NotificationModel")
//    }
    
}
