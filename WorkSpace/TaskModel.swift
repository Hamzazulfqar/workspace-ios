//
//  TaskModel.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation
import SwiftyJSON


class UserModel:NSObject, NSCoding
{
    var id:String!
    var userID:String!
    var fullName:String!
    var pictureUrl:String!
    var title:String!
    var email:String!
    var subject:String!
    var status:String!
    var onCall:Bool = false
    
    
    override init() {
        super.init()
    }
    
    init(json:JSON) {
        self.id = json["id"].string
        self.subject = json["profile"]["subject"].string
        self.userID = json["profile"]["userID"].string
        self.fullName = json["profile"]["fullName"].string
        self.pictureUrl = (json["profile"]["pictureURL"].string)?.replacingOccurrences(of: "/download/", with: "")
        
        self.title = json["profile"]["title"].string
        self.email = json["profile"]["email"].string
    }
    
    init(profileJson:JSON) {
        self.userID = profileJson["userID"].string
        self.subject = profileJson["subject"].string
        self.fullName = profileJson["fullName"].string
        self.pictureUrl = profileJson["photo"]["aPIID"].string
        self.title = profileJson["title"].string
        self.email = profileJson["email"].string
    }
    
    init(id:String?, userID:String?, fullName:String?, pictureUrl:String?, title:String?, email:String?, subject:String?, status:String? = "ONLINE") {
        self.id = id
        self.userID = userID
        self.fullName = fullName
        self.pictureUrl = pictureUrl
        self.title = title
        self.email = email
        self.subject = subject
        self.status = status
    }
    
    //MARK: TODO
    /*
    
    init(channelUser: CollabortionChatModelController.ChatUser) {
        self.subject = channelUser.sub
        self.email = channelUser.email
        self.fullName = channelUser.name
    }
    
    init(member:MsgMember) {
        
        self.subject = member.subject
        self.email = member.email
        self.fullName = member.name
    }
    
    init(mentionedUser:MentionedUser) {
        self.subject = mentionedUser.sub
        self.fullName = mentionedUser.name
        self.pictureUrl = mentionedUser.avatar
        self.email = mentionedUser.email
        self.title = mentionedUser.title
    }
    
    */
    
    required convenience init(coder aDecoder: NSCoder) {
        let id = aDecoder.decodeObject(forKey: "id") as? String
        let userID = aDecoder.decodeObject(forKey: "userID") as? String
        let fullName = aDecoder.decodeObject(forKey: "fullName") as? String
        let pictureUrl = aDecoder.decodeObject(forKey: "pictureUrl") as? String
        let title = aDecoder.decodeObject(forKey: "title") as? String
        let email = aDecoder.decodeObject(forKey: "email") as? String
        let subject = aDecoder.decodeObject(forKey: "subject") as? String
        let status = aDecoder.decodeObject(forKey: "status") as? String
        
        
        self.init(
            id:id,
            userID:userID,
            fullName:fullName,
            pictureUrl:pictureUrl,
            title:title,
            email:email,
            subject:subject,
            status:status
        )
        
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(userID, forKey: "userID")
        aCoder.encode(fullName, forKey: "fullName")
        aCoder.encode(pictureUrl, forKey: "pictureUrl")
        aCoder.encode(title, forKey: "title")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(subject, forKey: "subject")
        aCoder.encode(status, forKey: "status")
    }
    
    
    func getdictionayFromModel() -> [String:Any]
    {
        var dic = [String:Any]()
        dic["email"] = self.email
        dic["name"] = self.fullName
        dic["sub"] = self.subject
        return dic
    }
    
//    static func == (lhs:UserModel, rhs:UserModel) -> Bool
//    {
//        return lhs.subject == rhs.subject
//
//    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? UserModel {
            return self.subject == object.subject
        }
        return false
    }
    
//    deinit {
//        print("deinit UserModel")
//    }
}
