//
//  CardView.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation
import UIKit


@IBDesignable


class CardView: UIView {
    
    private var corner: CGFloat = 8
    private var pShaddowOpacity: Float = 0.3
    private var borderWdth: CGFloat = 0
    private var borderClr: UIColor? = UIColor.clear
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        makeCard()
    }

    @IBInspectable var cornerRadius: CGFloat {
        get {
            return corner
        }
        set {
            corner = newValue
            makeCard()
            self.layoutIfNeeded()
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return borderWdth
        }
        set {
            borderWdth = newValue
            makeCard()
            self.layoutIfNeeded()
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return borderClr
        }
        set {
            borderClr = newValue
            makeCard()
            self.layoutIfNeeded()
        }
    }
    
    @IBInspectable var shaddowOpacity: Float {
        get {
            return pShaddowOpacity
        }
        set {
            pShaddowOpacity = newValue
            makeCard()
            self.layoutIfNeeded()
        }
    }
    
    
    func makeCard() {
        
        self.layer.cornerRadius = corner
        self.layer.borderColor = borderClr?.cgColor
        self.layer.borderWidth = borderWdth
        self.layer.shadowColor = UIColor(red: 140 / 255, green: 140 / 255, blue: 150 / 255, alpha: 1).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = pShaddowOpacity
        
        DispatchQueue.main.async {
            self.layer.cornerRadius = self.corner
            self.layer.borderColor = self.borderClr?.cgColor
            self.layer.borderWidth = self.borderWdth
            self.layer.shadowColor = UIColor(red: 140 / 255, green: 140 / 255, blue: 150 / 255, alpha: 1).cgColor
            self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            self.layer.shadowRadius = 5
            self.layer.shadowOpacity = self.pShaddowOpacity
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                self.layer.cornerRadius = self.corner
                self.layer.borderColor = self.borderClr?.cgColor
                self.layer.borderWidth = self.borderWdth
                self.layer.shadowColor = UIColor(red: 140 / 255, green: 140 / 255, blue: 150 / 255, alpha: 1).cgColor
                self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                self.layer.shadowRadius = 5
                self.layer.shadowOpacity = self.pShaddowOpacity
            })
        }
    }
}
