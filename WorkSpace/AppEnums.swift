//
//  AppEnums.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation
import UIKit

public enum AppTheme:Int
{
    case dark = 0, light = 1
}


enum CollaborationListEnum:Int {
    case Member = 0, Group = 1, Broadcast = 2
}

extension AppTheme
{
    var currentThemeTabIcons:BottomTabImages
    {
        switch self {
        case .dark:
            return BottomTabImages(
                tab1: TabImage(
                    selectedImg: "collaboration_enable_dark_theme",
                    deSelectedImg: "collaboration_disabled_dark_theme",
                    selectedTxtColor: self.tabItemSelectTitleColor,
                    deSelectedTxtColor: self.tabItemDeSelectTitleColor) ,
                /*TabImage(
                 selectedImg: "workspaces_enable_dark_theme",
                 deSelectedImg: "workspaces_disabled_dark_theme",
                 selectedTxtColor:  UIColor(hexString: "76FBD7"),
                 deSelectedTxtColor:self.tabItemDeSelectTitleColor),*/
                
                tab2: TabImage(
                    selectedImg: "workspace_enable_dark",
                    deSelectedImg: "workspace_disabled_dark",
                    selectedTxtColor: self.tabItemSelectTitleColor,
                    deSelectedTxtColor: self.tabItemDeSelectTitleColor),
                
                tab3: TabImage(
                    selectedImg: "meetings_enable_dark_theme",
                    deSelectedImg: "meetings_disabled_dark_theme",
                    selectedTxtColor: self.tabItemSelectTitleColor,
                    deSelectedTxtColor: self.tabItemDeSelectTitleColor),
                
                tab4: TabImage(
                    selectedImg: "planner_enable_dark",
                    deSelectedImg: "planner_disabled_dark",
                    selectedTxtColor: self.tabItemSelectTitleColor,
                    deSelectedTxtColor: self.tabItemDeSelectTitleColor),
                
                
                tab5: TabImage(
                    selectedImg: "apps_dark_enable",
                    deSelectedImg: "apps_dark_disabled",
                    selectedTxtColor: self.tabItemSelectTitleColor,
                    deSelectedTxtColor: self.tabItemDeSelectTitleColor)
            )
        case .light:
            return BottomTabImages(
                tab1:TabImage(
                    selectedImg: "collaboration_enable",
                    deSelectedImg: "collaboration_disabled",
                    selectedTxtColor: self.tabItemSelectTitleColor,
                    deSelectedTxtColor: self.tabItemDeSelectTitleColor) ,
                /*TabImage(
                 selectedImg: "workspaces_enable",
                 deSelectedImg: "workspaces_disabled",
                 selectedTxtColor: self.tabItemSelectTitleColor,
                 deSelectedTxtColor: self.tabItemDeSelectTitleColor),*/
                
                tab2: TabImage(
                    selectedImg: "workspace_enable_lite",
                    deSelectedImg: "workspace_disabled_lite",
                    selectedTxtColor: self.tabItemSelectTitleColor,
                    deSelectedTxtColor: self.tabItemDeSelectTitleColor),
                
                tab3: TabImage(
                    selectedImg: "meetings_enable",
                    deSelectedImg: "meeting_disabled",
                    selectedTxtColor: self.tabItemSelectTitleColor,
                    deSelectedTxtColor: self.tabItemDeSelectTitleColor),
                
                tab4: TabImage(
                    selectedImg: "planner_enable_lite",
                    deSelectedImg: "planner_disabled_lite",
                    selectedTxtColor: self.tabItemSelectTitleColor,
                    deSelectedTxtColor: self.tabItemDeSelectTitleColor),
                
                
                tab5: TabImage(
                    selectedImg: "apps_enable",
                    deSelectedImg: "apps_disabled",
                    selectedTxtColor: self.tabItemSelectTitleColor,
                    deSelectedTxtColor: self.tabItemDeSelectTitleColor)
            )
            
        }
    }
    
    
    var tabBackgrounColor:UIColor
    {
        switch self {
        case .dark:
            return UIColor.black
        case .light:
            return UIColor.white
        }
    }
    
    var tabItemSelectTitleColor:UIColor
    {
        switch self {
        case .dark:
            return UIColor(hexString: "ffaa00")
        case .light:
            return UIColor(hexString: "2f346c")
        }
    }
    
    var tabItemDeSelectTitleColor:UIColor
    {
        switch self {
        case .dark:
            return AppColor.darkThemeTitleGrey
        case .light:
            return UIColor(hexString: "4F5B6A")
        }
    }
    
    public static var currentTheme:AppTheme
    {
        get{
            if (UserDefaults.standard.object(forKey: "AppTheme") != nil)
            {
                return AppTheme(rawValue: UserDefaults.standard.object(forKey: "AppTheme") as! NSInteger)!
            }
            else
            {
                return AppTheme.light
            }
        }
        set{
            UserDefaults.standard.set(newValue.rawValue, forKey: "AppTheme")
            UserDefaults.standard.synchronize()
            ApplicationManager.shared.appTheme = newValue
            AppObservers.appTheme.accept(newValue)
        }
    }
}


struct BottomTabImages {
    var tab1:TabImage
    var tab2:TabImage
    var tab3:TabImage
    var tab4:TabImage
    var tab5:TabImage
}

enum ViewAnimationType: Int {
    case shake = 0
    case pulse = 1
    case pulseHorizontally = 2
    case pulseVertically = 3
    case like = 4
}
