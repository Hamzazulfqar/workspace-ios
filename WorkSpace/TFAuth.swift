//
//  TFAuth.swift
//  BloomingBuyer
//
//  Created by Waqas Rasheed on 20/03/2021.
//

import OAuthSwift

struct TFAuth {
    /*
    static var oauthswift = OAuth2Swift(
        consumerKey: "mobile-app", //meera-dx "mobile-app"        // [1] Enter google app settings
        consumerSecret:"ZXhhbXBsZS1hcHAtc2VjcmV1",        // No secret required
        authorizeUrl: Config.ssoURL + "/auth",
        accessTokenUrl: Config.ssoURL + "/token",
        responseType: "code"
    )
    
    static func resetSSOurl() {
        oauthswift = OAuth2Swift(
            consumerKey: "mobile-app",
            consumerSecret:"ZXhhbXBsZS1hcHAtc2VjcmV1",
            authorizeUrl: Config.ssoURL + "/auth",
            accessTokenUrl: Config.ssoURL + "/token",
            responseType: "code"
        )
    }
    
    static func preparingOth2WithPassword(with userName:String, password: String, jsonHandler:@escaping(_ json: [String:Any]) -> Void, failure:@escaping(_ error: OAuthSwiftError)-> Void)
    {
        
        TFAuth.oauthswift.authorize(username: userName, password: password, scope: "openid email groups profile offline_access") { (result) in
            switch(result)
            {
            
            case .success(let (_,response ,_ )):
                if let jsonResponce = response?.data
                {
                    do {
                        let jsonResult = try JSONSerialization.jsonObject(with: jsonResponce, options: JSONSerialization.ReadingOptions.allowFragments)
                        
                        let parseJson = jsonResult as! [String:Any]
                        jsonHandler(parseJson)
                    }
                    catch {
                        let notValid = OAuthSwiftError.configurationError(message: "json not valid")
                        failure(notValid)
                    }
                }
                else{
                    let notValid = OAuthSwiftError.configurationError(message: "No user found")
                    failure(notValid)
                }
                
            case.failure(let error):
                
                let errorDic = error.errorUserInfo;
                
                if(self.outhErrorHandler(dic: errorDic).code == 401)
                {
                    let notValid = OAuthSwiftError.configurationError(message: "Invalid email or password.")
                    failure(notValid)
                }
                else if(self.outhErrorHandler(dic: errorDic).code == 400)
                {
                    failure(error)
                }
                else
                {
                    let meessages = self.outhErrorHandler(dic: errorDic)
                    let message = meessages.errorStr + "\(meessages.code)"
                    let notValid = OAuthSwiftError.configurationError(message: message)
                    failure(notValid)
                }
            }
        }
        
    }
    
    private static func outhErrorHandler(dic: [String:Any]?) -> (code:Int,errorStr:String)
    {
        if let _ = dic
        {
            if let m = dic!["error"] as? NSError
            {
                print(m.localizedDescription)
                print(m.code);
                return (code:m.code,errorStr:m.localizedDescription)
            }
        }
        
        return (code:0,errorStr:"Unknown error".localizeValue);
    }*/
}
