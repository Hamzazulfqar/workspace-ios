//
//  Alerts.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation
import UIKit
import TFNetwork
struct Alert
{
    
    private static func displayAlertActionSheet(withTitle title: String?, andMessage message: String!,_ delegate:UIViewController!, cancelActionHandler: ((UIAlertAction) -> Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: cancelActionHandler));
        DispatchQueue.main.async {
            delegate.present(alert, animated: true, completion: nil);
        }
    }
    
    private static func displayAlertActionSheet(withTitle title: String?, andMessage message: String!,successTitle:String, cancelTitle:String,_ delegate:UIViewController!,successActionHandler: ((UIAlertAction) -> Void)? = nil ,cancelActionHandler: ((UIAlertAction) -> Void)? = nil) {
           
           let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
           alert.addAction(UIAlertAction(title: successTitle, style: UIAlertAction.Style.default, handler: successActionHandler));
           alert.addAction(UIAlertAction(title: cancelTitle, style: UIAlertAction.Style.default, handler: cancelActionHandler));
           DispatchQueue.main.async {
               delegate.present(alert, animated: true, completion: nil);
           }
       }
    
    static func showServerErrors(controller:UIViewController, errorModel:TFNetwork.ErrorModel)
    {
        displayAlertActionSheet(withTitle: errorModel.errorTitle, andMessage: errorModel.errorDescp, controller)
    }
    
    static func showAlert(title:String?, descp:String?,controller:UIViewController)
    {
        displayAlertActionSheet(withTitle: title, andMessage: descp, controller)
    }
    
    static func showAlert(title:String?, descp:String?,controller:UIViewController, cancelHandler: @escaping ((UIAlertAction) -> Void))
    {
        displayAlertActionSheet(withTitle: title, andMessage: descp, controller, cancelActionHandler: cancelHandler)
    }
    
    static func showAlert(title:String?, descp:String?,controller:UIViewController,successTitle:String, cancelTitle:String,successHandler: @escaping ((UIAlertAction) -> Void) ,cancelHandler: @escaping ((UIAlertAction) -> Void))
    {
        displayAlertActionSheet(withTitle: title, andMessage: descp, successTitle: successTitle, cancelTitle: cancelTitle, controller,successActionHandler: successHandler,cancelActionHandler: cancelHandler)
    }
    
}
