//
//  WorkspaceCell.swift
//  Meera
//
//  Created by Zuhair Hussain on 18/06/2020.
//  Copyright © 2020 Muhammad Arslan Khalid. All rights reserved.
//

import UIKit
import SDWebImage

class WorkspaceCell: UICollectionViewCell {
    
    @IBOutlet weak var viewContainer: CardView!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgIconMembers: UIImageView!
    @IBOutlet weak var lblMembers: UILabel!
    @IBOutlet weak var imgIconCreator: UIImageView!
    @IBOutlet weak var lblCreator: UILabel!
    
    @IBOutlet var imgUsers: [UIImageView]!
    @IBOutlet weak var viewMoreUsers: UIView!
    @IBOutlet weak var lblMoreUsers: UILabel!
    
    @IBOutlet weak var cstViewContainerTrailing: NSLayoutConstraint!
    @IBOutlet weak var cstViewContainerLeading: NSLayoutConstraint!
    @IBOutlet weak var cstStackViewHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setTheme(_ theme: AppTheme) {
        if theme == .light {
            viewContainer.backgroundColor = UIColor.white
            lblName.textColor = UIColor.black
            lblMembers.textColor = AppColor.appGray
            lblCreator.textColor = AppColor.appGray
            imgUsers.forEach { $0.viewBorderColor = UIColor.white }
        } else {
            viewContainer.backgroundColor = UIColor.black
            lblName.textColor = UIColor.white
            lblMembers.textColor = UIColor.white
            lblCreator.textColor = UIColor.white
            imgUsers.forEach { $0.viewBorderColor = UIColor.black }
        }
    }
    
    func setData(_ data: WorkspaceModel) {
        // imgCover.setImage(url: data.coverImage.attachment?.imgURL, placeholder: "workspace_bg".bundleImage)
        if data.coverImage.attachment != nil {
            if data.coverImage.attachment.contains("http") {
                self.imgCover.sd_setImage(with: data.coverImage.attachment?.imgURL1, placeholderImage: AppImages.wsPlaceHolderGeneralImg, options: SDWebImageOptions.continueInBackground, completed: nil)
                
            } else {
                imgCover.setImage(url: data.coverImage.attachment?.imgURL, placeholder: "workspace_bg".bundleImage)
                
                //  self.imgCover.sd_setImage(with: data.coverImage.attachment?.imgURL, placeholderImage: AppImages.wsPlaceHolderGeneralImg, options: SDWebImageOptions.continueInBackground, completed: nil)
                
            }
        }
        imgCover.contentMode = .scaleToFill
        lblName.text = data.name
        lblMembers.text = "\(data.wsUsers.count) MEMBERS"
        lblCreator.text = data.createdBy?.fullName ?? "--"
        

        for (index, imageView) in imgUsers.enumerated() {
            let user = data.wsUsers.object(at: index)?.user
//            let user = data.wsUsers[index].user
            imageView.setImage(url: user?.pictureUrl.imgURL, placeholder: AppConstants.userPlaceholder)
            imageView.isHidden = user == nil
        }
        
//        for index in 0..<data.wsUsers.count{
//            let user = data.wsUsers[index].user
//            imgUsers[index].setImage(url: user?.pictureUrl.imgURL, placeholder: AppConstants.userPlaceholder)
//            imgUsers[index].isHidden = false
//        }
        
        
        
        
        
        let remaingUsers = data.wsUsers.count - imgUsers.count
        lblMoreUsers.text = "+\(remaingUsers)"
        viewMoreUsers.isHidden = remaingUsers <= 0
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            cstStackViewHeight.constant = 26
            imgUsers.forEach { $0.viewCornerRadius = 13 }
            viewMoreUsers.viewCornerRadius = 13
        }
        
    }

    
}
