//
//  UIElements.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation
import UIKit
import SDWebImage

extension UIApplication {
    
    var localizedAppName: String! {
        
        var appName = ""
        // Non-Localized
        if let displayName = Bundle.main.infoDictionary?["CFBundleDisplayName"] as? String {
            
            appName = displayName
            // Localized
            if let displayName = Bundle.main.localizedInfoDictionary?["CFBundleDisplayName"] as? String {
                
                appName = displayName
            }
        }
        
        return appName
    }
    
    static func isMeeraOIA() -> Bool {
        #if MOIA
        return true
        #else
        return false
        #endif
    }
}

extension UIScreen {
    static var width: CGFloat {
        return UIScreen.main.bounds.width
    }
    static var height: CGFloat {
        return UIScreen.main.bounds.height
    }
}



extension UIView {
    
    private struct AssociatedKeys {
        static var shaddow = "shaddowKey"
    }
    
    func layoutIfNeeded(animated: Bool) {
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
        }
    }
    
    //    func addRefreshControl(_ target: Any, selector: Selector) {
    //        let refreshControl = UIRefreshControl()
    //        refreshControl.addTarget(target, action: selector, for: .valueChanged)
    //        refreshControl.backgroundColor = UIColor.clear
    //        self.refreshControl = refreshControl
    //    }
    func addTapGesture(_ target: Any, action: Selector) {
        let gesture = UITapGestureRecognizer(target: target, action: action)
        self.addGestureRecognizer(gesture)
    }
    
    var viewController: UIViewController? {
        
        var nextResponder: UIResponder? = self
        repeat {
            nextResponder = nextResponder?.next
            if let viewController = nextResponder as? UIViewController {
                return viewController
            }
        } while nextResponder != nil
        
        return nil
    }
    
    @IBInspectable var showShaddow : Bool {
        get {
            guard let number = objc_getAssociatedObject(self, &AssociatedKeys.shaddow) as? NSNumber else {
                return true
            }
            return number.boolValue
        }
        
        set {
            
            if newValue {
                self.layer.masksToBounds = false
                self.layer.shadowColor = UIColor(red: 140 / 255, green: 140 / 255, blue: 150 / 255, alpha: 1).cgColor
                self.layer.shadowOpacity = 0.2
                self.layer.shadowOffset = CGSize(width: 0, height: 1)
                self.layer.shadowRadius = 5
                self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
                self.layer.shouldRasterize = true
                self.layer.rasterizationScale = UIScreen.main.scale
                
                DispatchQueue.main.async {
                    self.layer.masksToBounds = false
                    self.layer.shadowColor = UIColor(red: 140 / 255, green: 140 / 255, blue: 150 / 255, alpha: 1).cgColor
                    self.layer.shadowOpacity = 0.2
                    self.layer.shadowOffset = CGSize(width: 0, height: 1)
                    self.layer.shadowRadius = 5
                    self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
                    self.layer.shouldRasterize = true
                    self.layer.rasterizationScale = UIScreen.main.scale
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                        self.layer.masksToBounds = false
                        self.layer.shadowColor = UIColor(red: 140 / 255, green: 140 / 255, blue: 150 / 255, alpha: 1).cgColor
                        self.layer.shadowOpacity = 0.2
                        self.layer.shadowOffset = CGSize(width: 0, height: 1)
                        self.layer.shadowRadius = 5
                        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
                        self.layer.shouldRasterize = true
                        self.layer.rasterizationScale = UIScreen.main.scale
                    })
                }
            } else {
                DispatchQueue.main.async {
                    self.layer.masksToBounds = false
                    self.layer.shadowColor = nil
                    self.layer.shadowOpacity = 0
                    self.layer.shadowOffset = CGSize.zero
                    self.layer.shadowRadius = 0
                    self.layer.shadowPath = nil
                    self.layer.shouldRasterize = true
                    self.layer.rasterizationScale = UIScreen.main.scale
                }
            }
            
            objc_setAssociatedObject(self,&AssociatedKeys.shaddow,NSNumber(value: newValue),objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func hideAnimated() {
        if self.isHidden == false {
            let a = self.alpha
            UIView.animate(withDuration: 0.3, animations: {
                self.alpha = 0
            }) { (_) in
                self.isHidden = true
                self.alpha = a
            }
        }
    }
    func showAnimated() {
        if self.isHidden == true {
            let a = self.alpha
            self.alpha = 0
            self.isHidden = false
            UIView.animate(withDuration: 0.3, animations: {
                self.alpha = a
            })
        }
    }
    
    
    @IBInspectable var viewCornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    @IBInspectable var viewBorderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var viewBorderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    func animate(with animationType: ViewAnimationType, transform: CGFloat = 0.85) {
        switch animationType {
        case .shake:
            self.shake()
            break
        case .pulse:
            self.pulse()
            break
        case .pulseHorizontally:
            self.pulseHorizontally()
            break
        case .pulseVertically:
            self.pulseVertically()
        case .like:
            self.like()
            break
        }
    }
    
    
    private func shake() {
        self.transform = CGAffineTransform(translationX: 16, y: 0)
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    private func pulse(transform: CGFloat = 0.85) {
        self.alpha = 0.3
        self.layer.transform = CATransform3DMakeScale(transform, transform, transform)
        
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.alpha = 1
            self.layer.transform = CATransform3DScale(CATransform3DIdentity, 1, 1, 1)
        })
    }
    
    private func like(transform: CGFloat = 1.8) {
        self.alpha = 1
        
        UIView.animate(withDuration: 0.15, animations: {
            self.layer.transform = CATransform3DMakeScale(transform, transform, transform)
        }) { (_) in
            UIView.animate(withDuration: 0.2, animations: {
                self.alpha = 0.8
                self.layer.transform = CATransform3DMakeScale(0.8, 0.8, 0.8)
            }) { (_) in
                UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
                    self.alpha = 1
                    self.layer.transform = CATransform3DScale(CATransform3DIdentity, 1, 1, 1)
                })
            }
        }
        
    }
    private func pulseHorizontally(transform: CGFloat = 0.85) {
        self.alpha = 0.3
        self.layer.transform = CATransform3DMakeScale(transform, 1, transform)
        
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.alpha = 1
            self.layer.transform = CATransform3DScale(CATransform3DIdentity, 1, 1, 1)
        })
    }
    private func pulseVertically(transform: CGFloat = 0.85) {
        self.alpha = 0.3
        self.layer.transform = CATransform3DMakeScale(1, transform, transform)
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.alpha = 1
            self.layer.transform = CATransform3DScale(CATransform3DIdentity, 1, 1, 1)
        })
    }
    
}

extension UIImageView {
    override open func awakeFromNib() {
        super.awakeFromNib()
        tintColorDidChange()
    }
    
    func setImage(urlString: String, placeholder: UIImage? = nil) {
        if let u = URL(string: urlString) {
            self.sd_setImage(with: u, placeholderImage: placeholder)
        } else {
            self.image = placeholder
        }
    }
    func setImage(url: URL?, placeholder: UIImage? = nil, completion: ((_ image: UIImage?) -> Void)? = nil) {
        if let u = url {
            //            self.sd_setImage(with: u, placeholderImage: placeholder)
            
            self.sd_setImage(with: u, placeholderImage: placeholder) { (image, error, casheType, url) in
                completion?(image)
            }
        } else {
            self.image = placeholder
        }
    }
}

extension UICollectionView {
    func registerCell(ofType type: Any, withReuseIdentifier identifier: String) {
        let className = String(describing: type)
        let nib = UINib(nibName: className, bundle: Bundle.main)
        self.register(nib, forCellWithReuseIdentifier: identifier)
    }
    func registerHeader(ofType type: Any, withReuseIdentifier identifier: String) {
        let className = String(describing: type)
        self.register(UINib(nibName: className, bundle: Bundle.main), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: identifier)
    }
    func addRefreshControl(_ target: Any, selector: Selector) {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(target, action: selector, for: .valueChanged)
        refreshControl.backgroundColor = UIColor.clear
        self.refreshControl = refreshControl
    }
    
}

extension UICollectionViewCell {
    static var identifier: String {
        return "k\(String(describing: self))"
    }
}

extension UIColor {
    static var appGreen = UIColor(red: 104 / 255, green: 187 / 255, blue: 114 / 255, alpha: 1)
    static var appBlue = UIColor(red: 57 / 255, green: 119 / 255, blue: 246 / 255, alpha: 1)
    static var separatorGray = UIColor(red: 229 / 255, green: 229 / 255, blue: 229 / 255, alpha: 1)
    static var appBlackHeading = UIColor(named: "AppHeadingBlack")
    static let borderGray = UIColor(red: 229 / 255, green: 229 / 255, blue: 229 / 255, alpha: 1)
    static var placeholderGray = UIColor(red: 200 / 255, green: 200 / 255, blue: 200 / 255, alpha: 1)
    
    func isLight(threshold: Float = 0.5) -> Bool? {
        let originalCGColor = self.cgColor
        let RGBCGColor = originalCGColor.converted(to: CGColorSpaceCreateDeviceRGB(), intent: .defaultIntent, options: nil)
        guard let components = RGBCGColor?.components else { return nil }
        guard components.count >= 3 else { return nil }
        let brightness = Float(((components[0] * 299) + (components[1] * 587) + (components[2] * 114)) / 1000)
        return (brightness > threshold)
    }
    func lighter(by percentage: CGFloat = 30.0) -> UIColor? {
        return self.adjust(by: abs(percentage) )
    }
    
    func darker(by percentage: CGFloat = 30.0) -> UIColor? {
        return self.adjust(by: -1 * abs(percentage) )
    }
    
    func adjust(by percentage: CGFloat = 30.0) -> UIColor? {
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
        if self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
            return UIColor(red: min(red + percentage/100, 1.0),
                           green: min(green + percentage/100, 1.0),
                           blue: min(blue + percentage/100, 1.0),
                           alpha: alpha)
        } else {
            return nil
        }
    }
}
