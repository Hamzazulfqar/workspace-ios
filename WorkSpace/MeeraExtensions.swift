//
//  MeeraExtensions.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import UIKit
import Foundation
import SwiftyJSON

extension UIColor
{
    
    static func colorWithRGB(_ red:CGFloat,_ green:CGFloat, _ blue:CGFloat, opacity:CGFloat) -> UIColor
    {
        return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: opacity);
    }
    
    static func AppLightGreyColor() -> UIColor
    {
        return UIColor.colorWithRGB(231, 230, 231, opacity: 1.0)
    }
    
    static func AppDarkGreyColor() -> UIColor
    {
        return UIColor.colorWithRGB(139, 138, 139, opacity: 1.0)
    }
    
    static func AppTextLightGreyColor() -> UIColor
    {
        return UIColor.colorWithRGB(195, 194, 195, opacity: 1.0)
    }
    
    static var AppProfilePurlpleColor: UIColor {
        return UIColor(hexString: "2E3490")
    }
    
    static var AppProfileBlueColor:UIColor
    {
        return UIColor(hexString: "3063BA")
    }
    
    static var AppProfileGreyColor:UIColor
    {
        return UIColor(hexString: "B4B3B4")
    }
    
    static var AppTaskStreamGrey:UIColor {
        
        return UIColor(hexString: "757475")
    }
    
    static var JoinedColor:UIColor {
        
        return UIColor(hexString: "F6C344")
    }
    
    static var AcceptedColor:UIColor {
        
        return UIColor(hexString: "76B26D")
    }
    
    static var InvitedColor:UIColor {
        
        return UIColor(hexString: "3876F3")
    }
    
    static var CollapsedCellColor:UIColor {
        
        return UIColor(hexString: "777A7B")
    }
    
    static var DeclinedBg:UIColor {
        
        return UIColor(hexString: "FBEEEE")
    }
    
    static var DeclinedDarkBg:UIColor {
        
        return UIColor(hexString: "513636")
    }
    
    static var DeclineTextColor:UIColor {
        
        return UIColor(hexString: "E86762")
    }
    
    static var JoinedBg:UIColor {
        
        return UIColor(hexString: "EDF8EA")
    }
    
    static var JoineDarkBg:UIColor {
        
        return UIColor(hexString: "3E4B3F")
    }
    
    static var JoinedTextColor:UIColor {
        
        return UIColor(hexString: "64C447")
    }
    
    static var AppWSLightModeGrayColor:UIColor {
        
        return UIColor(hexString: "727677")
    }
    
    static var AppWSDarkModeGrayColor:UIColor {
        
        return UIColor(hexString: "8D9193")
    }
    
    static var AppWSLightModeBlackColor:UIColor {
        
        return UIColor(hexString: "171D24")
    }
    
    static var AppWSDarkModeWhiteColor:UIColor {
        
        return UIColor(hexString: "E5E6EC")
    }
    
    static var AppTaskStreamBlue:UIColor {
        
        return UIColor(hexString: "2E308D")
    }
    
    
    convenience init(hexString:String) {
        
        let hexString:NSString = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) as NSString
        
        // let hexString = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        //hexString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let scanner = Scanner(string: hexString as String)
        
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        
        var color:UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red:red, green:green, blue:blue, alpha:1)
    }
    
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return NSString(format:"#%06x", rgb) as String
    }
    
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    static var AppOrangeColor:UIColor {
        
        return UIColor(hexString: "FF5722")
    }
    
}




let whitespaceAndNewlineChars: [Character] = ["\n", "\r", "\t", " "]
extension String {
    
    var bundleImage: UIImage? {
        
        return UIImage(named:self)
    }
    
    var color: UIColor? {
        if self.contains("rgba(") {
            let components = self.replacingOccurrences(of: "rgba(", with: "").replacingOccurrences(of: ")", with: "").components(separatedBy: ",")
           
            let r = Float(components[0]) ?? 0.0
            let g = Float(components[1]) ?? 0.0
            let b = Float(components[2]) ?? 0.0
            let a = Float(components[3]) ?? 0.0
            
            
            
//            let r = Floa //Float(components.object(at: 0)?.trimmed ?? "0") ?? 0
//            let g = //Float(components.object(at: 1)?.trimmed ?? "0") ?? 0
//            let b = //Float(components.object(at: 2)?.trimmed ?? "0") ?? 0
//            let a = //Float(components.object(at: 3)?.trimmed ?? "0") ?? 0

            return UIColor(red: CGFloat(r ), green: CGFloat(g ), blue: CGFloat(b), alpha: CGFloat(a))
        }
        return UIColor(hexString: self)
    }
    
    var trimmed: String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        
        return String(data: data, encoding: .utf8)
    }

    var localizeValue:String
    {
        let val  = Bundle.main.localizedString(forKey: self, value: "", table: "Localizable")
        return val
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    var lastPathComponent: String {
        get {
            return (self as NSString).lastPathComponent
        }
    }
    
    var stringByDeletingLastPathComponent: String {
        get {
            return (self as NSString).deletingLastPathComponent
        }
    }
    var stringByDeletingPathExtension: String {
        get {
            return (self as NSString).deletingPathExtension
        }
    }
    var pathComponents: [String] {
        get {
            return (self as NSString).pathComponents
        }
    }
    func stringByAppendingPathComponent(path: String) -> String {
        let nsSt = self as NSString
        return nsSt.appendingPathComponent(path)
    }
    
    func stringByAppendingPathExtension(ext: String) -> String? {
        let nsSt = self as NSString
        return nsSt.appendingPathExtension(ext)
    }
    
    func rtrim() -> String {
        if isEmpty { return "" }
        var i = endIndex
        while i >= startIndex {
            i = index(before: i)
            let c = self[i]
            if whitespaceAndNewlineChars.firstIndex(of: c) == nil  {
                break
            }
        }
        return String(self[startIndex...i])
    }
    
    func ltrim() -> String {
        if isEmpty { return "" }
        var i = startIndex
        while i < endIndex {
            let c = self[i]
            
            
            //"".color
            if whitespaceAndNewlineChars.firstIndex(of: c) == nil {
                break
            }
            i = index(after: i)
        }
        return String(self[i..<endIndex])
    }
    
    func trim() -> String {
        return ltrim().rtrim()
    }
    
    var parseJSONString: JSON? {
        
        let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        if let jsonData = data {
            // Will return an object or nil if JSON decoding fails
            do
            {
                return  try JSON(data: jsonData)
            }
            catch
            {
                return nil
            }
            
        } else {
            // Lossless conversion of the string was not possible
            return nil
        }
    }
    
    var isImageFile:Bool
    {
        
        let fileExtension = NSString(string: self).pathExtension
        if fileExtension  == "jpeg" || fileExtension == "jpg" || fileExtension == "JPG" || fileExtension == "png" || fileExtension == "PNG"
            || fileExtension == "bmp"
        {
            return true
        }
        return false
    }
    
    
    var imgURL2:URL?
    {
        
        if  AppUtility.hasValidText(self)
        {
            let workaround = self.replacingOccurrences(of: "/download/", with: "")
            let urlStr = imgbaseUrl + workaround + "?access_token=\(UserInfo.userAccessToken.accessToken)"
            
            let url = URL(string: urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
            return url
        }
        return nil
        
        
    }
    var imgURL:URL?
    {
        if  AppUtility.hasValidText(self)
        {
            let workaround = self.replacingOccurrences(of: "/download", with: "")
            let urlStr = imgbaseUrl + workaround + "?access_token=\(UserInfo.userAccessToken.accessToken)"
            
            let url = URL(string: urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
            return url
        }
        return nil
        
        
    }
    
    
    func imgURL(baseURL: String) -> URL? {
        
        if  AppUtility.hasValidText(self) {
            
            let workaround = self.replacingOccurrences(of: "/download", with: "")
            
            let urlStr = baseURL + workaround + "?access_token=\(UserInfo.userAccessToken.accessToken)"
            let url = URL(string: urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
            return url
        }
        return nil
        
        
    }
    
    var imgURL1:URL?
    {
        
        if  AppUtility.hasValidText(self)
        {
            let workaround = self.replacingOccurrences(of: "/download", with: "")
            let urlStr = workaround + "?access_token=\(UserInfo.userAccessToken.accessToken)"
            
            let url = URL(string: urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
            return url
        }
        return nil
        
        
    }
    
    var imgIdImgURL:URL?
    {
        if  AppUtility.hasValidText(self)
        {
            let workaround = self.replacingOccurrences(of: "/download", with: "")
            
            let urlStr = workaround + "?access_token=\(UserInfo.userAccessToken.accessToken)"
            
            let url = URL(string: urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
            return url
        }
        return nil
    }
    
    var basicImgUrl:URL?
    {
        if  AppUtility.hasValidText(self)
        {
            
            
            let urlStr = self + "?access_token=\(UserInfo.userAccessToken.accessToken)"
            
            let url = URL(string: urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
            return url
        }
        return nil
    }
    
    func imageURL_MeeraFS_token(token : String, thumbnail: Bool = false) -> (urlString: String,url: URL?) {
        
        var path = self
        
        if self.starts(with: "/") {
            
            path.removeFirst()
        }
        let thumb = thumbnail == true ? "/thumbnail" : ""
        
        let baseUrlPath = Rest_BasicURL + "/meerastorage/files"
        var urlStr = baseUrlPath + thumb + "/\(path)"
        
        urlStr = urlStr + "?token=\(token)" + "&access_token=\(UserInfo.userAccessToken.accessToken)"
        
        let url = URL(string: urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
        
        return (urlStr,url)
    }
    
    
    
    func dateValue(format: String, timeZone: TimeZone? = TimeZone(abbreviation: "GMT")) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = timeZone
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter.date(from: self)
    }
    
    var toDate: Date? {
        var date: Date? = nil
        var dateString = self
        
        if dateString.contains("0000-01-01T") {
            dateString = dateString.replacingOccurrences(of: "0000-01-01T", with: Date().stringValue(format: "yyyy-MM-dd", timeZone: .current) + "T")
        }
        
        let formates = ["yyyy-MM-dd'T'HH:mm:ss.SSSXXX", "yyyy-MM-dd'T'HH:mm:ssZ", "yyyy-MM-dd'T'HH:mm:ss"]
        for f in formates {
            if let d = dateString.dateValue(format: f) {
                date = d
                break
            }
        }
        
        return date
    }
    
    
    func date(with timeZone: TimeZone? = TimeZone(abbreviation: "GMT")) -> Date? {
        var date: Date? = nil
        var dateString = self
        
        if dateString.contains("0000-01-01T") {
            dateString = dateString.replacingOccurrences(of: "0000-01-01T", with: Date().stringValue(format: "yyyy-MM-dd", timeZone: .current) + "T")
        }
        
        let formates = ["yyyy-MM-dd'T'HH:mm:ss.SSSXXX", "yyyy-MM-dd'T'HH:mm:ssZ", "yyyy-MM-dd'T'HH:mm:ss"]
        for f in formates {
            if let d = dateString.dateValue(format: f, timeZone: timeZone) {
                date = d
                break
            }
        }
        
        return date
    }
    
    
    
    subscript(_ range: CountableRange<Int>) -> String {
        let idx1 = index(startIndex, offsetBy: max(0, range.lowerBound))
        let idx2 = index(startIndex, offsetBy: min(self.count, range.upperBound))
        return String(self[idx1..<idx2])
    }
    
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    func IntValue() -> Int {
        if let Intval = Int(self) {
            return Intval
        }
        return 0
    }
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
            
        }
    }
    
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.string.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.string.count && self.string.count >= 5
            } else {
                return false
            }
        } catch {
            return false
        }
    }
}

extension Date {
    func getElapsedInterval() -> String {
        
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self, to: Date())
        
        if let year = interval.year, year > 0 {
            return year == 1 ? "\(year)" + " " + "year ago" :
                "\(year)" + " " + "years ago"
        } else if let month = interval.month, month > 0 {
            return month == 1 ? "\(month)" + " " + "month ago" :
                "\(month)" + " " + "months ago"
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "\(day)" + " " + "day ago" :
                "\(day)" + " " + "days ago"
        } else if let hour = interval.hour, hour > 0 {
            return hour == 1 ? "\(hour)" + " " + "hour ago" :
                "\(hour)" + " " + "hours ago"
        } else if let minute = interval.minute, minute > 0 {
            return minute == 1 ? "\(minute)" + " " + "min ago" :
                "\(minute)" + " " + "mins ago"
        } else if let second = interval.second, second > 0 {
            //return second == 1 ? "\(second)" + " " + "second" : "\(second)" + " " + "seconds"
            return "Just Now";
        } else {
            return "Just Now";
        }
        
    }
    
    static func dateToString(date: Date) -> String
    {
        let formatter = AppUtility.dateFormatter
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "dd-MM-yyyy"
        formatter.locale = Locale.current;
        let myString = formatter.string(from: date) // string purpose I add here
        // convert your string to date
        //let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        //formatter.dateFormat = "dd-MMM-yyyy"
        // again convert your date to string
        // let myStringafd = formatter.string(from: yourDate!);
        
        return myString;
    }
    
    static func getLocalDate(WithDateString dateStr:String, WithDateFormat format:String) -> String
    {
        var dateFormat = AppUtility.dateFormatter;//DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
        
        var postTime = dateFormat.date(from: dateStr)
        
        if postTime == nil
        {
            dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            postTime = dateFormat.date(from: dateStr)
        }
        
        if postTime == nil
        {
            dateFormat.dateFormat = "YYYY-MM-dd'T'HH:mm:ssZ"
            postTime = dateFormat.date(from: dateStr)
        }
        
        if postTime == nil
        {
            dateFormat.dateFormat = "HH:mm:ss.SSSXXX"
            postTime = dateFormat.date(from: dateStr)
        }
        
        if postTime == nil
        {
            //12 Sep 2018, 13h00 (1:00 PM) +0000
            dateFormat.dateFormat = "dd MMM YYYY, HH'h'mm (mm:ss a).SSSXXX";
            postTime = dateFormat.date(from: dateStr)
        }
        
        if postTime == nil
        {
            
            dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss";
            dateFormat.timeZone = NSTimeZone(name: "UTC") as TimeZone?;
            // time string from serve do not have the time zone info (2019-03-27 08:52:51), so i assumed, its a UTC time;
            postTime = dateFormat.date(from: dateStr)
        }
        
        
        dateFormat.timeZone = NSTimeZone.local
        
        if let _ = postTime
        {
            let localDate = dateFormat.date(from: dateFormat.string(from: postTime!))
            
            
            dateFormat = AppUtility.dateFormatter;//DateFormatter()
            dateFormat.timeZone = NSTimeZone.local
            dateFormat.dateFormat = format;
            let strDate = dateFormat.string(from: localDate!)
            
            return strDate;
        }
        
        
        
        
        
        return dateStr;
    }
    static func convertDateIntoUTCTimeStr(date:Date, locateIdentifier:String = Locale.current.identifier) -> String
    {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
        dateFormat.locale = Locale(identifier:locateIdentifier)
        //"yyyy-MM-dd'T'HH:mm:ss.SSSSSSSXXX"
        dateFormat.timeZone = TimeZone(abbreviation: "UTC");
        return dateFormat.string(from: date)
    }
    
    static func convertTimeOnlyIntoUTCTimeStr(dateStr:String, formatStr:String = "HH:mm:ss", locateIdentifier:String = Locale.current.identifier ) -> String
    {
        var dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
        dateFormat.timeZone = TimeZone(abbreviation: "UTC");
        var date = dateFormat.date(from: dateStr)
        
        if date == nil
        {
            dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            date = dateFormat.date(from: dateStr)
        }
        
        if date == nil
        {
            dateFormat.dateFormat = "YYYY-MM-dd'T'HH:mm:ssZ"
            date = dateFormat.date(from: dateStr)
        }
        
        dateFormat = DateFormatter()
        dateFormat.dateFormat = "HH:mm:ss"
        dateFormat.locale = Locale(identifier: locateIdentifier)
        //"yyyy-MM-dd'T'HH:mm:ss.SSSSSSSXXX"
        dateFormat.timeZone = TimeZone(abbreviation: "UTC");
        return dateFormat.string(from: date!)
    }
    
    static func convertDateTimeOnlyIntoUTCTimeStr(dateStr:String) -> Date
    {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
        dateFormat.timeZone = TimeZone(abbreviation: "UTC");
        var date = dateFormat.date(from: dateStr)
        
        if date == nil
        {
            dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            date = dateFormat.date(from: dateStr)
        }
        
        if date == nil
        {
            dateFormat.dateFormat = "YYYY-MM-dd'T'HH:mm:ssZ"
            date = dateFormat.date(from: dateStr)
        }
        
        //        dateFormat.dateFormat = "HH:mm:ss.SSSXXX"
        //        dateFormat.timeZone = TimeZone(abbreviation: "UTC");
        
        
        return date!
        //dateFormat.string(from: date!)
    }
    
    static func convertDateStrIntoLocalDateModel(dateStr:String) -> Date
    {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
        dateFormat.timeZone = NSTimeZone.local
        var date = dateFormat.date(from: dateStr)
        
        if date == nil
        {
            dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            date = dateFormat.date(from: dateStr)
        }
        
        if date == nil
        {
            dateFormat.dateFormat = "YYYY-MM-dd'T'HH:mm:ssZ"
            date = dateFormat.date(from: dateStr)
        }
        
        return date!
    }
    
    static func getDateFromString(_ dateStr:String) -> Date?
    {
        let dateFormat = DateFormatter()
        dateFormat.timeZone = TimeZone(abbreviation: "UTC")
        dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
        
        var postTime = dateFormat.date(from: dateStr)
        
        if postTime == nil
        {
            dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            postTime = dateFormat.date(from: dateStr)
        }
        
        if postTime == nil
        {
            dateFormat.dateFormat = "YYYY-MM-dd'T'HH:mm:ssZ"
            postTime = dateFormat.date(from: dateStr)
        }
        
        if postTime == nil
        {
            dateFormat.dateFormat = "HH:mm:ss.SSSXXX"
            postTime = dateFormat.date(from: dateStr)
        }
        
        if postTime == nil
        {
            dateFormat.dateFormat = "E MMM d HH:mm:ss Z yyyy"
            postTime = dateFormat.date(from: dateStr)
        }
        
        dateFormat.timeZone = NSTimeZone.local
        let strDate =  dateFormat.date(from: dateFormat.string(from: postTime!))!
        
        //dateFormat.timeZone =  TimeZone(abbreviation: "UTC")
        
        
        
        
        //let localDate = dateFormat.date(from: postTime)
        
        return strDate
    }
    
    
    func minutes(fromDate: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: fromDate, to: self).minute ?? 0
    }
    func seconds(fromDate: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: fromDate, to: self).second ?? 0
    }
    
    func hours(fromDate: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: fromDate, to: self).hour ?? 0
    }
    
    func getDaysInMonth() -> Int{
        let calendar = Calendar.current
        
        
        let dateComponents = DateComponents(year: calendar.component(.year, from: self), month: calendar.component(.month, from: self))
        let date = calendar.date(from: dateComponents)!
        
        let range = calendar.range(of: .day, in: .month, for: date)!
        let numDays = range.count
        
        return numDays
    }
    
    func getDaysInMonth(_ date:Date) -> Int{
        let calendar = Calendar.current
        
        
        let dateComponents = DateComponents(year: calendar.component(.year, from: date), month: calendar.component(.month, from: date))
        let date = calendar.date(from: dateComponents)!
        
        let range = calendar.range(of: .day, in: .month, for: date)!
        let numDays = range.count
        
        return numDays
    }
    
    func startOfMonth() -> Date? {
        let comp: DateComponents = Calendar.current.dateComponents([.year, .month, .hour], from: Calendar.current.startOfDay(for: self))
        return Calendar.current.date(from: comp)!
    }
    
    func endOfMonth() -> Date? {
        guard let startOfMonth = startOfMonth() else { return nil }
        
        var components = DateComponents()
        components.month = 1
        components.second = -1
        return Calendar(identifier: .gregorian).date(byAdding: components, to: startOfMonth)
    }
    
    static func getSpecificDateFormatWithDefaultDate(_ date:Date,_ format:String, localeIdentifier:String = Locale.current.identifier) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: localeIdentifier)
        let resultDateStr = dateFormatter.string(from: date)
        return resultDateStr
    }
    
    
//    MARK: TODO
//    var startOfWeek: Date? {
//        return Calendar.gregorian.date(from: Calendar.gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear,], from: self))
       
//    }
    
    
    var startDateOfWeek:Date? {
        
        let  dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"
        var gregorian = Calendar(identifier: .gregorian)
        gregorian.locale = Locale.current
        gregorian.timeZone = TimeZone.current
        var componentsDate: DateComponents? = gregorian.dateComponents([.weekday, .year, .month, .day], from: self)
        let dayofweek: Int = Calendar.current.dateComponents([.weekday], from: self).weekday!
        // this will give you current day of week
        var local = componentsDate?.day
        local = ((componentsDate!.day)! - (dayofweek - 1))
        componentsDate?.day = local
        // for beginning of the week.
        let beginningOfWeek: Date? = gregorian.date(from: componentsDate!)
        let dateFormat_first = DateFormatter()
        dateFormat_first.dateFormat = "yyyy-MM-dd"
        let dateString2Prev = dateFormat.string(from: beginningOfWeek!)
        let  weekstartPrev = dateFormat_first.date(from: dateString2Prev)
        return weekstartPrev
    }
    
    
    func dateByAdding(hours: Int) -> Date? {
        return Calendar.current.date(byAdding: .hour, value: hours, to: self)
    }
    func dateByAdding(days: Int) -> Date? {
        return Calendar.current.date(byAdding: .day, value: days, to: self)
    }
    func dateByAdding(months: Int) -> Date? {
        return Calendar.current.date(byAdding: .month, value: months, to: self)
    }
    func dateByAdding(years: Int) -> Date? {
        return Calendar.current.date(byAdding: .year, value: years, to: self)
    }
    func stringValue(format: String, timeZone: TimeZone? = TimeZone(abbreviation: "GMT")) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = timeZone
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter.string(from: self)
    }
    
    static func calculateAge(dob : String) -> (year :Int, month : Int, day : Int){
        if let dateFormat = AppUtility.getDateFromString(date: dob)
        {
            let df = AppUtility.dateFormatter
            df.dateFormat = dateFormat
            let date = df.date(from: dob)
            guard let val = date else{
                return (0, 0, 0)
            }
            var years = 0
            var months = 0
            var days = 0
            let cal = NSCalendar.current
            years = cal.component(.year, from: Date()) -  cal.component(.year, from: val)
            let currMonth = cal.component(.month, from: Date())
            let birthMonth = cal.component(.month, from: val)
            //get difference between current month and birthMonth
            months = currMonth - birthMonth
            //if month difference is in negative then reduce years by one and calculate the number of months.
            if months < 0
            {
                years = years - 1
                months = 12 - birthMonth + currMonth
                if cal.component(.day, from: Date()) < cal.component(.day, from: val){
                    months = months - 1
                }
            } else if months == 0 && cal.component(.day, from: Date()) < cal.component(.day, from: val)
            {
                years = years - 1
                months = 11
            }
            //Calculate the days
            if cal.component(.day, from:Date()) > cal.component(.day, from: val){
                days = cal.component(.day, from: NSDate() as Date) - cal.component(.day, from: val)
            }
            else if cal.component(.day, from: NSDate() as Date) < cal.component(.day, from: val)
            {
                let today = cal.component(.day, from: Date())
                let date = cal.date(byAdding: Calendar.Component.month, value: -1, to: Date())
                //dateByAddingUnit(.Month, value: -1, toDate: Date(), options: [])
                days = (cal.component(.day, from: date!) - cal.component(.day, from: val)) + today
            } else
            {
                days = 0
                if months == 12
                {
                    years = years + 1
                    months = 0
                }
            }
            return (years, months, days)
        }
        return(0,0,0)
        
    }
    
    static func calculateDifferenceeBwDates(date1 : String, date2:String?) -> (year :Int, month : Int, day : Int){
        
        if let dateFormat = AppUtility.getDateFromString(date: date1)
        {
            let df = AppUtility.dateFormatter
            df.dateFormat = dateFormat
            let date = df.date(from: date1)
            guard let val = date else{
                return (0, 0, 0)
            }
            var dateOther = Date()
            if date2 != nil
            {
                df.dateFormat = AppUtility.getDateFromString(date: date2!)!
                dateOther = df.date(from: date2!)!
            }
            
            var years = 0
            var months = 0
            var days = 0
            let cal = NSCalendar.current
            years = cal.component(.year, from: dateOther) -  cal.component(.year, from: val)
            let currMonth = cal.component(.month, from: dateOther)
            let birthMonth = cal.component(.month, from: val)
            //get difference between current month and birthMonth
            months = currMonth - birthMonth
            //if month difference is in negative then reduce years by one and calculate the number of months.
            if months < 0
            {
                years = years - 1
                months = 12 - birthMonth + currMonth
                if cal.component(.day, from: dateOther) < cal.component(.day, from: val){
                    months = months - 1
                }
            } else if months == 0 && cal.component(.day, from: dateOther) < cal.component(.day, from: val)
            {
                years = years - 1
                months = 11
            }
            //Calculate the days
            if cal.component(.day, from:dateOther) > cal.component(.day, from: val){
                days = cal.component(.day, from: dateOther) - cal.component(.day, from: val)
            }
            else if cal.component(.day, from: dateOther) < cal.component(.day, from: val)
            {
                let today = cal.component(.day, from: dateOther)
                let date = cal.date(byAdding: Calendar.Component.month, value: -1, to:dateOther)
                //dateByAddingUnit(.Month, value: -1, toDate: Date(), options: [])
                days = (cal.component(.day, from: date!) - cal.component(.day, from: val)) + today
            } else
            {
                days = 0
                if months == 12
                {
                    years = years + 1
                    months = 0
                }
            }
            return (years, months, days)
        }
        return (0, 0, 0)
    }
    
    var millisecondsSince1970:Double {
        return (self.timeIntervalSince1970 * 1000.0).rounded()
    }
    
    init(milliseconds:Double) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
    
    static func getDateFromTimeStamp(timeStamp: Double, dtFormatter: String) -> String {
        let date = Date(timeIntervalSince1970: timeStamp/1000.0)
        let dayTimePeriodFormatter = AppUtility.dateFormatter
        dayTimePeriodFormatter.dateFormat = dtFormatter;
        let dateString = dayTimePeriodFormatter.string(from: date)
        return dateString
    }
    
}



extension Bundle {
    var releaseVersionNumber: String {
        return infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
    }
    var buildVersionNumber: String {
        return infoDictionary?["CFBundleVersion"] as? String ?? ""
    }
    
    var bundleIdentifier: String? {
        return infoDictionary?["CFBundleIdentifier"] as? String
    }
    
    static func appDisplayName() -> String {
        
        guard let dictionary = Bundle.main.infoDictionary else {
            return ""
        }
        if let version : String = dictionary["CFBundleDisplayName"] as? String {
            return version
        } else {
            return ""
        }
    }
}

extension Array {
    func unique<T:Hashable>(map: ((Element) -> (T)))  -> [Element] {
        var set = Set<T>() //the unique list kept in a Set for fast retrieval
        var arrayOrdered = [Element]() //keeping the unique list of elements but ordered
        for value in self {
            if !set.contains(map(value)) {
                set.insert(map(value))
                arrayOrdered.append(value)
            }
        }
        
        return arrayOrdered
    }
    
    func object(at index: Int) -> Element? {
        if index >= 0 &&  index < self.count {
            return self[index]
        }
        return nil
    }
}
