//
//  Exrtension+URL.swift
//  WorkSpace
//
//  Created by Hamza Zulfqar on 05/07/2021.
//

import Foundation
import UIKit

extension URL {
    func withQueries(_ queries: [String:String?]) -> URL? {
        var components = URLComponents(url: self, resolvingAgainstBaseURL: true)
        components?.queryItems = queries.compactMap { URLQueryItem(name: $0, value: $1)}
        return components?.url
    }
    
    func appendingAccessToken(token:String? = nil) -> URL {
        if token == nil{
            return self.withQueries(["access_token":"\(UserInfo.userAccessToken.accessToken)"])!
        }
        else{
            return self.withQueries(["access_token":"\(UserInfo.userAccessToken.accessToken)","token":token])!
        }
        
    }
    func appendingMeeraStorageToken(token:String) -> URL {
        return self.withQueries(["token":token])!
    }
}
