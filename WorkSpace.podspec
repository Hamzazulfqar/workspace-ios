#
#  Be sure to run `pod spec lint WorkSpace.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.name         = "WorkSpace"
  spec.version      = "1.0.0"
  spec.summary      = "A short description of WorkSpace."
  spec.description  = "A complete description of WorkSpace."

  spec.platform     = :ios, "12.1"

spec.static_framework = true
  spec.homepage     = "http://EXAMPLE/WorkSpace"
  spec.license      = "MIT"
  spec.author             = { "Hamza Zulfqar" => "khouram426@gmail.com" }
  #spec.source       = { :path => '.' }
 spec.source       = { :git => "https://gitlab.com/Hamzazulfqar/workspace-ios.git", :tag => "3.1"}

spec.source_files  = "WorkSpace/**/*.{swift}"
  spec.exclude_files = "Classes/Exclude"

  # spec.public_header_files = "Classes/**/*.h"


  # ――― Resources ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  A list of resources included with the Pod. These are copied into the
  #  target bundle with a build phase script. Anything else will be cleaned.
  #  You can preserve files from being cleaned, please don't preserve
  #  non-essential files like tests, examples and documentation.
  #

  # spec.resource  = "icon.png"
  # spec.resources = "Resources/*.png"
  spec.resources = "WorkSpace/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"
 

  # spec.preserve_paths = "FilesToSave", "MoreFilesToSave"


  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Link your library with frameworks, or libraries. Libraries do not include
  #  the lib prefix of their name.
  #

  # spec.framework  = "SomeFramework"
  # spec.frameworks = "SomeFramework", "AnotherFramework"

  # spec.library   = "iconv"
  # spec.libraries = "iconv", "xml2"


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  # spec.requires_arc = true

  # spec.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
   spec.dependency "FittedSheets" ,"~> 1.4.6"
   spec.dependency "RxSwift"
   spec.dependency "RxCocoa"
   spec.dependency "SwiftyJSON"
   spec.dependency "SDWebImage"
   spec.dependency "OAuthSwift"
   spec.dependency "Alamofire", "~> 5.2"
   spec.dependency "PromiseKit", "~> 6.8"
   spec.dependency "IQKeyboardManagerSwift"
   spec.dependency "MaterialComponents/ActivityIndicator"
   spec.dependency "MaterialComponents/TextFields"
   #spec.dependency "TUSKit", "~> 1.4.2"
   #spec.dependency "SwiftGRPC"
   spec.dependency "MBCircularProgressBar"
   spec.dependency "MBProgressHUD", "~> 1.2.0"
     spec.dependency 'TFNetwork' 

end
